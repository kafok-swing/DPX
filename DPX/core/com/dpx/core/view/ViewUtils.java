package com.dpx.core.view;

import java.io.File;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dpx.core.utils.TextUtils;

public class ViewUtils {
	
	public static boolean transformJstlOut(Collection<File> files) {
		boolean res = true;
		
		System.out.println("===========================================");
		String start = "<jstl:out value=\"";
		String end = "\" />";
		Pattern pattern = Pattern.compile("[$][{][^{}]*[}]");
		for(File f : files) {
			String text = TextUtils.readTextFile(f);
			Matcher matcher = pattern.matcher(text);
			
			String trans = "";
			int acum = 0;
			
			while(matcher.find()) {
				if(!checkSpaceNone(true, end, text, matcher.end() + 1) || !checkSpaceNone(false, start, text, matcher.start())) {
					trans += text.substring(acum, matcher.start());
					trans += (start + text.substring(matcher.start(), matcher.end()) + end);
					acum = matcher.end();
				}
			}
			
			if(!trans.equals(""))
				System.out.println(TextUtils.saveTextFile(f.getPath(), trans));
		}
		
		return res;
	}
	
	
	private static boolean checkSpaceNone(boolean dir, String check, String text, int init) {
		check = check.replaceAll("\\s", "");
		
		try {
			int i = 0;
			int count = init;
			while(Math.abs(i) < check.length()) {
				char at = text.charAt(count);
				if(!Character.isWhitespace(at)) {
					if(at != check.charAt(i))
						return false;
					
					i += dir ? 1 : -1;
				}
				
				count++;
			}
		} catch(Throwable t) {
			return false;
		}
		
		return true;
	}
	
}
