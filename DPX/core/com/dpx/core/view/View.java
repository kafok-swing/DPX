package com.dpx.core.view;

import java.io.File;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.dpx.core.Project;
import com.dpx.core.common.Pair;
import com.dpx.core.common.Tuple3;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.common.DpxProgressable;
import com.dpx.gui.models.main.workspace.column.ColumnModel;
import com.dpx.resource.ResourceManager;

public class View {

	public static Collection<Tuple3<String, String, String>> getViewByEntityName(Project p, String name) {
		List<Tuple3<String, String, String>> res = new LinkedList<Tuple3<String, String, String>>();
		
		DpxProgressable.get().setIndeterminate(true);
		readXmls(res, p, name);
		DpxProgressable.get().setIndeterminate(false);
		
		return res;
	}

	private static boolean readXmls(Collection<Tuple3<String, String, String>> res, Project p, String name) {
		Map<String, Tuple3<String, String, String>> map = new LinkedHashMap<String, Tuple3<String, String, String>>();
		name = TextUtils.tranformString(name, false, true, false, "");
		
		try {
			File xml = new File(p.getProjectPath() + "src\\main\\webapp\\views\\" + name + "\\tiles.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xml);
			
			File xmlES = new File(p.getProjectPath() + "src\\main\\webapp\\views\\" + name + "\\tiles_es.xml");
			DocumentBuilderFactory dbFactoryES = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilderES = dbFactoryES.newDocumentBuilder();
			Document docES = dBuilderES.parse(xmlES);
			
			doc.normalize();
			docES.normalize();
			
			//EN
			NodeList node = doc.getElementsByTagName("definition");
			for(int i=0; i<node.getLength(); i++) {
				Node n = node.item(i);
				String attrName = n.getAttributes().getNamedItem("name").getNodeValue();
				attrName = attrName.substring(name.length()+1, attrName.length());
				
				NodeList puts = n.getChildNodes();
				for(int j=0; j<puts.getLength(); j++) {
					Node n2 = puts.item(j);
					if(n2.getNodeType() == Node.ELEMENT_NODE && n2.getNodeName().equals("put-attribute")) {
						String value = n2.getAttributes().getNamedItem("name").getNodeValue();
						if(value.equals("title")) {
							String title = n2.getAttributes().getNamedItem("value").getNodeValue();
							map.put(attrName, new Tuple3<String, String, String>(attrName, null, title));
						}
					}
				}
			}
			
			//ES
			node = docES.getElementsByTagName("definition");
			for(int i=0; i<node.getLength(); i++) {
				Node n = node.item(i);
				String attrName = n.getAttributes().getNamedItem("name").getNodeValue();
				attrName = attrName.substring(name.length()+1, attrName.length());
				
				NodeList puts = n.getChildNodes();
				for(int j=0; j<puts.getLength(); j++) {
					Node n2 = puts.item(j);
					if(n2.getNodeType() == Node.ELEMENT_NODE && n2.getNodeName().equals("put-attribute")) {
						String value = n2.getAttributes().getNamedItem("name").getNodeValue();
						if(value.equals("title")) {
							String title = n2.getAttributes().getNamedItem("value").getNodeValue();
							Tuple3<String, String, String> t = map.get(attrName);
							if(t != null) {
								t.setSecond(title);
								map.put(attrName, t);
							}
						}
					}
				}
			}
			
			
		} catch(Throwable t) {
			t.printStackTrace();
			return false;
		}
		
		for(Tuple3<String, String, String> t : map.values())
			res.add(t);
		
		return true;
	}

	
	//-------------------------------------------- Save file ---------------------------------------------
	
	public static boolean saveViewsFile(Project p, String name, Collection<Tuple3<String, String, String>> paramES, Collection<Tuple3<String, String, String>> paramEN) {
		//tiles
		saveFile(p, "src/main/webapp/views/" + name + "/tiles.xml", getTileLocation(paramEN));
		saveFile(p, "src/main/webapp/views/" + name + "/tiles_es.xml", getTileLocation(paramES));
		
		//properties
		if(!new File(p.getProjectPath() + "src/main/webapp/views/" + name + "/messages.properties").exists())
			saveFile(p, "src/main/webapp/views/" + name + "/messages.properties", "");
		if(!new File(p.getProjectPath() + "src/main/webapp/views/" + name + "/messages_es.properties").exists())
			saveFile(p, "src/main/webapp/views/" + name + "/messages_es.properties", "");
		
		//config
		saveFile(p, "src/main/resources/spring/config/i18n-l10n.xml", getI18n_L10n(p));
		saveFile(p, "src/main/resources/spring/config/tiles.xml", getTiles(p));
		
		for(Tuple3<String, String, String> t : paramES) {
			String path = "src/main/webapp/views/" + name + "/" + t.getSecond() + ".jsp";
			File jsp = new File(p.getProjectPath() + path);
			if(!jsp.exists())
				saveFile(p, path, getVoidView(false));
		}
		
		return true;
	}
	
	public static boolean deleteFile(Project p, String name, Collection<String> files) {
		for(File f : ResourceManager.scanFiles(p.getProjectPath() + "src/main/webapp/views/" + name + "/", "jsp")) {
			if(!files.contains(f.getName().substring(0, f.getName().length() - ".jsp".length())))
				f.delete();
		}
		
		return true;
	}
	
	public static boolean saveFile(Project p, String path, String text) {
		return TextUtils.saveTextFile(p.getProjectPath() + path, text);
	}
	
	public static boolean saveFile(String path, String text) {
		try {
			File dir = new File(path).getParentFile();
			if(!dir.exists())
				dir.mkdirs();
			
			PrintWriter out = new PrintWriter(path);
			out.print(text);
			out.close();
		} catch(Throwable e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static String getI18n_L10n(Project p) {
		String dirs = "";
		for(File dir : ResourceManager.scanDirectories(p.getProjectPath() + "/src/main/webapp/views/"))
			dirs += ("\t\t<value>/views/" + dir.getName() + "/messages</value>\n");
		
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "\n"
				+ "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n"
				+ "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:util=\"http://www.springframework.org/schema/util\"\n"
				+ "\txsi:schemaLocation=\"\n"
				+ "\t\thttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd\n"
				+ "\t\thttp://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd \n"
				+ "\t\">\n"
				+ "\n"
				+ "\t<util:list id=\"messages\">\n"
				+ dirs
				+ "\t</util:list>\n"
				+ "\n"
				+ "</beans>\n";
	}
	
	public static String getVoidView(boolean importCustomTag) {
		return "<%@page language=\"java\" contentType=\"text/html; charset=ISO-8859-1\" pageEncoding=\"ISO-8859-1\"%>\n"
				+ "\n"
				+ "<%@taglib prefix=\"jstl\"\turi=\"http://java.sun.com/jsp/jstl/core\"%>\n"
				+ "<%@taglib prefix=\"spring\" uri=\"http://www.springframework.org/tags\"%>\n"
				+ "<%@taglib prefix=\"form\" uri=\"http://www.springframework.org/tags/form\"%>\n"
				+ "<%@taglib prefix=\"security\" uri=\"http://www.springframework.org/security/tags\"%>\n"
				+ "<%@taglib prefix=\"display\" uri=\"http://displaytag.sf.net\"%>\n"
				+ "\n"
				+ "<%@taglib prefix=\"c\" uri=\"http://java.sun.com/jsp/jstl/core\"%>\n"
				+ (importCustomTag ? "<%@taglib prefix=\"acme\" tagdir=\"/WEB-INF/tags\"%>\n" : "");
	}
	
	public static String getTiles(Project p) {
		String dirs = "";
		
		for(File dir : ResourceManager.scanDirectories(p.getProjectPath() + "/src/main/webapp/views/"))
					dirs += ("\t\t<value>/views/" + dir.getName() + "/tiles.xml</value>\n");
				
				return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
						+ "\n"
						+ "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n"
						+ "\txmlns:util=\"http://www.springframework.org/schema/util\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
						+ "\txsi:schemaLocation=\"\n"
						+ "\t\thttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd\n"
						+ "\t\thttp://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd\n"
						+ "\t\">\n"
						+ "\n"
						+ "\t<util:list id=\"tiles\">\n"
						+ dirs
						+ "\t</util:list>\n"
						+ "\n"
						+ "</beans>\n";
	}
	
	public static String getTileLocation(Collection<Tuple3<String, String, String>> param) {
		String base = "";
		
		for(Tuple3<String, String, String> value : param) {
				base += ("\t<definition name=\"" + value.getFirst() + "/" + value.getSecond() + "\" extends=\"master.page\">\t\n"
					+ "\t\t<put-attribute name=\"title\" value=\"" + value.getThird() + "\" />\n"
					+ "\t\t<put-attribute name=\"body\" value=\"/views/" + value.getFirst() + "/" + value.getSecond() + ".jsp\" />\n"
					+ "\t</definition>\n"
					+ "\t\n");
		}
		
		return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
				+ "\n"
				+ "<!DOCTYPE tiles-definitions PUBLIC\n"
				+ "       \"-//Apache Software Foundation//DTD Tiles Configuration 2.0//EN\"\n"
				+ "       \"http://tiles.apache.org/dtds/tiles-config_2_0.dtd\">\n"
				+ "       \n"
				+ "<tiles-definitions>\n"
				+ "\t\n"
				+ base
				+ "</tiles-definitions>\n";
	}
	
	public static String getEditForm(String name, boolean ctag, Collection<String> hidden, Collection<Pair<DomainField, String>> fields) {
		String res = "<form:form action=\"" + name + "/edit.do\" modelAttribute=\"" + name + "\">\n"
				+ "\t\n"
				+ getHidden(hidden)
				+ (ctag ? getCustomFields(name, fields) : getEditFields(name, fields))
				+ (ctag ? getCustomButtons(name) : getButtons(name))
				+ "</form:form>\n";
		
		return res;
	}

	private static String getCustomFields(String name, Collection<Pair<DomainField, String>> fields) {
		String res = "";
		for(Pair<DomainField, String> par : fields) {
			String type = par.getSecond().equals("input") ? "textbox" : par.getSecond();
			if(par.getFirst().isEnum()) {
				res += ("\t<select id\"" + par.getFirst().getName() + "\">\n");
				for(String value : par.getFirst().getEnumValues())
					res += ("\t\t<option value=\"" + value + "\">" + value + "</option>\n");
				res +=  "\t</select>\n";
			} else
				res += ("\t<acme:" + type + " code=\"" + name + "." + par.getFirst().getName() + "\" path=\"" + par.getFirst().getName() + "\"/>\n");
		}

		res += "\t<br />\n\n";
		
		return res;
	}

	private static String getButtons(String name) {
		return "\t<input type=\"submit\" name=\"save\" value=\"<spring:message code=\"" + name + ".save\"/>\" />\n"
				+ "\t<jstl:if test=\"${" + name + ".id != 0}\">\n"
				+ "\t\t<input type=\"submit\" name=\"delete\"\n"
				+ "\t\t\tvalue=\"<spring:message code=\"" + name + ".delete\" />\"\n"
				+ "\t\t\tonclick=\"return confirm('<spring:message code=\"" + name + ".confirm.delete\" />')\" />\n"
				+ "\t</jstl:if>\n"
				+ "\t<input type=\"button\" name=\"cancel\" value=\"<spring:message code=\"" + name + ".cancel\"/>\" \n"
				+ "\t\tonclick=\"javascript: relativeRedir('/" + name + "/list.do')\"/>\n"
				+ "\t\n";
	}
	
	private static String getCustomButtons(String name) {
		return "\t<acme:submit code=\"" + name + ".save\" name=\"save\" />\n"
				+ "\t<jstl:if test=\"${" + name + ".id != 0}\">\n"
				+ "\t\t<acme:submit code=\"" + name + ".delete\" name=\"delete\" confirm=\"" + name + ".confirm.delete\"\" />\n"
				+ "\t</jstl:if>\n"
				+ "\t<acme:cancel code=\"" + name + ".cancel\" url=\"" + name + "/list.do\" />\n\n";
	}

	private static String getEditFields(String name, Collection<Pair<DomainField, String>> fields) {
		String res = "";
		for(Pair<DomainField, String> par : fields) {
			if(par.getFirst().isEnum()) {
				res += ("\t<select id\"" + par.getFirst().getName() + "\">\n");
				for(String value : par.getFirst().getEnumValues())
					res += ("\t\t<option value=\"" + value + "\">" + value + "</option>\n");
				res +=  "\t</select>\n";
			} else {
				res += ("\t<form:label path=\"" + par.getFirst().getName() + "\">\n"
						+ "\t\t<spring:message code=\"" + name + "." + par.getFirst().getName() + "\" />\n"
						+ "\t</form:label>\n"
						+ "\t<form:" + par.getSecond() + " path=\"" + par.getFirst().getName() + "\" />\n"
						+ "\t<form:errors cssClass=\"error\" path=\"" + par.getFirst().getName() + "\" />\n"
						+ "\t<br />\n"
						+ "\n");
			}
		}
		
		res = res.substring(0, res.length()-1);
		res += "\t<br />\n\n";
		
		return res;
	}

	private static String getHidden(Collection<String> hidden) {
		String res = "";
		for(String str : hidden)
			res += ("\t<form:hidden path=\"" + str + "\"/>\n");
		
		res += "\n";
			
		return res;
	}
	
	public static String getList(String entity, Collection<ColumnModel> columns) {
		String name = TextUtils.tranformString(entity, false, true, false, "") + "s";
		entity = TextUtils.tranformString(entity, true, true, false, "");
		String str = "";
		
		for(ColumnModel cm : columns)
			str += cm.getColumn();
		
		return "<display:table pagesize=\"5\" class=\"displaytag\" keepStatus=\"true\" name=\"" + name + "\" requestURI=\"${requestUri}\" id=\"row" + entity + "\">\n"
				+ "\n"
				+ str
				+ "</display:table>\n";
	}
	
	public static String getColumnByField(String entity, DomainField field, boolean sortable) {
		entity = TextUtils.tranformString(entity, false, true, false, "");
		String sort = "sortable=\"" + sortable + "\"";
		String date =  field.getType().getName().equals("Date") ? " format=\"{0,date,dd/MM/yyyy hh:mm}\"" : "";
		
		return "\t<spring:message code=\"" + entity + "." + field.getName() + "\" var=\"" + field.getName() + "Header\" />\n"
				+ "\t<display:column property=\"" + field.getName() + "\" title=\"${" + field.getName() + "Header}\" " + sort + date + " />\n\n";
	}
	
	public static String getColumnLink(String code, String nameES, String nameEN, String url) {
		return null;
	}
}
