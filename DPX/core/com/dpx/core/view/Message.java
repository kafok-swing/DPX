package com.dpx.core.view;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.dpx.core.common.Pair;
import com.dpx.core.utils.TextUtils;

public class Message {
	
	private Map<String, Pair<String, String>> map;
	
	private Message(Map<String, Pair<String, String>> map) {
		super();
		this.map = map;
	}
	

	public static Message load(String path) {
		Map<String, Pair<String, String>> map = new HashMap<String, Pair<String, String>>();
		try {
			Collection<String> msg = TextUtils.readLineTextFile(path + ".properties");
			for(String s : msg) {
				if(s.contains("=")) {
					String arg[] = s.split("[=]");
					map.put(arg[0].trim(), new Pair<String, String>(arg[1].trim(), ""));
				}
			}
			
			msg = TextUtils.readLineTextFile(path + "_es.properties");
			for(String s : msg) {
				if(s.contains("=")) {
					String arg[] = s.split("[=]");
					Pair<String, String> par = map.get(arg[0].trim());
					if(par == null)
						map.put(arg[0].trim(), new Pair<String, String>("", arg[1].trim()));
					else {
						par.setSecond(arg[1].trim());
						map.put(arg[0].trim(), par);
					}
				}
			}
		} catch(Throwable t) {}
		
		return new Message(map);
	}
	
	public String getEN(String code) {
		return getEN(code, null);
	}
	
	public String getEN(String code, String attr) {
		Pair<String, String> msg = map.get(code);
		String res = null;
		if(msg != null)
			res = msg.getFirst();
		
		return res == null || res.equals("") ? (attr == null || attr.equals("") ? res : TextUtils.tranformString(attr, true, false, false, " ")) : res;
	}
	
	public String getES(String code) {
		return getES(code, null);
	}
	
	public String getES(String code, String attr) {
		Pair<String, String> msg = map.get(code);
		String res = null;
		if(msg != null)
			res = msg.getSecond();
		
		return res == null || res.equals("") ? (attr == null || attr.equals("") ? res : TextUtils.tranformString(attr, true, false, false, " ")) : res;
	}
}
