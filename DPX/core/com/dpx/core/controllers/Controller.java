package com.dpx.core.controllers;

import java.io.File;

import javax.swing.JOptionPane;

import com.dpx.core.Project;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.DpxFrame;

public class Controller {
	
	public static boolean saveFile(Project p, String name, String path, boolean reconstructs, boolean _void) {
		File f = new File(p.getProjectPath() + "src/main/java/controllers/" + name + "Controller.java");
		int num = JOptionPane.OK_OPTION;
		if(f.exists())
			num = JOptionPane.showConfirmDialog(DpxFrame.get(), "�Cuidado! Ya existe un controlador para la clase " + name + ". �Desea sobreescribirlo?", 
					"El archivo ya existe", JOptionPane.WARNING_MESSAGE);
		
		if(num != JOptionPane.OK_OPTION)
			return false;
		
		return TextUtils.saveTextFile(f.getAbsolutePath(), getContentFile(p, name, path, reconstructs, _void));
	}
	
	public static void copyFile(Project p, String name, String path, boolean reconstructs, boolean _void) {
		UtilsGui.copyToClippboard(getContentFile(p, name, path, reconstructs, _void));
	}
	
	public static String getContentFile(Project p, String name, String path, boolean reconstructs, boolean _void) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		
		String noVoid = "";
		if(!_void) {
			noVoid = "\t//----------------------------------------------- List -----------------------------------------------\n"
					+ "\t\n"
					+ "\t@RequestMapping(value = \"/list\")\n"
					+ "\tpublic ModelAndView list() {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\tCollection<" + name + "> " + n + "s;\n"
					+ "\t\t\n"
					+ "\t\t" + n + "s = " + n + "Service.findAll();\n"
					+ "\t\t\n"
					+ "\t\tresult = new ModelAndView(\"" + n + "/list\");\n"
					+ "\t\tresult.addObject(\"" + n + "s\", " + n + "s);\n"
					+ "\t\tresult.addObject(\"requestUri\", \"" + path + "/list.do\");\n"
					+ "\t\t\n"
					+ "\t\treturn result;\n"
					+ "\t}\t\n"
					+ "\t\n"
					+ "\t\n"
					+ "\t//---------------------------------------------- Create ----------------------------------------------\n"
					+ "\t\n"
					+ "\t@RequestMapping(value = \"/create\", method = RequestMethod.GET)\n"
					+ "\tpublic ModelAndView create() {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\t" + name + " " + n + ";\n"
					+ "\t\t\n"
					+ "\t\t" + n + " = " + n + "Service.create();\n"
					+ "\t\t\n"
					+ "\t\tresult = new ModelAndView(\"" + n + "/edit\");\n"
					+ "\t\tresult.addObject(\"" + n + "\", " + n + ");\n"
					+ "\t\t\n"
					+ "\t\treturn result;\n"
					+ "\t}\n"
					+ "\t\n"
					+ "\t\n"
					+ "\t//----------------------------------------------- Edit -----------------------------------------------\n"
					+ "\t\n"
					+ "\t@RequestMapping(value = \"/edit\", method = RequestMethod.GET)\n"
					+ "\tpublic ModelAndView edit(@RequestParam int " + n + "Id){\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\t" + name + " " + n + ";\n"
					+ "\t\t\n"
					+ "\t\t" + n + " = " + n + "Service.findOne(" + n + "Id);\n"
					+ "\t\tresult = createEditModelAndView(" + n + ");\n"
					+ "\t\t\n"
					+ "\t\treturn result;\n"
					+ "\t}\n"
					+ "\t\n"
					+ "\t@RequestMapping(value = \"/edit\", method = RequestMethod.POST, params = \"save\")\n"
					+ "\tpublic ModelAndView save(" + (reconstructs ? "" : "@Valid ") + name + " " + n + ", BindingResult binding) {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\t\n"
					+ (reconstructs ? "\t\t" + n + " = " + n + "Service.reconstruct(" + n + ", binding);\n" : "")
					+ "\t\tif(binding.hasErrors()) {\n"
					+ "\t\t\tresult = createEditModelAndView(" + n + ");\n"
					+ "\t\t} else {\n"
					+ "\t\t\ttry {\n"
					+ "\t\t\t\t" + n + "Service.save(" + n + ");\n"
					+ "\t\t\t\tresult = new ModelAndView(\"redirect:" + path + "/list.do\");\n"
					+ "\t\t\t} catch (Throwable oops) {\n"
					+ "\t\t\t\tresult = createEditModelAndView(" + n + ", \"" + n + ".commit.error\");\n"
					+ "\t\t\t}\n"
					+ "\t\t}\n"
					+ "\t\t\n"
					+ "\t\treturn result;\n"
					+ "\t}\n"
					+ "\t\n"
					+ "\t@RequestMapping(value = \"edit\", method = RequestMethod.POST, params = \"delete\")\n"
					+ "\tpublic ModelAndView delete(" + name + " " + n + ", BindingResult binding) {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\t\n"
					+ "\t\ttry {\n"
					+ "\t\t\t" + n + "Service.delete(" + n + ");\n"
					+ "\t\t\tresult = new ModelAndView(\"redirect:" + path + "/list.do\");\n"
					+ "\t\t} catch(Throwable oops ){\n"
					+ "\t\t\tresult = createEditModelAndView(" + n + ", \"" + n + ".commit.error\");\n"
					+ "\t\t}\n"
					+ "\t\t\n"
					+ "\t\treturn result;\n"
					+ "\t}\n"
					+ "\t\n"
					+ "\t\n"
					+ "\t//------------------------------------------ Helper methods ------------------------------------------\n"
					+ "\t\n"
					+ "\tprotected ModelAndView createEditModelAndView(" + name + " " + n + ") {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\tresult = createEditModelAndView(" + n + ", null);\n"
					+ "\t\treturn result;\n"
					+ "\t}\t\n"
					+ "\t\n"
					+ "\tprotected ModelAndView createEditModelAndView(" + name + " " + n + ", String message) {\n"
					+ "\t\tModelAndView result;\n"
					+ "\t\t\t\t\n"
					+ "\t\tresult = new ModelAndView(\"" + n + "/edit\");\n"
					+ "\t\tresult.addObject(\"" + n + "\", " + n + ");\n"
					+ "\t\tresult.addObject(\"message\", message);\n"
					+ "\n"
					+ "\t\treturn result;\n"
					+ "\t}\n"
					+ "\n";
		}
		
		return "package controllers;\n"
				+ (reconstructs ? "" : "\nimport javax.validation.Valid;\n")
				+ "\n"
				+ "import java.util.Collection;\n"
				+ "\n"
				+ "import org.springframework.beans.factory.annotation.Autowired;\n"
				+ "import org.springframework.stereotype.Controller;\n"
				+ "import org.springframework.validation.BindingResult;\n"
				+ "import org.springframework.web.bind.annotation.RequestMapping;\n"
				+ "import org.springframework.web.bind.annotation.RequestMethod;\n"
				+ "import org.springframework.web.servlet.ModelAndView;\n"
				+ "import org.springframework.web.bind.annotation.RequestParam;\n"
				+ "\n"
				+ "import services." + name + "Service;\n"
				+ "import controllers.AbstractController;\n"
				+ "import domain." + name + ";\n"
				+ "\n"
				+ "@Controller\n"
				+ "@RequestMapping(\"" + path + "\")\n"
				+ "public class " + name + "Controller extends AbstractController {\n"
				+ "\t\n"
				+ "\t//--------------------------------------- Supporting services ----------------------------------------\n"
				+ "\t\n"
				+ "\t@Autowired\n"
				+ "\tprivate " + name + "Service " + n + "Service;\n"
				+ "\t\n"
				+ "\t\n"
				+ "\t//------------------------------------------- Constructor --------------------------------------------\n"
				+ "\t\n"
				+ "\tpublic " + name + "Controller() {\n"
				+ "\t\tsuper();\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\t\n"
				+ noVoid
				+ "}\n";
	}
}
