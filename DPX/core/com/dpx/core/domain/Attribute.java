package com.dpx.core.domain;

import java.util.List;

import com.dpx.core.domain.constrains.Constrain;

public class Attribute {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private String name;
	private AttributeType type;
	private boolean isId;
	private boolean isVersion;
	private boolean isDatatype;
	private boolean isOptative;
	private boolean isCollection;
	
	private List<Constrain> constrains;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Attribute(String name, AttributeType type, boolean isId, boolean isVersion, boolean isDatatype, boolean isOptative,
			List<Constrain> constrains, boolean isCollection) {
		super();
		this.name = name;
		this.type = type;
		this.isId = isId;
		this.isVersion = isVersion;
		this.isDatatype = isDatatype;
		this.isOptative = isOptative;
		this.constrains = constrains;
		this.isCollection = isCollection;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public boolean isCollection() {
		return isCollection;
	}

	public String getName() {
		return name;
	}

	public AttributeType getType() {
		return type;
	}
	
	public boolean isId() {
		return isId;
	}

	public boolean isVersion() {
		return isVersion;
	}
	
	public boolean isDatatype() {
		return isDatatype;
	}
	
	public List<Constrain> getConstrains() {
		return constrains;
	}
	
	public boolean isOptative() {
		return isOptative;
	}
	
	
	//-------------------------------------------- Estaticos ---------------------------------------------
	
	public static String[] getTypes() {
		AttributeType val[] = AttributeType.values();
		String res[] = new String[val.length-1];
		
		for(int i=0; i<val.length-1; i++) {
			switch(val[i]) {
				case BOOLEAN:
					res[i] = "boolean";
					break;
				case BYTE:
					res[i] = "byte";
					break;
				case CHAR:
					res[i] = "char";
					break;
				case DATATYPE:
					res[i] = "Datatype";
					break;
				case DATE:
					res[i] = "Date";
					break;
				case DOUBLE:
					res[i] = "Double";
					break;
				case ENUM:
					res[i] = "Enum";
					break;
				case FLOAT:
					res[i] = "float";
					break;
				case INT:
					res[i] = "int";
					break;
				case LONG:
					res[i] = "long";
					break;
				case SHORT:
					res[i] = "short";
					break;
				case STRING:
					res[i] = "String";
					break;
				default:
					break;
			}
		}
		
		return res;
	}
	
	public static int getIndexByType(AttributeType type) {
		AttributeType val[] = AttributeType.values();
		for(int i=0; i<val.length-1; i++) {
			if(val[i].equals(type))
				return i;
		}
		
		return 0;
	}
	
}
