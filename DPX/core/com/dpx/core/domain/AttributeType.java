package com.dpx.core.domain;

public enum AttributeType {
	BOOLEAN,
	BYTE,
	SHORT,
	CHAR,
	INT,
	LONG,
	FLOAT, 
	DOUBLE,
	
	STRING,
	ENUM,
	
	DATE,
	
	DATATYPE,
	
	RELATIONSHIP
}