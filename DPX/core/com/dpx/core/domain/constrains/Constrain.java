package com.dpx.core.domain.constrains;

import com.dpx.core.utils.TextUtils;

public class Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private ConstrainType type;

	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Constrain(ConstrainType type) {
//		if(type.equals(ConstrainType.MAX) ||
//				type.equals(ConstrainType.MIN) ||
//				type.equals(ConstrainType.RANGE) ||
//				type.equals(ConstrainType.PATTERN) ||
//				type.equals(ConstrainType.DIGIT) ||
//				type.equals(ConstrainType.SIZE) ||
//				type.equals(ConstrainType.LENGTH))
//			throw new IllegalArgumentException("Invalid inicialitation constrain!");
		
		this.type = type;
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public ConstrainType getType() {
		return type;
	}

	
	//---------------------------------------------- Check -----------------------------------------------
	
	public boolean check(Object obj) {
		boolean res = false;
		
		switch(type) {
			case PAST:
				break;
				
			case NOT_NULL:
				break;
				
			case NOT_BLANK:
				break;
				
			case NOT_EMPTY:
				break;
				
			default:
				break;
		}
		
		return res;
	}
	
	
	//-------------------------------------------- Operations --------------------------------------------
	
	public static String[] getListConstraints() {
		ConstrainType types[] = ConstrainType.values();
		String res[] = new String[types.length];
		
		int i = 0;
		for(ConstrainType c : types) {
			res[i] = TextUtils.tranformString(c.toString(), true, true, false, "");
			i++;
		}
		
		return res;
	}
	
	public static Constrain getByNumber(int num) {
		ConstrainType type = ConstrainType.values()[num];
		Constrain res = null;
		
		switch(type) {
			case DIGIT:
				res = new ConstrainDigit(0, 0);
				break;
				
			case LENGTH:
				res = new ConstrainLength(0, 0);
				break;
				
			case MAX:
				res = new ConstrainMax(0);
				break;
				
			case MIN:
				res = new ConstrainMin(0);
				break;
				
			case NOT_BLANK:
				res = new Constrain(ConstrainType.NOT_BLANK);
				break;
				
			case NOT_EMPTY:
				res = new Constrain(ConstrainType.NOT_EMPTY);
				break;
				
			case NOT_NULL:
				res = new Constrain(ConstrainType.NOT_NULL);
				break;
				
			case PAST:
				res = new Constrain(ConstrainType.PAST);
				break;
				
			case PATTERN:
				res = new ConstrainPattern("^$");
				break;
				
			case RANGE:
				res = new ConstrainRange(0, 0);
				break;
				
			case SIZE:
				res = new ConstrainSize(0, 0);
				break;
		}
		
		return res;
	}
}
