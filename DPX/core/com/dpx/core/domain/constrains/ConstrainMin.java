package com.dpx.core.domain.constrains;

public class ConstrainMin extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private int min;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainMin(int min) {
		super(ConstrainType.MIN);
		this.min = min;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public int getMin() {
		return min;
	}


}
