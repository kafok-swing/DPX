package com.dpx.core.domain.constrains;

public class ConstrainDigit extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private int integer;
	private int fraction;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainDigit(int integer, int fraction) {
		super(ConstrainType.DIGIT);
		this.integer = integer;
		this.fraction = fraction;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public int getInteger() {
		return integer;
	}

	public int getFraction() {
		return fraction;
	}
	
}
