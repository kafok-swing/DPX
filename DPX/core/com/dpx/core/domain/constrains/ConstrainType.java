package com.dpx.core.domain.constrains;

public enum ConstrainType {
	PAST,
	MAX,
	MIN,
	RANGE,
	PATTERN,
	DIGIT,
	NOT_NULL,
	SIZE,
	LENGTH,
	NOT_BLANK,
	NOT_EMPTY
}
