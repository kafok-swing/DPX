package com.dpx.core.domain.constrains;

public class ConstrainMax extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private int max;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainMax(int max) {
		super(ConstrainType.MAX);
		this.max = max;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------

	public int getMax() {
		return max;
	}
	

}
