package com.dpx.core.domain.constrains;

public class ConstrainRange extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private int min;
	private int max;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainRange(int min, int max) {
		super(ConstrainType.RANGE);
		this.min = min;
		this.max = max;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
	

}
