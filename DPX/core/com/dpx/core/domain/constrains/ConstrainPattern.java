package com.dpx.core.domain.constrains;

public class ConstrainPattern extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private String pattern;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainPattern(String pattern) {
		super(ConstrainType.PATTERN);
		this.pattern = pattern;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------

	public String getPattern() {
		return pattern;
	}
	

}
