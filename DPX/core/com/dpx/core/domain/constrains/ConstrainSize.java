package com.dpx.core.domain.constrains;

public class ConstrainSize extends Constrain {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private int min;
	private int max;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public ConstrainSize(int min, int max) {
		super(ConstrainType.SIZE);
		this.min = min;
		this.max = max;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
	

}
