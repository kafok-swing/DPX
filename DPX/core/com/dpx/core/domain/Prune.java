package com.dpx.core.domain;

import java.io.PrintWriter;
import java.util.Collection;

import com.dpx.core.Project;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.gui.view.main.DpxFrame;

public class Prune {
	
	//-------------------------------------------- Save file ---------------------------------------------

	public static boolean saveFile(Project p, Collection<DomainField> fields, String name) {
		try {
			PrintWriter out = new PrintWriter(p.getProjectPath() + "src/main/java/forms/" + name + ".java");
			out.print(getContentFile(fields, name));
			out.close();
			
			DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(p);
		} catch(Throwable e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static String getContentFile(Collection<DomainField> fields, String name) {
		String imports = "";
		String props = "";
		String gs = "";
		boolean hasDate = false;
		for(DomainField f : fields) {
			//imports
			if(f.getType().isOtherClass())
				imports += ("import domain." + f.getType().getName() + ";\n");
			
			//tipo
			String type = f.isCollection() ? "Collection<" : "";
			type += f.getType().getName();
			type += (f.isCollection() ? ">" : "");
			
			//atributos
			props += ("\tprivate " + type + " " + f.getName() + ";\n");
			
			//getters-setters
			gs += "\tpublic " + type + " " + f.getterName() + "() {\n"
					+ "\t\treturn " + f.getName() + ";\n"
					+ "\t}\n"
					+ "\n"
					+ "\tpublic void " + f.setterName() + "(" + type + " " + f.getName() + ") {\n"
					+ "\t\tthis." + f.getName() + " = " + f.getName() + ";\n"
					+ "\t}\n\n";
			
			if(f.getType().getName().equals("Date"))
				hasDate = true;
		}
		
		if(hasDate)
			imports = "import java.util.Date;\n\n" + imports;
		
		return "package forms;\n"
				+ (imports.equals("") ? "" : "\n")
				+ imports
				+ "\n"
				+ "public class " + name + " {\n"
				+ "\n"
				+ "\t//-------------------------------------------- Attributes --------------------------------------------\n"
				+ "\t\n"
				+ props
				+ "\t\n"
				+ "\t\n"
				+ "\t//------------------------------------------- Constructor --------------------------------------------\n"
				+ "\t\n"
				+ "\tpublic " + name + "() {\n"
				+ "\t\tsuper();\n"
				+ "\t}\n"
				+ "\n"
				+ "\t\n"
				+ "\t//----------------------------------------- Getters-Setters ------------------------------------------\n"
				+ "\t\n"
				+ gs
				+ "}\n";
	}
}
