package com.dpx.core.domain;

public class EntityFactory {

	
//	@SuppressWarnings("rawtypes")
//	public static Collection<Entity> getAllEntities(String packageName) throws ClassNotFoundException, IOException, NoSuchFieldException, SecurityException
//	{
//		Map<String, Entity> res = new HashMap<String, Entity>();
//		List<Class> classes = Reflexion.getClasses(Configuration.get().getUsingPackage(), Configuration.get().getProjectPath() + Configuration.get().getTargetPath());
//		for(Class c : classes) {
//			Entity _ent = createEntity(res, c);
//			if(_ent != null)
//				res.put(_ent.getName(), _ent);
//		}
//		
//		return res.values();
//	}
//
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	private static Entity createEntity(Map<String, Entity> res, Class c) throws ClassNotFoundException, NoSuchFieldException, SecurityException {
//		if(c.isAnnotationPresent(javax.persistence.Entity.class)) {
//			Entity parent = null;
//			Class cparent = c.getSuperclass();
//			if(cparent != null) {
//				parent = res.get(c.getSimpleName());
//				if(parent == null) {
//					parent = createEntity(res, cparent);
//					if(parent != null)
//						res.put(cparent.getSimpleName(), parent);
//				}
//			}
//			
//			Tuple attr = createAttributes(c);
//			Entity ent = new Entity(c.getSimpleName(), attr.attr, parent, new LinkedList<Entity>(), attr.rel);
//			return ent;
//		}
//		
//		return null;
//	}
//	
//	@SuppressWarnings("rawtypes")
//	private static Tuple createAttributes(Class c) throws ClassNotFoundException, NoSuchFieldException, SecurityException {
//		Tuple res = new Tuple();
//		res.attr = new LinkedList<Attribute>();
//		res.rel = new LinkedList<Relationship>();
//		
//		Method[] ms = c.getDeclaredMethods();
//		for(Method m : ms) {
//			if(m.getName().startsWith("get") && !m.isAnnotationPresent(javax.persistence.Transient.class)) {
//				Attribute attr = null;
//				if(m.getReturnType().isAnnotationPresent(javax.persistence.Entity.class) || Class.forName("java.lang.Iterable").isAssignableFrom(m.getReturnType())) {
//					//TODO es relacion
//				} else {
//					String name;
//					boolean id;
//					boolean version;
//					
//					switch(m.getReturnType().getName()) {
//						case "java.lang.Boolean":
//						case "boolean":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.BOOLEAN, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Byte":
//						case "byte":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.BYTE, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Short":
//						case "short":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.SHORT, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Character":
//						case "char":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.CHAR, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Integer":
//						case "int":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.INT, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Long":
//						case "long":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.LONG, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Float":
//						case "float":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.FLOAT, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.Double":
//						case "double":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.DOUBLE, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.lang.String":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.STRING, id, version, false);
//							
//							//TODO restricciones
//							break;
//							
//						case "java.util.Date":
//							name = TextFileUtils.tranformString(m.getName().substring(3), true, false, false, " ");
//							id = m.isAnnotationPresent(javax.persistence.Id.class);
//							version = m.isAnnotationPresent(javax.persistence.Version.class);
//							
//							attr = new Attribute(name, AttributeType.DATE, id, version, false);
//							
//							//TODO restricciones
//							break;
//					}
//					
//					res.attr.add(attr);
//				}
//			}
//		}
//		
//		return res;
//	}
//	
//	
//	private static class Tuple {
//		List<Attribute> attr;
//		List<Relationship> rel;
//	}
}
