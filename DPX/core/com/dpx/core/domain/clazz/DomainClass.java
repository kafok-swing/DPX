package com.dpx.core.domain.clazz;

import java.util.LinkedList;
import java.util.List;

public class DomainClass implements Comparable<DomainClass> {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private String name;
	private DomainClass parent;
	private List<DomainField> fields;
	private boolean isAbstract;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public DomainClass(String name, DomainClass parent, List<DomainField> fields, boolean isAbstract) {
		super();
		this.name = name;
		this.parent = parent;
		this.fields = fields;
		this.isAbstract = isAbstract;
	}


	//--------------------------------------------- Getters ----------------------------------------------

	public boolean isAbstract() {
		return isAbstract;
	}

	public String getName() {
		return name;
	}

	public DomainClass getParent() {
		return parent;
	}
	
	public List<DomainField> getFields() {
		return fields;
	}
	
	public void setParent(DomainClass parent) {
		this.parent = parent;
	}


	//---------------------------------------------- Equals ----------------------------------------------

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainClass other = (DomainClass) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	//-------------------------------------------- CompareTo ---------------------------------------------

	@Override
	public int compareTo(DomainClass arg0) {
		return getName().compareTo(arg0.getName());
	}
	
	
	//--------------------------------------------- Derived ----------------------------------------------
	
	public List<DomainField> getCollectionField() {
		List<DomainField> res = new LinkedList<DomainField>();
		
		for(DomainField field : getAllField()) {
			if(field.isCollection())
				res.add(field);
		}
		
		return res;
	}
	
	public List<DomainField> getAllField() {
		List<DomainField> res = new LinkedList<DomainField>();
		if(parent != null) 
			res.addAll(parent.getAllField());
		
		res.addAll(fields);
		return res;
	}
	
	public boolean hasFieldType(String type) {
		for(DomainField f : getAllField())
			if(f.getType().getName().equals(type))
				return true;
		
		return false;
	}
	
	public boolean hasFieldName(String type) {
		for(DomainField f : getAllField())
			if(f.getName().equals(type))
				return true;
		
		return false;
	}
	
	public DomainField getField(String field) {
		for(DomainField f : getAllField())
			if(f.getName().equals(field))
				return f;
		
		return null;
	}
}
