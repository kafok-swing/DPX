package com.dpx.core.domain.clazz;

public class DomainFieldType {

	//-------------------------------------------- Attributes --------------------------------------------

	private String name;
	private boolean isPrimitive;
	private boolean isWarpper;
	private boolean isOtherClass;

	
	//------------------------------------------- Constructor --------------------------------------------

	public DomainFieldType(String name, boolean isPrimitive, boolean isWarpper, boolean isOtherClass) {
		super();
		this.name = name;
		this.isPrimitive = isPrimitive;
		this.isWarpper = isWarpper;
		this.isOtherClass = isOtherClass;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------

	public String getName() {
		return name;
	}

	public boolean isPrimitive() {
		return isPrimitive;
	}

	public boolean isWarpper() {
		return isWarpper;
	}

	public boolean isOtherClass() {
		return isOtherClass;
	}
	
	
	//---------------------------------------------- Static ----------------------------------------------

	public static Class<?> primitiveToWarpper(Class<?> primitive) {
		
		if(primitive.equals(boolean.class))
			return Boolean.class;
		else if(primitive.equals(byte.class)) 
			return Byte.class;
		else if(primitive.equals(char.class)) 
			return Character.class;
		else if(primitive.equals(double.class)) 
			return Double.class;
		else if(primitive.equals(float.class)) 
			return Float.class;
		else if(primitive.equals(int.class)) 
			return Integer.class;
		else if(primitive.equals(long.class)) 
			return Long.class;
		else if(primitive.equals(short.class)) 
			return Short.class;
		else if(primitive.equals(void.class)) 
			return Void.class;
		
		return null;
	}
	
	public static Class<?> wrapperToPrimitive(Class<?> warpper) {
		
		if(warpper.equals(Boolean.class))
			return boolean.class;
		else if(warpper.equals(Byte.class)) 
			return byte.class;
		else if(warpper.equals(Character.class)) 
			return char.class;
		else if(warpper.equals(Double.class)) 
			return double.class;
		else if(warpper.equals(Float.class)) 
			return float.class;
		else if(warpper.equals(Integer.class)) 
			return int.class;
		else if(warpper.equals(Long.class)) 
			return long.class;
		else if(warpper.equals(Short.class)) 
			return short.class;
		else if(warpper.equals(Void.class)) 
			return void.class;
		
		return null;
	}
	
	public static boolean isWrapper(Class<?> clazz) {
		
		if(clazz.equals(Boolean.class))
			return true;
		else if(clazz.equals(Byte.class)) 
			return true;
		else if(clazz.equals(Character.class)) 
			return true;
		else if(clazz.equals(Double.class)) 
			return true;
		else if(clazz.equals(Float.class)) 
			return true;
		else if(clazz.equals(Integer.class)) 
			return true;
		else if(clazz.equals(Long.class)) 
			return true;
		else if(clazz.equals(Short.class)) 
			return true;
		else if(clazz.equals(Void.class)) 
			return true;
		
		return false;
	}
}
