package com.dpx.core.domain.clazz;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dpx.core.utils.TextUtils;

public class DomainField {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private String name;
	private DomainFieldType type;
	private boolean isCollection;
	private List<Annotation> annotations;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public DomainField(String name, DomainFieldType type, boolean isCollection, List<Annotation> annotations) {
		super();
		this.name = name;
		this.type = type;
		this.isCollection = isCollection;
		this.annotations = annotations;
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------

	public String getName() {
		return name;
	}

	public DomainFieldType getType() {
		return type;
	}

	public boolean isCollection() {
		return isCollection;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}
	
	
	//--------------------------------------------- Derived ----------------------------------------------
	
	public String getterName() {
		return "get" + TextUtils.tranformString(name, true, true, false, "");
	}
	
	public String setterName() {
		return "set" + TextUtils.tranformString(name, true, true, false, "");
	}
	
	public boolean isEnum() {
		Pattern pattern = Pattern.compile("^[\\^]((\\w|\\s)+[|])+(\\w|\\s)+[$]$");
		Annotation a = getAnnotation("Pattern");
		if(a != null) {
			String regex = ((javax.validation.constraints.Pattern)a).regexp();
			Matcher m = pattern.matcher(regex);
			return m.find();
		}
		
		return false;
	}
	
	
	public Annotation getAnnotation(String name) {
		for(Annotation a : annotations)
			if(a.annotationType().getSimpleName().equals(name))
				return a;
		
		return null;
	}
	
	public Collection<String> getEnumValues() {
		Annotation pattern = getAnnotation("Pattern");
		String regex;
		List<String> res;
		if(pattern != null) {
			regex = ((javax.validation.constraints.Pattern)pattern).regexp();
			res = new LinkedList<String>();
		} else
			return null;
		
		regex = regex.substring(1, regex.length()-1);
		for(String s : regex.split("[\\|]"))
			res.add(s);
		
		return res;
	}
}
