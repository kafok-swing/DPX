package com.dpx.core.domain;

import java.util.LinkedList;

import com.dpx.core.domain.constrains.Constrain;

public class Relationship extends Attribute {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private String entity;
	private int type;	//indices de getTypes (abajo)
	private boolean isJoin;
	
	
	//------------------------------------------ Constructors --------------------------------------------
	
	public Relationship(String role, String entity, int type, boolean isJoin, boolean isOptative, boolean isCollection) {
		super(role, type == 4 ? AttributeType.DATATYPE : AttributeType.RELATIONSHIP, false, false, false, isOptative, new LinkedList<Constrain>(), isCollection);
		this.entity = entity;
		this.type = type;
		this.isJoin = isJoin;
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public String getEntity() {
		return entity;
	}

	public int getRelType() {
		return type;
	}

	public boolean isJoin() {
		return isJoin;
	}
	
	
	//-------------------------------------------- Operations --------------------------------------------
	
	public static String[] getTypes() {
		String res[] = new String[5];
		res[0] = "One to One";
		res[1] = "One to Many";
		res[2] = "Many to One";
		res[3] = "Many to Many";
		res[4] = "Datatype";
		return res;
	}
}
