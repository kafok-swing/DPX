package com.dpx.core.domain;

import java.util.Collection;
import java.util.List;

import com.dpx.core.Project;
import com.dpx.core.common.Pair;

public class Entity implements Comparable<Entity> {
	
	//-------------------------------------------- Atributos ---------------------------------------------

	private String name;
	private List<Attribute> attr;
	private Entity parent;
	private List<Entity> children;
	private List<Relationship> relationships;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Entity(String name, List<Attribute> attr, Entity parent, List<Entity> children, List<Relationship> relationships) {
		super();
		this.name = name;
		this.attr = attr;
		this.parent = parent;
		this.children = children;
		this.relationships = relationships;
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public String getName() {
		return name;
	}

	public List<Attribute> getAttr() {
		return attr;
	}

	public Entity getParent() {
		return parent;
	}

	public List<Entity> getChildren() {
		return children;
	}

	public List<Relationship> getRelationships() {
		return relationships;
	}
	
	public void setParent(Entity parent) {
		this.parent = parent;
	}

	
	//---------------------------------------------- Object ----------------------------------------------
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	//-------------------------------------------- Operations --------------------------------------------
	
	public static Pair<Integer, String[]> getEntitiesAndPos(Project p, Entity ent) {
		String res[] = new String[p.getEntities().size()];
		int i = 0;
		int pos = -1;
		for(Entity e : p.getEntities()) {
			res[i] = e.getName();
			if(ent != null && ent.getParent() != null && e.getName().equals(ent.getParent().getName()))
				pos = i;
			i++;
		}
		
		return new Pair<Integer, String[]>(pos, res);
	}
	
	public static Pair<Integer, String[]> getEntitiesAndPos(Project p, String ent) {
		String res[] = new String[p.getEntities().size()];
		int i = 0;
		int pos = -1;
		for(Entity e : p.getEntities()) {
			res[i] = e.getName();
			if(ent != null && e.getName().equals(ent))
				pos = i;
			i++;
		}
		
		return new Pair<Integer, String[]>(pos, res);
	}
	
	public static Entity getEntityByName(Collection<Entity> ent, String name) {
		for(Entity e : ent) {
			if(e.getName().equals(name))
				return e;
		}
		
		return null;
	}


	//-------------------------------------------- Comparable --------------------------------------------
	
	@Override
	public int compareTo(Entity ent) {
		return name.compareTo(ent.getName());
	}
}
