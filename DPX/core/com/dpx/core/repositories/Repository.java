package com.dpx.core.repositories;

import java.util.List;

import com.dpx.core.Project;
import com.dpx.core.domain.Entity;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.view.main.DpxFrame;

public class Repository implements Comparable<Repository> {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private Entity entity;
	private List<Query> queries;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Repository(Entity entity, List<Query> queries) {
		super();
		this.entity = entity;
		this.queries = queries;
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public Entity getEntity() {
		return entity;
	}
	public List<Query> getQueries() {
		return queries;
	}

	
	//--------------------------------------------- Equality ---------------------------------------------
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.getName().hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Repository other = (Repository) obj;
		if (entity == null) {
			if (other.entity != null)
				return false;
		} else if (!entity.getName().equals(other.entity.getName()))
			return false;
		return true;
	}


	//-------------------------------------------- Comparable --------------------------------------------
	
	@Override
	public int compareTo(Repository o) {
		return entity.getName().compareTo(o.getEntity().getName());
	}
	
	
	//-------------------------------------------- Save file ---------------------------------------------
	
	public static boolean saveFile(Project p, String name) {
		boolean res = TextUtils.saveTextFile(p.getProjectPath() + "src/main/java/repositories/" + name + "Repository.java", getContentFile(p, name));
		DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(p);
		return res;
	}
	
	public static String getContentFile(Project p, String name) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		n = n.substring(0, 1);
		
		String findActor = "\n\t@Query(\"select " + n + " from " + name + " " + n + " where " + n + ".userAccount.id = ?1\")\n"
				+ "\t" + name + " findByUserAcconuntId(int id);\n\n";
		
		boolean isActor = p.getClasses().get(name).hasFieldType("UserAccount");
		
		return "package repositories;\n"
				+ "\n"
				+ "import org.springframework.data.jpa.repository.JpaRepository;\n"
				+ (isActor ? "import org.springframework.data.jpa.repository.Query;" : "")
				+ "import org.springframework.stereotype.Repository;\n"
				+ "\n"
				+ "import domain." + name + ";\n"
				+ "\n"
				+ "@Repository\n"
				+ "public interface " + name + "Repository extends JpaRepository<" + name + ", Integer> {\n"
				+ (isActor ? findActor : "\t\n")
				+ "}\n";
	}
}
