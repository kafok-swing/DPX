package com.dpx.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.dpx.core.common.Pair;
import com.dpx.core.domain.Entity;
import com.dpx.core.domain.clazz.DomainClass;
import com.dpx.core.repositories.Repository;
import com.dpx.core.services.Service;
import com.dpx.core.services.Test;

public class Project {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private String projectPath;
	private String name;
	
	private Map<String, DomainClass> classes;
	private Map<String, DomainClass> formClasses;
	private Set<Entity> entities;
	private Set<Repository> repos;
	private Set<Service> service;
	private Set<Test> tests;
	private List<String> authorities;

	
	//------------------------------------------- Constructors -------------------------------------------

	public Project(String projectPath, String name) {
		super();
		this.projectPath = projectPath;
		this.name = name;
		this.entities = new HashSet<Entity>();
	}
	
	public Project(String projectPath, String name, Set<Entity> entities) {
		super();
		this.projectPath = projectPath;
		this.name = name;
		this.entities = entities;
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------

	public String getProjectPath() {
		return projectPath;
	}

	public String getName() {
		return name;
	}

	public Set<Entity> getEntities() {
		return entities;
	}
	
	public Set<Repository> getRepos() {
		return repos;
	}

	public void setRepos(Set<Repository> repos) {
		this.repos = repos;
	}
	
	public Set<Service> getService() {
		return service;
	}

	public Map<String, DomainClass> getClasses() {
		return classes;
	}
	
	public DomainClass getAnyClass(String name) {
		DomainClass res = classes.get(name);
		if(res == null)
			res = formClasses.get(name);
		
		return res;
	}

	public void setClasses(Map<String, DomainClass> classes) {
		this.classes = classes;
	}

	public void setService(Set<Service> service) {
		this.service = service;
	}
	
	public void setTests(Set<Test> tests) {
		this.tests = tests;
	}

	public Set<Test> getTests() {
		return tests;
	}
	
	public Map<String, DomainClass> getFormClasses() {
		return formClasses;
	}

	public void setFormClasses(Map<String, DomainClass> formClasses) {
		this.formClasses = formClasses;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
	
	
	//-------------------------------------------- Operation ---------------------------------------------
	
	public Entity getEntityByName(String name) {
		for(Entity e : entities)
			if(e.getName().equals(name))
				return e;
		
		return null;
	}
	
	public Repository getRespositoryByEntityName(String name) {
		for(Repository e : repos)
			if(e.getEntity().getName().equals(name))
				return e;
		
		return null;
	}
	
	public Collection<Pair<String, Boolean>> getRepositoriesRemain() {
		Set<Pair<String, Boolean>> res = new TreeSet<Pair<String, Boolean>>(Pair.<String, Boolean>getComparatorByFirst());
		
		for(Entity r : entities) {
			res.add(new Pair<String, Boolean>(r.getName() + "Repository", getRespositoryByEntityName(r.getName()) != null));
		}
		
		return res;
	}
	
	public Service getServiceByEntityName(String name) {
		for(Service e : service)
			if(e.getEntity().getName().equals(name))
				return e;
		
		return null;
	}
	
	public Test getTestsByEntityName(String name) {
		for(Test e : tests)
			if(e.getEntity().getName().equals(name))
				return e;
		
		return null;
	}
	
	public Collection<Pair<String, Boolean>> getServicesRemain() {
		Set<Pair<String, Boolean>> res = new TreeSet<Pair<String, Boolean>>(Pair.<String, Boolean>getComparatorByFirst());
		
		for(Entity r : entities)
			res.add(new Pair<String, Boolean>(r.getName() + "Service", getServiceByEntityName(r.getName()) != null));
		
		return res;
	}
	
	public Collection<Pair<String, Boolean>> getTestsRemain() {
		Set<Pair<String, Boolean>> res = new TreeSet<Pair<String, Boolean>>(Pair.<String, Boolean>getComparatorByFirst());
		
		for(Entity r : entities)
			res.add(new Pair<String, Boolean>(r.getName() + "ServiceTest", getTestsByEntityName(r.getName()) != null));
		
		return res;
	}
	
	public Collection<String> getEntitiesNames() {
		List<String> res = new LinkedList<String>();
		for(Entity e : entities)
			res.add(e.getName());
		
		return res;
	}
	
	public String[] getArrayDomainClass() {
		int i = 0;
		for(Map.Entry<String, DomainClass> c : classes.entrySet())
			if(!c.getValue().isAbstract())
				i++;
		
		String[] res = new String[i];
		
		i = 0;
		for(Map.Entry<String, DomainClass> c : classes.entrySet()) {
			if(!c.getValue().isAbstract()) {
				res[i] = c.getKey();
				i++;
			}
		}
		
		return res;
	}
	
	public String[] getArrayAllDomainClass() {
		String[] res = new String[classes.size()];
		int i = 0;
		for(Map.Entry<String, DomainClass> c : classes.entrySet()) {
			res[i] = c.getKey();
			i++;
		}
		
		return res;
	}
	
	public String[] getArrayDomainAndFormClass() {
		Set<String> set = new TreeSet<String>();
		for(Map.Entry<String, DomainClass> c : classes.entrySet())
			if(!c.getValue().isAbstract())
				set.add(c.getValue().getName());
		
		for(Map.Entry<String, DomainClass> c : formClasses.entrySet())
			set.add(c.getValue().getName());
		
		String[] res = new String[set.size()];
		
		int i = 0;
		for(String c : set) {
			res[i] = c;
			i++;
		}
		
		return res;
	}

	public String[] getArrayAllDomainAndFormClass() {
		String[] res = new String[classes.size() + formClasses.size()];
		
		int i = 0;
		for(DomainClass c : classes.values()) {
			res[i] = c.getName();
			i++;
		}
		
		for(DomainClass c : formClasses.values()) {
			res[i] = c.getName();
			i++;
		}
		
		return res;
	}
}
