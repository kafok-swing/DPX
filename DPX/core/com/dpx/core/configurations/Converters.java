package com.dpx.core.configurations;

import java.io.PrintWriter;
import java.util.Collection;

import com.dpx.core.Project;
import com.dpx.core.utils.TextUtils;

public class Converters {
	
	public static boolean saveFile(String text, String path) {
		try {
			PrintWriter out = new PrintWriter(path);
			out.print(text);
			out.close();
		} catch(Throwable e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	public static boolean saveAll(Project p, Collection<String> conv) {
		boolean res = true;
		for(String name : conv) {
			if(!saveFile(getContentFileXToString(name), p.getProjectPath() + "src/main/java/converters/" + name + "ToStringConverter.java"))
				res = false;
			if(!saveFile(getContentFileStringToX(name), p.getProjectPath() + "src/main/java/converters/StringTo" + name + "Converter.java"))
				res = false;
		}
		
		if(!saveFile(getContentFileAuthorityToString(), p.getProjectPath() + "src/main/java/converters/AuthorityToStringConverter.java"))
			res = false;
		if(!saveFile(getContentFileStringToAuthority(), p.getProjectPath() + "src/main/java/converters/StringToAuthorityConverter.java"))
			res = false;
		
		if(!saveFile(getContentFileConfig(conv), p.getProjectPath() + "src/main/resources/spring/config/converters.xml"))
			res = false;
		
		return res;
	}
	
	
	public static String getContentFileStringToAuthority() {
		return "package converters;\n"
				+ "\n"
				+ "import org.springframework.core.convert.converter.Converter;\n"
				+ "import org.springframework.stereotype.Component;\n"
				+ "import org.springframework.transaction.annotation.Transactional;\n"
				+ "import org.springframework.util.StringUtils;\n"
				+ "\n"
				+ "import security.Authority;\n"
				+ "\n"
				+ "@Component\n"
				+ "@Transactional\n"
				+ "public class StringToAuthorityConverter implements Converter<String, Authority> {\n"
				+ "\n"
				+ "\t@Override\n"
				+ "\tpublic Authority convert(String text) {\n"
				+ "\t\tAuthority auth;\n"
				+ "\t\tauth=new Authority();\n"
				+ "\n"
				+ "\t\ttry {\n"
				+ "\t\t\tif (StringUtils.isEmpty(text)) {\n"
				+ "\t\t\t\tauth = null;\n"
				+ "\t\t\t} else {\n"
				+ "\t\t\t\tauth.setAuthority(text);\n"
				+ "\t\t\t}\n"
				+ "\t\t} catch (Throwable oops) {\n"
				+ "\t\t\tthrow new IllegalArgumentException(oops);\n"
				+ "\t\t}\n"
				+ "\n"
				+ "\t\treturn auth;\n"
				+ "\t}\n"
				+ "\n"
				+ "}\n";
	}
	
	public static String getContentFileAuthorityToString() {
		return "package converters;\n"
				+ "\n"
				+ "import org.springframework.core.convert.converter.Converter;\n"
				+ "import org.springframework.stereotype.Component;\n"
				+ "import org.springframework.transaction.annotation.Transactional;\n"
				+ "\n"
				+ "import security.Authority;\n"
				+ "\n"
				+ "@Component\n"
				+ "@Transactional\n"
				+ "public class AuthorityToStringConverter implements Converter<Authority, String> {\n"
				+ "\n"
				+ "\t@Override\n"
				+ "\tpublic String convert(Authority authority) {\n"
				+ "\t\tString result;\n"
				+ "\n"
				+ "\t\tif (authority == null) {\n"
				+ "\t\t\tresult = null;\n"
				+ "\t\t} else {\n"
				+ "\t\t\tresult = authority.getAuthority();\n"
				+ "\t\t}\n"
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\n"
				+ "}\n";
	}
	
	public static String getContentFileStringToX(String name) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		return "package converters;\n"
				+ "\n"
				+ "import org.springframework.beans.factory.annotation.Autowired;\n"
				+ "import org.springframework.core.convert.converter.Converter;\n"
				+ "import org.springframework.stereotype.Component;\n"
				+ "import org.springframework.transaction.annotation.Transactional;\n"
				+ "import org.springframework.util.StringUtils;\n"
				+ "\n"
				+ "import repositories." + name + "Repository;\n"
				+ "import domain." + name + ";\n"
				+ "\n"
				+ "@Component\n"
				+ "@Transactional\n"
				+ "public class StringTo" + name + "Converter implements Converter<String, " + name + ">{\n"
				+ "\t\n"
				+ "\t@Autowired\n"
				+ "\tprivate " + name + "Repository " + n + "Repository;\n"
				+ "\t\n"
				+ "\t@Override\n"
				+ "\tpublic " + name + " convert(String text) {\n"
				+ "\t\t" + name + " result;\n"
				+ "\t\tint " + n + "Id;\n"
				+ "\t\t\n"
				+ "\t\ttry{\n"
				+ "\t\t\tif(StringUtils.isEmpty(text)){\n"
				+ "\t\t\t\tresult = null;\n"
				+ "\t\t\t} else{\n"
				+ "\t\t\t\t" + n + "Id = Integer.valueOf(text);\n"
				+ "\t\t\t\tresult = " + n + "Repository.findOne(" + n + "Id);\n"
				+ "\t\t\t}\n"
				+ "\t\t} catch (Throwable oops) {\n"
				+ "\t\t\tthrow new IllegalArgumentException(oops);\n"
				+ "\t\t}\n"
				+ "\t\t\n"
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\n"
				+ "}\n";
	}
	
	public static String getContentFileXToString(String name) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		return "package converters;\n"
				+ "\n"
				+ "import org.springframework.core.convert.converter.Converter;\n"
				+ "import org.springframework.stereotype.Component;\n"
				+ "import org.springframework.transaction.annotation.Transactional;\n"
				+ "\n"
				+ "import domain." + name + ";\n"
				+ "\n"
				+ "@Component\n"
				+ "@Transactional\n"
				+ "public class " + name + "ToStringConverter implements Converter<" + name + ", String>{\n"
				+ "\n"
				+ "\t@Override\n"
				+ "\tpublic String convert(" + name + " " + n + ") {\n"
				+ "\t\tString result;\n"
				+ "\t\t\n"
				+ "\t\tif(" + n + " == null){\n"
				+ "\t\t\tresult = null;\n"
				+ "\t\t}\n"
				+ "\t\telse{\n"
				+ "\t\t\tresult = String.valueOf(" + n + ".getId());\n"
				+ "\t\t}\n"
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\n"
				+ "}\n";
	}
	
	public static String getContentFileConfig(Collection<String> conv) {
		
		if(!conv.contains("Authority"))
			conv.add("Authority");
		
		String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "\n"
				+ "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n"
				+ "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:util=\"http://www.springframework.org/schema/util\"\n"
				+ "\txsi:schemaLocation=\"\n"
				+ "\t\thttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd\t\t\n"
				+ "\t\thttp://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd\t\t\n"
				+ "\t\">\n"
				+ "\n"
				+ "\t<util:list id=\"converters\">\n";
		for(String s : conv) {
			str += ("\t\t<bean class=\"converters." + s + "ToStringConverter\"/>\n"
				+ "\t\t<bean class=\"converters.StringTo" + s + "Converter\"/>\n"
				+ "\t\t\n");
		}
		
		str += ("\t</util:list>\n"
				+ "\t\n"
				+ "</beans>\n");
		
		return str;
	}
}
