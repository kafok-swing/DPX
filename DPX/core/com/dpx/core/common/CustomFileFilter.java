package com.dpx.core.common;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class CustomFileFilter extends FileFilter {

	//-------------------------------------------- Attributes --------------------------------------------
	
	private String[] extensions;
	private String description;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public CustomFileFilter(String description, String... extensions) {
		this.extensions = extensions;
		this.description = description;
	}
	
	
	//-------------------------------------------- Interface ---------------------------------------------
	
	@Override
	public boolean accept(File f) {
		for(String s : extensions) {
			if(f.isDirectory() || f.getName().toLowerCase().endsWith(s))
				return true;
		}
		
		return false;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

}
