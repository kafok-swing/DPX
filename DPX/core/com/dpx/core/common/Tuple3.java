package com.dpx.core.common;

public class Tuple3 <T, R, S> {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	T first;
	R second;
	S third;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Tuple3(T first, R second, S third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}


	//----------------------------------------- Getters-Setters ------------------------------------------
	
	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public R getSecond() {
		return second;
	}

	public void setSecond(R second) {
		this.second = second;
	}
	
	public S getThird() {
		return third;
	}

	public void setThird(S third) {
		this.third = third;
	}

	
	//--------------------------------------------- toString ---------------------------------------------
	
	@Override
	public String toString() {
		return "Tuple3 [first=" + first + ", second=" + second + ", third=" + third + "]";
	}
	
}
