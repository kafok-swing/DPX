package com.dpx.core.common;

public interface Consumer<T> {
	void run(T param);
}
