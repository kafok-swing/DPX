package com.dpx.core.common;

public class Tuple4 <T, R, S, U> {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	T first;
	R second;
	S third;
	U fourth;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Tuple4(T first, R second, S third, U fourth) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
	}


	//----------------------------------------- Getters-Setters ------------------------------------------

	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public R getSecond() {
		return second;
	}

	public void setSecond(R second) {
		this.second = second;
	}
	
	public S getThird() {
		return third;
	}

	public void setThird(S third) {
		this.third = third;
	}
	
	public U getFourth() {
		return fourth;
	}

	public void setFourth(U fourth) {
		this.fourth = fourth;
	}

	
	//--------------------------------------------- toString ---------------------------------------------
	
	@Override
	public String toString() {
		return "Tuple4 [first=" + first + ", second=" + second + ", third=" + third + ", fourth=" + fourth + "]";
	}
	
}
