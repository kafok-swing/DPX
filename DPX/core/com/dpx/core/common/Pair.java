package com.dpx.core.common;

import java.util.Comparator;

public class Pair <T, R> {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	T first;
	R second;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Pair(T first, R second) {
		super();
		this.first = first;
		this.second = second;
	}

	
	//----------------------------------------- Getters-Setters ------------------------------------------
	
	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public R getSecond() {
		return second;
	}

	public void setSecond(R second) {
		this.second = second;
	}
	
	
	//--------------------------------------------- toString ---------------------------------------------
	
	public String toString() {
		return "[" + first.toString() + ", " + second.toString() + "]";
	}
	
	
	//-------------------------------------------- Comparator --------------------------------------------
	
	public static <T extends Comparable<T>, R> Comparator<Pair <T, R>> getComparatorByFirst() {
		return new FirstComparator<T, R>();
	}
	
	public static <T, R extends Comparable<R>> Comparator<Pair <T, R>> getComparatorBySecond() {
		return new SecondComparator<T, R>();
	}
	
	private static class FirstComparator<T extends Comparable<T>, R> implements Comparator<Pair <T, R>> {
		@Override
		public int compare(Pair<T, R> o1, Pair<T, R> o2) {
			return o1.getFirst().compareTo(o2.getFirst());
		}
	}
	
	private static class SecondComparator<T, R extends Comparable<R>> implements Comparator<Pair <T, R>> {
		@Override
		public int compare(Pair<T, R> o1, Pair<T, R> o2) {
			return o1.getSecond().compareTo(o2.getSecond());
		}
	}
}
