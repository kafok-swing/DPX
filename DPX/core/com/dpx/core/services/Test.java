package com.dpx.core.services;

import java.io.PrintWriter;

import com.dpx.core.Project;
import com.dpx.core.domain.Entity;
import com.dpx.core.utils.TextUtils;

public class Test implements Comparable<Test> {
	
	private Entity entity;
	
	public Test(Entity entity) {
		super();
		this.entity = entity;
	}

	
	public Entity getEntity() {
		return entity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.getName().hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (entity == null) {
			if (other.getEntity() != null)
				return false;
		} else if (!entity.getName().equals(other.getEntity().getName()))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Test arg0) {
		return entity.getName().compareTo(arg0.getEntity().getName());
	}

	
	
	public static boolean saveFile(Project p, String name) {
		try {
			PrintWriter out = new PrintWriter(p.getProjectPath() + "src/test/java/services/" + name + "ServiceTest.java");
			out.print(getContentFile(p, name));
			out.close();
		} catch(Throwable e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static String getContentFile(Project p, String name) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		
		return "package services;\n"
				+ "\n"
				+ "import java.util.Collection;\n"
				+ "\n"
				+ "import javax.transaction.Transactional;\n"
				+ "\n"
				+ "import org.junit.Assert;\n"
				+ "import org.junit.Test;\n"
				+ "import org.junit.runner.RunWith;\n"
				+ "import org.springframework.beans.factory.annotation.Autowired;\n"
				+ "import org.springframework.test.context.ContextConfiguration;\n"
				+ "import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;\n"
				+ "\n"
				+ "import utilities.AbstractTest;\n"
				+ "import domain." + name + ";\n"
				+ "\n"
				+ "@RunWith(SpringJUnit4ClassRunner.class)\n"
				+ "@ContextConfiguration(locations = {\n"
				+ "\t\t\"classpath:spring/datasource.xml\",\n"
				+ "\t\t\"classpath:spring/config/packages.xml\" })\n"
				+ "@Transactional\n"
				+ "public class " + name + "ServiceTest extends AbstractTest {\n"
				+ "\t\n"
				+ "\t//---------------------------------------- Service under test ----------------------------------------\n"
				+ "\t\n"
				+ "\t@Autowired\n"
				+ "\tprivate " + name + "Service " + n + "Service; \n"
				+ "\t\n"
				+ "\t//--------------------------------------- Supporting services ----------------------------------------\n"
				+ "\t\n"
				+ "\t\n"
				+ "\t\n"
				+ "\t\n"
				+ "\t//----------------------------------------------- Test -----------------------------------------------\n"
				+ "\t\n"
				+ "\t@Test\n"
				+ "\tpublic void test1() {\n"
				+ "\t\tauthenticate(\"\");\n"
				+ "\t\t\n"
				+ "\t\t//TODO\n"
				+ "\t\t\n"
				+ "\t\tauthenticate(null);\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\t@Test(expected=java.lang.IllegalArgumentException.class)\n"
				+ "\tpublic void test2() {\n"
				+ "\t\tauthenticate(\"\");\n"
				+ "\t\t\n"
				+ "\t\t//TODO\n"
				+ "\t\t\n"
				+ "\t\tauthenticate(null);\n"
				+ "\t}\n"
				+ "}\n";
	}
	
}
