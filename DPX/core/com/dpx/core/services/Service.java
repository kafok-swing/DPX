package com.dpx.core.services;

import java.util.Collection;

import com.dpx.core.Project;
import com.dpx.core.domain.Entity;
import com.dpx.core.domain.clazz.DomainClass;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.view.main.DpxFrame;

public class Service implements Comparable<Service> {
	
	private Entity entity;

	public Service(Entity entity) {
		super();
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}
	
	//--------------------------------------------- Equality ---------------------------------------------
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.getName().hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (entity == null) {
			if (other.getEntity() != null)
				return false;
		} else if (!entity.getName().equals(other.getEntity().getName()))
			return false;
		return true;
	}


	//-------------------------------------------- Comparable --------------------------------------------
	
	@Override
	public int compareTo(Service o) {
		return entity.getName().compareTo(o.getEntity().getName());
	}
	
	
	//-------------------------------------------- Save file ---------------------------------------------
	
	public static boolean saveFile(Project p, String name) {
		boolean res = TextUtils.saveTextFile(p.getProjectPath() + "src/main/java/services/" + name + "Service.java", getContentFile(p, name));
		DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(p);
		return res;
	}
	
	public static String getCreate(Project p, String name) {
		String createSetters = "";
		for(DomainField field : p.getClasses().get(name).getCollectionField())
			createSetters += ("\t\tresult." + field.setterName() + "(new ArrayList<" + field.getType().getName() + ">());\n");
		createSetters += "\n";
		
		String create = "\t//---------------------------------------------- Create ----------------------------------------------\n"
				+ "\t\n"
				+ "\tpublic " + name + " create() {\n"
				+ "\t\t" + name + " result;\n"
				+ "\t\tresult = new " + name + "();\n\n"
				+ createSetters
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\n";
		
		return create;
	}
	
	public static String getReconstruct(Project p, String nameClass, String name, Collection<DomainField> nameFields) {
		String n = TextUtils.tranformString(nameClass, false, true, false, "");
		
		String reconstructSetters = "";
		String createSetters = "";
		DomainClass formClass = p.getClasses().get(nameClass);
		if(formClass != null) {
			for(DomainField field : nameFields) {
				if(nameFields.contains(field) && formClass.hasFieldName(field.getName()))
					reconstructSetters += ("\t\t\tresult." + field.setterName() + "(" + n +  "." + field.getterName() + "());\n");
			}
			
			for(DomainField field : formClass.getAllField()) {
				if(field.isCollection() && !nameFields.contains(field))
					createSetters += ("\t\t\tresult." + field.setterName() + "(new ArrayList<" + field.getType().getName() + ">());\n");
			}
			
			if(!createSetters.equals(""))
				createSetters = createSetters.substring(3, createSetters.length());
		}
		
		String reconstruct = "\t//------------------------------------------- Reconstruct --------------------------------------------\n"
				+ "\t\n"
				+ "\tpublic " + nameClass + " reconstruct(" + name + " " + n + ", BindingResult binding) {\n"
				+ "\t\t" + nameClass + " result;\n"
				+ "\t\t\n"
				+ "\t\tif(" + n + ".getId() == 0) {\n"
				+ (name.equals(nameClass) ? "\t\t\tresult = " + n + ";\n\n" : "\t\t\tresult = new " + nameClass + "();\n\n")
				+ (createSetters.equals("") ? "" : "\t\t\t" + createSetters)
				+ "\t\t} else {\n"
				+ "\t\t\tresult = " + n + "Repository.findOne(" + n + ".getId());\n\n"
				+ reconstructSetters
				+ "\t\t}\n"
				+ "\t\t\n"
				+ "\t\tvalidator.validate(result, binding);\n"
				+ "\t\t\n"
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\t\n";
		
		return reconstruct;
	}
	
	public static String getContentFile(Project p, String name) {
		String n = TextUtils.tranformString(name, false, true, false, "");
		
		String createSetters = "";
		String imports = "";
		DomainClass clazz = p.getClasses().get(name);
		for(DomainField field : clazz.getCollectionField()) {
			createSetters += ("\t\tresult." + field.setterName() + "(new ArrayList<" + field.getType().getName() + ">());\n");
			if(field.getType().isOtherClass())
				imports += ("import domain." + field.getType().getName() + ";\n");
		}
		
		String create = "\t//---------------------------------------------- Create ----------------------------------------------\n"
				+ "\t\n"
				+ "\tpublic " + name + " create() {\n"
				+ "\t\t" + name + " result;\n"
				+ "\t\tresult = new " + name + "();\n\n"
				+ createSetters + "\n"
				+ "\t\treturn result;\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\n";
		
		DomainClass formClass = p.getFormClasses().get(name + "Form");
		String reconstruct = formClass != null ? getReconstruct(p, name, name + "Form", formClass.getAllField()) : "";
		String importForm = formClass != null ? "import forms." + name + "Form;" : "";
		
		boolean isActor = p.getClasses().get(name).hasFieldType("UserAccount");
		
		String findByPrincipal = "\tpublic " + name + " findByPrincipal() {\n"
				+ "\t\tUserAccount userAccount;\n"
				+ "\t\t" + name + " res;\n"
				+ "\n"
				+ "\t\tuserAccount = LoginService.getPrincipal();\n"
				+ "\t\tAssert.notNull(userAccount);\n"
				+ "\n"
				+ "\t\tres = findByUserAccount(userAccount);\n"
				+ "\t\tAssert.notNull(res);\n"
				+ "\n"
				+ "\t\treturn res;\n"
				+ "\t}\n"
				+ "\n"
				+ "\tpublic " + name + " findByUserAccount(UserAccount userAccount) {\n"
				+ "\t\t" + name + " res;\n"
				+ "\t\tres = " + n + "Repository.findByUserAcconuntId(userAccount.getId());\n"
				+ "\t\tAssert.notNull(res);\n"
				+ "\t\treturn res;\n"
				+ "\t}\n";
		
		return "package services;\n"
				+ "\n"
				+ "import java.util.ArrayList;\n"
				+ "import java.util.Collection;\n"
				+ "\n"
				+ "import org.springframework.beans.factory.annotation.Autowired;\n"
				+ "import org.springframework.stereotype.Service;\n"
				+ "import org.springframework.transaction.annotation.Transactional;\n"
				+ "import org.springframework.util.Assert;\n"
				+ (formClass != null ? "import org.springframework.validation.BindingResult;\n" : "")
				+ "import org.springframework.validation.Validator;\n"
				+ "\n"
				+ "import domain." + name + ";\n"
				+ imports
				+ importForm + "\n"
				+ "import repositories." + name + "Repository;\n"
				+ (isActor ? "import security.LoginService;\nimport security.UserAccount;\n" : "")
				+ "\n"
				+ "@Service\n"
				+ "@Transactional\n"
				+ "public class " + name + "Service {\n"
				+ "\n"
				+ "\t//---------------------------------------- Managed repository ----------------------------------------\n"
				+ "\t\n"
				+ "\t@Autowired\n"
				+ "\tprivate " + name + "Repository " + n + "Repository;\n"
				+ "\t\n"
				+ "\n"
				+ "\t//--------------------------------------- Supporting services ----------------------------------------\n"
				+ "\n\n\n"
				+ "\t//-------------------------------------------- Validator ---------------------------------------------\n"
				+ "\t\n"
				+ "\t@Autowired\n"
				+ "\tprivate Validator validator;\n"
				+ "\t\n"
				+ "\n"
				+ (formClass != null ? reconstruct : "")
				+ (clazz.isAbstract() ? "" : create)
				+ "\t//------------------------------------------- Constructors -------------------------------------------\n"
				+ "\n"
				+ "\tpublic " + name + "Service() {\n"
				+ "\t\tsuper();\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\t\n"
				+ "\t//------------------------------------------- CRUD methods -------------------------------------------\n"
				+ "\n"
				+ "\tpublic Collection<" + name + "> findAll() {\n"
				+ "\t\treturn " + n + "Repository.findAll();\n"
				+ "\t}\n"
				+ "\n"
				+ "\tpublic " + name + " findOne(int " + n + "Id) {\n"
				+ "\t\treturn " + n + "Repository.findOne(" + n + "Id);\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\tpublic " + name + " save(" + name + " " + n + ") {\n"
				+ "\t\t" + name + " res;\n"
				+ "\t\tAssert.notNull(" + n + ");\n"
				+ "\t\tres = " + n + "Repository.save(" + n + ");\n"
				+ "\t\treturn res;\n"
				+ "\t}\n"
				+ "\t\n"
				+ "\tpublic void delete(" + name + " " + n + ") {\n"
				+ "\t\tAssert.notNull(" + n + ");\n"
				+ "\t\t" + n + "Repository.delete(" + n + ");\n"
				+ "\t}\n"
				+ "\t\n\n"
				+ "\t//-------------------------------------- Other Business methods --------------------------------------\n\n"
				+ (isActor ? findByPrincipal : "\t")
				+ "\n}\n";
	}
}
