package com.dpx.core;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.dpx.core.common.Pair;
import com.dpx.core.domain.Attribute;
import com.dpx.core.domain.AttributeType;
import com.dpx.core.domain.Entity;
import com.dpx.core.domain.Relationship;
import com.dpx.core.domain.clazz.DomainClass;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.domain.clazz.DomainFieldType;
import com.dpx.core.domain.constrains.Constrain;
import com.dpx.core.domain.constrains.ConstrainDigit;
import com.dpx.core.domain.constrains.ConstrainLength;
import com.dpx.core.domain.constrains.ConstrainMax;
import com.dpx.core.domain.constrains.ConstrainMin;
import com.dpx.core.domain.constrains.ConstrainPattern;
import com.dpx.core.domain.constrains.ConstrainRange;
import com.dpx.core.domain.constrains.ConstrainSize;
import com.dpx.core.domain.constrains.ConstrainType;
import com.dpx.core.repositories.Repository;
import com.dpx.core.services.Service;
import com.dpx.core.services.Test;
import com.dpx.core.utils.TextUtils;
import com.dpx.resource.ResourceManager;

public class ProjectFactory {

	public static Project getProject(String path) {
		try {
			File xml = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xml);
				
			doc.normalize();	//tardara, pero hay que hacerlo
			
			String name = null;
			NodeList node = doc.getElementsByTagName("projectDescription").item(0).getChildNodes();
			for(int i=0; i<node.getLength(); i++) {
				Node n = node.item(i);
				if(n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("name")) {
					name = n.getFirstChild().getNodeValue();
					break;
				}
			}
			
			if(name == null) 
				throw new IllegalArgumentException("File \"" + path +"\" is not valid!");
			else {
				path = path.replace(".project", "");
				
				Project res = new Project(path.replace(".project", ""), name, getEntities(path));
				res.setClasses(getClasses(path, "domain"));
				res.setFormClasses(getClasses(path, "forms"));
				res.setRepos(getRepositories(res, path));
				res.setService(getServices(res, path));
				res.setTests(getTests(res, path));
				res.setAuthorities(getAuthorities(res, path));
				
				return res;
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static Map<String, DomainClass> getClasses(String path, String dir) {
		Map<String, DomainClass> classes = new TreeMap<String, DomainClass>();
		Map<DomainClass, String> parents = new HashMap<DomainClass, String>();
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\classes");	//TODO configuracion
			for(File f : ResourceManager.scanFiles(path + "target\\classes\\" + dir)) {		//TODO configuracion
				Class<?> clazz = loader.loadClass(dir + "." + f.getName().replace(".class", ""));	//TODO configuracion
				
				//atributos
				List<DomainField> attrs = new LinkedList<DomainField>();
				for(Field field : clazz.getDeclaredFields()) {
					if(Modifier.isStatic(field.getModifiers()))	//solo los no estaticos
						continue;
					
					//si es coleccion
					boolean isCollection = false;
					Class<?> type = field.getType();
					if(type.getSimpleName().equals("Collection")) {
						isCollection = true;
						ParameterizedType parametized = (ParameterizedType) field.getGenericType();
				        type = (Class<?>) parametized.getActualTypeArguments()[0];
					}
					
					//tipo
					boolean isWrapper = DomainFieldType.isWrapper(type);
					boolean isOther = type.getSimpleName().equals("String") || type.getSimpleName().equals("Date") || type.isPrimitive() || isWrapper ? false : true;
					
					//anotaciones
					List<Annotation> anns = new LinkedList<Annotation>();
					try {
						Method m = clazz.getMethod("get" + TextUtils.tranformString(field.getName(), true, true, false, ""));
						for(Annotation a : m.getAnnotations())
							anns.add(a);
					} catch(Throwable t) {
					}
					
					//a�adimos
					DomainFieldType dtype = new DomainFieldType(type.getSimpleName(), type.isPrimitive(), isWrapper, isOther);
					attrs.add(new DomainField(field.getName(), dtype, isCollection, anns));
				}
				
				//clase
				DomainClass res = new DomainClass(clazz.getSimpleName(), null, attrs, clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers()));
				classes.put(clazz.getSimpleName(), res);
				
				if(clazz.getSuperclass() != null)
					parents.put(res, clazz.getSuperclass().getSimpleName());
			}
			
			//parents
			for(Map.Entry<DomainClass, String> c : parents.entrySet())
				c.getKey().setParent(classes.get(c.getValue()));
			
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return classes;
	}
	
	private static Set<Entity> getEntities(String path) {
		Set<Entity> res = new TreeSet<Entity>();
		
		Pattern pattern = Pattern.compile("[\\^]([A-Z]+[|]?)*[A-Z][$]");
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\classes");
			List<Pair<Entity, String>> parents = new LinkedList<Pair<Entity, String>>();
			for(File f : ResourceManager.scanFiles(path + "target\\classes\\domain")) {
				Class<? extends Object> clazz = loader.loadClass("domain." + f.getName().replace(".class", ""));
				
				//atributos
				List<Attribute> attrs = new LinkedList<Attribute>();
				for(Field field : clazz.getDeclaredFields()) {
					if(Modifier.isStatic(field.getModifiers()))
						continue;
					
					boolean version = field.isAnnotationPresent(Version.class);
					boolean id = field.isAnnotationPresent(Id.class);
					
					AttributeType type = null;
					boolean optative = false;
					int relType = 0;
					boolean isJoin = false;
					
					String simpleName = field.getType().getSimpleName();
					boolean isCollection = false;
					if(simpleName.equals("Collection")) {
						ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
				        Class<?> stringListClass = (Class<?>) stringListType.getActualTypeArguments()[0];
				        simpleName = stringListClass.getSimpleName();
				        isCollection = true;
					}
					
					switch(simpleName.toLowerCase()) {
						case "boolean":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.BOOLEAN;
							break;
						case "byte":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.BYTE;
							break;
						case "character":
						case "char":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.CHAR;
							break;
						case "date":
							type = AttributeType.DATE;
							break;
						case "double":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.DOUBLE;
							break;
						case "float":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.FLOAT;
							break;
						case "integer":
						case "int":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.INT;
							break;
						case "long":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.LONG;
							break;
						case "short":
							optative = Character.isUpperCase(simpleName.charAt(0));
							type = AttributeType.SHORT;
							break;
						case "string":
							Method m = clazz.getMethod("get" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1, field.getName().length()));
							
							if(m.isAnnotationPresent(javax.validation.constraints.Pattern.class)) {
								Matcher matcher = pattern.matcher(m.getAnnotation(javax.validation.constraints.Pattern.class).regexp());
								if(matcher.matches())
									type = AttributeType.ENUM;
								else
									type = AttributeType.STRING;
							} else
								type = AttributeType.STRING;
							
							break;
						default:
							if(field.getType().isAnnotationPresent(javax.persistence.Entity.class))
								type = AttributeType.RELATIONSHIP;
							else {
								type = AttributeType.DATATYPE;
								relType = 4;
							}
							
							Method m2 = clazz.getMethod("get" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1, field.getName().length()));
							if(m2.isAnnotationPresent(OneToOne.class)) {
								relType = 0;
								String mp = m2.getAnnotation(OneToOne.class).mappedBy();
								if(mp == null || mp.equals(""))
									isJoin = true;
							}
							else if(m2.isAnnotationPresent(OneToMany.class)) {
								relType = 1;
								String mp = m2.getAnnotation(OneToMany.class).mappedBy();
								if(mp == null || mp.equals(""))
									isJoin = true;
							}
							else if(m2.isAnnotationPresent(ManyToOne.class)) {
								relType = 2;
								isJoin = true;
							}
							else if(m2.isAnnotationPresent(ManyToMany.class)) {
								relType = 3;
								String mp = m2.getAnnotation(ManyToMany.class).mappedBy();
								if(mp == null || mp.equals(""))
									isJoin = true;
							}
							
							break;
					}
					
					
					//restricciones
					List<Constrain> cons = new LinkedList<Constrain>();
					Method m;
					try {
						if(field.getType().getSimpleName().toLowerCase().equals("boolean")) {
							if(field.getName().startsWith("is"))
								m = clazz.getMethod("is" + Character.toUpperCase(field.getName().charAt(2)) + field.getName().substring(3, field.getName().length()));
							else
								m = clazz.getMethod("is" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1, field.getName().length()));
						} else
							m = clazz.getMethod("get" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1, field.getName().length()));
					} catch(Throwable e) {
						m = clazz.getMethod("get" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1, field.getName().length()));
					}
					
					for(Annotation a : m.getAnnotations()) {
						switch(a.annotationType().getSimpleName().toLowerCase()) {
							case "past":
								cons.add(new Constrain(ConstrainType.PAST));
								break;
								
							case "min":
								cons.add(new ConstrainMin((int) m.getAnnotation(Min.class).value()));
								break;
								
							case "max":
								cons.add(new ConstrainMax((int) m.getAnnotation(Max.class).value()));
								break;
								
							case "range":
								Range r = m.getAnnotation(Range.class);
								cons.add(new ConstrainRange((int) r.min(), (int) r.max()));
								break;
								
							case "pattern":
								cons.add(new ConstrainPattern(m.getAnnotation(javax.validation.constraints.Pattern.class).regexp()));
								break;
								
							case "digits":
								Digits d = m.getAnnotation(Digits.class);
								cons.add(new ConstrainDigit(d.integer(), d.fraction()));
								break;
								
							case "notnull":
								cons.add(new Constrain(ConstrainType.NOT_NULL));
								break;
								
							case "size":
								Size s = m.getAnnotation(Size.class);
								cons.add(new ConstrainSize(s.min(), s.max()));
								break;
								
							case "length":
								Length l = m.getAnnotation(Length.class);
								cons.add(new ConstrainLength(l.min(), l.max()));
								break;
								
							case "notblank":
								cons.add(new Constrain(ConstrainType.NOT_BLANK));
								break;
								
							case "notempty":
								cons.add(new Constrain(ConstrainType.NOT_EMPTY));
								break;
						}
					}
					
					if(type != AttributeType.RELATIONSHIP && type != AttributeType.DATATYPE) {
						Attribute attr = new Attribute(field.getName(), type, id, version, false, optative, cons, isCollection);
						attrs.add(attr);
					} else {
						String name = null;
						if(field.getType().getSimpleName().equals("Collection")) {
							ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
					        Class<?> stringListClass = (Class<?>) stringListType.getActualTypeArguments()[0];
					        name = stringListClass.getSimpleName();
						} else 
							name = field.getType().getSimpleName();
						
						Relationship attr = new Relationship(field.getName(), name, relType, isJoin, false, isCollection);
						attrs.add(attr);
					}
				}
				
				//entity
				Entity entity = new Entity(clazz.getSimpleName(), attrs, null, null, null);
				res.add(entity);
				
				if(clazz.getSuperclass() != Object.class) {
					Class<?> parentClazz = clazz.getSuperclass();
					parents.add(new Pair<Entity, String>(entity, parentClazz == null ? null : parentClazz.getSimpleName()));
				}
			}
			
			//padre
			for(Pair<Entity, String> par : parents) {
				par.getFirst().setParent(Entity.getEntityByName(res, par.getSecond()));
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	private static Set<Repository> getRepositories(Project p, String path) {
		Set<Repository> res = new TreeSet<Repository>();
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\classes");
			for(File f : ResourceManager.scanFiles(path + "target\\classes\\repositories")) {
				Class<? extends Object> clazz = loader.loadClass("repositories." + f.getName().replace(".class", ""));
				res.add(new Repository(p.getEntityByName(clazz.getSimpleName().replace("Repository", "")), null));
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	private static Set<Service> getServices(Project p, String path) {
		Set<Service> res = new TreeSet<Service>();
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\classes");
			for(File f : ResourceManager.scanFiles(path + "target\\classes\\services")) {
				Class<? extends Object> clazz = loader.loadClass("services." + f.getName().replace(".class", ""));
				if(!clazz.getName().contains("$"))
					res.add(new Service(p.getEntityByName(clazz.getSimpleName().replace("Service", ""))));
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	private static Set<Test> getTests(Project p, String path) {
		Set<Test> res = new TreeSet<Test>();
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\test-classes");
			for(File f : ResourceManager.scanFiles(path + "target\\test-classes\\services")) {
				Class<? extends Object> clazz = loader.loadClass("services." + f.getName().replace(".class", ""));
				if(!clazz.getName().contains("$"))
					res.add(new Test(p.getEntityByName(clazz.getSimpleName().replace("ServiceTest", ""))));
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	private static List<String> getAuthorities(Project p, String path) {
		List<String> res = new LinkedList<String>();
		
		try {
			ClassLoader loader = ResourceManager.createLoader(path + "target\\classes");
			Class<?> clazz = loader.loadClass("security.Authority");
			Method m = clazz.getMethod("getAuthority");
			String pattern = m.getAnnotation(javax.validation.constraints.Pattern.class).regexp();
			pattern = pattern.substring(1, pattern.length()-1);
			for(String s : pattern.split("[\\|]"))
				res.add(s);
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	
	//------------------------------------------ Loaded Project ------------------------------------------
	
	private static Map<Integer, Project> projects = new HashMap<Integer, Project>();
	
	public static void loadProject(Project p) {
		projects.put(p.getName().hashCode(), p);
	}
	
	public static void unloadProject(Project p) {
		projects.remove(p.getName().hashCode());
	}
	
	public static Project getLoadedProject(String name) {
		return projects.get(name.hashCode());
	}
	
	public static Collection<Project> getAllProject() {
		return projects.values();
	}
	
	public static void getClearAllProject() {
		projects.clear();
	}
	
}
