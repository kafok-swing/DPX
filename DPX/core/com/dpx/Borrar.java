package com.dpx;

import java.io.File;
import java.io.StringWriter;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.dpx.core.common.Pair;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.utils.UtilsCommonDialogs;

public class Borrar {

	public static void main(String[] args) throws Throwable {
		String path = UtilsCommonDialogs.chooseFile("Elija el proyecto", "C:\\Temp\\VM\\Acme-Pad-Thai\\src/main/resources/spring/config", 
				new Pair<String, String>("Proyecto de eclipse (.project)", "xml"), null);
		
		File xml = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xml);
			
//		doc.normalize();	//tardara, pero hay que hacerlo
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		StringWriter stringWriter = new StringWriter();
		Result output = new StreamResult(stringWriter);
		Source input = new DOMSource(doc);

		transformer.transform(input, output);
		
		Set<String> order = new TreeSet<String>();
		Pattern pattern = Pattern.compile("^\\s*<security:intercept-url\\s*pattern=\"[0-9a-zA-Z\\/\\*.]*\"\\s*access=\"([0-9a-zA-Z(),']|\\s)*\"\\s*\\/>\\s*$");
		for(String s : TextUtils.readLineTextFile(path)) {
			Matcher m = pattern.matcher(s);
			if(m.find()) {
//				System.out.println(s);
				order.add(s);
			}
		}
		
		for(String s : order) {
			System.out.println(s);
		}
		
	}

}
