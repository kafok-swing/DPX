package com.dpx;

import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.resource.ResourceLoader;

public class Dpx {

	public static DpxFrame dpxFrame;
	
	public static void main(String[] args) throws Exception {
		
		UtilsGui.setSystemLookAndFeel();
		
		loadResources();
		
		dpxFrame = new DpxFrame();
	}
	
	private static void loadResources() {
		ResourceLoader.loadIcons();
//		ResourceLoader.loadConfigs("com.dpx.resource");
	}

}
