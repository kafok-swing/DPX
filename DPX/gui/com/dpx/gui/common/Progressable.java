package com.dpx.gui.common;

public interface Progressable {
	
	int getMaximum();
	void setMaximum(int maximum);

	int getMinimum();
	void setMinimum(int minimum);

	int getValue();
	void setValue(int value);

	String getText();
	void setText(String text);

	boolean isIndeterminate();
	void setIndeterminate(boolean indeterminate);
}
