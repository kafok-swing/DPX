package com.dpx.gui.common;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class DpxProgressable implements Progressable {
	
	//-------------------------------------------- Singleton ---------------------------------------------
	
	private static DpxProgressable instance;
	
	public static DpxProgressable get() {
		return instance;
	}
	
	public static DpxProgressable create(JLabel label, JProgressBar progressBar) {
		instance = new DpxProgressable();
		
		instance.setLabel(label);
		instance.setProgressBar(progressBar);
		
		instance.setMinimum(0);
		instance.setMaximum(100);
		instance.setValue(0);
		instance.setIndeterminate(false);
		instance.setText("");
		
		return instance;
	}
	
	private DpxProgressable() {
		super();
	}
	
	
	//-------------------------------------------- Properties --------------------------------------------
	
	private int maximum;
	private int minimum;
	private int value;
	private String text;
	private boolean indeterminate;
	
	private JLabel label;
	private JProgressBar progressBar;
	

	public int getMaximum() {
		return maximum;
	}

	public void setMaximum(int maximum) {
		progressBar.setMaximum(maximum);
		this.maximum = maximum;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMinimum(int minimum) {
		progressBar.setMinimum(minimum);
		this.minimum = minimum;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		progressBar.setValue(value);
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		label.setText(text);
		this.text = text;
	}

	public boolean isIndeterminate() {
		return indeterminate;
	}

	public void setIndeterminate(boolean indeterminate) {
		progressBar.setIndeterminate(indeterminate);
		this.indeterminate = indeterminate;
	}

	public JLabel getLabel() {
		return label;
	}

	public void setLabel(JLabel label) {
		this.label = label;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}
	
}
