package com.dpx.gui.models.main.workspace.column;

import com.dpx.core.view.View;

public class ColumnLink implements ColumnContain {
	
	private String code;
	private String nameES;
	private String nameEN;
	private String url;
	
	private ColumnLink(String code, String nameES, String nameEN, String url) {
		super();
		this.code = code;
		this.nameES = nameES;
		this.nameEN = nameEN;
		this.url = url;
	}

	@Override
	public String getColumn() {
		return View.getColumnLink(code, nameES, nameEN, url);
	}
}
