package com.dpx.gui.models.main.workspace.column;

import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.view.View;

public class ColumnField extends ColumnModel {

	private DomainField field;
	private String entity;
	private boolean sortable;

	public ColumnField(DomainField field, String entity, boolean sortable, String code, String nameES, String nameEN, int wrapper) {
		super(code, nameES, nameEN, wrapper);
		this.field = field;
		this.entity = entity;
		this.sortable = sortable;
	}

	@Override
	public String getColumn() {
		return View.getColumnByField(entity, field, sortable);
	}
}
