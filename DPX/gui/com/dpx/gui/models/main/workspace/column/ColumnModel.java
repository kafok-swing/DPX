package com.dpx.gui.models.main.workspace.column;

public abstract class ColumnModel implements ColumnContain {
	
	private String code;
	private String nameES;
	private String nameEN;
	private int wrapper;
	
	public static final int NO_WRAPPER = 0;
	public static final int WRAPPER_IF = 1;
	public static final int WRAPPER_AUTH = 2;
	
	protected ColumnModel(String code, String nameES, String nameEN, int wrapper) {
		super();
		this.code = code;
		this.nameES = nameES;
		this.nameEN = nameEN;
		this.wrapper = wrapper;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameES() {
		return nameES;
	}

	public void setNameES(String nameES) {
		this.nameES = nameES;
	}

	public String getNameEN() {
		return nameEN;
	}

	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

	public int getWrapper() {
		return wrapper;
	}

	public void setWrapper(int wrapper) {
		this.wrapper = wrapper;
	}
}
