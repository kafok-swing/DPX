package com.dpx.gui.models.main.workspace;

import javax.swing.tree.DefaultMutableTreeNode;

import com.dpx.core.Project;
import com.dpx.core.ProjectFactory;
import com.dpx.core.domain.Entity;
import com.dpx.core.repositories.Repository;
import com.dpx.core.services.Service;
import com.dpx.gui.view.main.workspace.Navigator;

public class NavigatorModel {
	
	//---------------------------------------------- Finals ----------------------------------------------
	
	public static final String DOMINIO = "Dominio";
	public static final String AGREGAR_ENTIDAD = "Agregar entidad";
	public static final String AGREGAR_ACTOR = "Agragar actor";
	public static final String AUTORIDADES = "Autoridades";
	public static final String AGREGAR_PRUNE = "Agregar entidad pruneada";
	public static final String ENTIDADES = "Entidades";
	public static final String ACTORES = "Actores";
	public static final String POPULATE = "Populate";
	public static final String REPOSITORIOS = "Respositorios";
	public static final String AGREGAR_REPOSITORIOS = "Agregar respositorios";
	public static final String L_REPOSITORIOS = "Respositorios";
	public static final String SERVICIOS = "Servicios";
	public static final String AGREGAR_SERVICIOS = "Agregar servicios";
	public static final String AGREGAR_SERVICIO = "Agregar servicio";
	public static final String COMMON_METHOD = "Métodos comunes";
	public static final String TEST_SERVICES = "Agregar tests";
	public static final String L_SERVICIOS = "Servicios";
	public static final String CONTROLADORES = "Controladores";
	public static final String AGREAGAR_CONTROLADORES = "Agregar controladores";
	public static final String AGREAGAR_CONTROLADOR = "Agregar controlador";
	public static final String VISTAS = "Vistas";
	public static final String JSTL_OUT = "Todos con <jstl:out />";
	public static final String GESTIONAR_VISTA = "Gestionar vista";
	public static final String AGREGAR_VISTA_EDIT = "Agregar vista de edición";
	public static final String AGREGAR_VISTA_LIST = "Agregar vista de listado";
	public static final String CONFIGURACION = "Configuración";
	public static final String CREAR_CONVERTERS = "Crear converters";
	
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private Navigator navigator;

	
	//------------------------------------------- Constructors -------------------------------------------
	
	public NavigatorModel(Navigator navigator) {
		super();
		this.navigator = navigator;
	}
	
	
	//-------------------------------------------- Operations --------------------------------------------
	
	public void addProject(Project project) {
		//proyecto
		DefaultMutableTreeNode top = new DefaultMutableTreeNode(project.getName());
		
		//funcionalidades
		DefaultMutableTreeNode dominio = new DefaultMutableTreeNode(DOMINIO);
		DefaultMutableTreeNode populate = new DefaultMutableTreeNode(POPULATE);
		DefaultMutableTreeNode repositorios = new DefaultMutableTreeNode(REPOSITORIOS);
		DefaultMutableTreeNode servicios = new DefaultMutableTreeNode(SERVICIOS);
		DefaultMutableTreeNode controladores = new DefaultMutableTreeNode(CONTROLADORES);
		DefaultMutableTreeNode vistas = new DefaultMutableTreeNode(VISTAS);
		DefaultMutableTreeNode configuration = new DefaultMutableTreeNode(CONFIGURACION);
		
		top.add(dominio);
		top.add(populate);
		top.add(repositorios);
		top.add(servicios);
		top.add(controladores);
		top.add(vistas);
		top.add(configuration);
		
			//dominio
			DefaultMutableTreeNode newEntity = new DefaultMutableTreeNode(AGREGAR_ENTIDAD);
			DefaultMutableTreeNode newActor = new DefaultMutableTreeNode(AGREGAR_ACTOR);
			DefaultMutableTreeNode gestionaAutoridades = new DefaultMutableTreeNode(AUTORIDADES);
			
				//autoridades
				for(String a : project.getAuthorities()) {
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(a);
					gestionaAutoridades.add(node);
				}
			
			DefaultMutableTreeNode prune = new DefaultMutableTreeNode(AGREGAR_PRUNE);
			DefaultMutableTreeNode entidades = new DefaultMutableTreeNode(ENTIDADES);
			DefaultMutableTreeNode actores = new DefaultMutableTreeNode(ACTORES);
			
			dominio.add(newEntity);
			dominio.add(newActor);
			dominio.add(gestionaAutoridades);
			dominio.add(prune);
			dominio.add(entidades);
			dominio.add(actores);
			
				//entidades
				for(Entity e : project.getEntities()) {
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(e.getName());
					entidades.add(node);
				}
				
				
			//repositorio
			DefaultMutableTreeNode newRepo = new DefaultMutableTreeNode(AGREGAR_REPOSITORIOS);
			DefaultMutableTreeNode repos = new DefaultMutableTreeNode(L_REPOSITORIOS);
			
			repositorios.add(newRepo);
			repositorios.add(repos);
			
				for(Repository e : project.getRepos()) {
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(e.getEntity().getName() + "Repository");
					repos.add(node);
				}
				
				
			//servicios
			DefaultMutableTreeNode allServ = new DefaultMutableTreeNode(AGREGAR_SERVICIOS);
			DefaultMutableTreeNode newServ = new DefaultMutableTreeNode(AGREGAR_SERVICIO);
			DefaultMutableTreeNode commonMehtod = new DefaultMutableTreeNode(COMMON_METHOD);
			DefaultMutableTreeNode testServices = new DefaultMutableTreeNode(TEST_SERVICES);
			DefaultMutableTreeNode servs = new DefaultMutableTreeNode(L_SERVICIOS);
			
				for(Service s : project.getService()) {
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(s.getEntity().getName() + "Service");
					servs.add(node);
				}
			
			servicios.add(allServ);
			servicios.add(newServ);
			servicios.add(commonMehtod);
			servicios.add(testServices);
			servicios.add(servs);
			
			//controladores
			DefaultMutableTreeNode addCtrls = new DefaultMutableTreeNode(AGREAGAR_CONTROLADORES);
			DefaultMutableTreeNode addCtrl = new DefaultMutableTreeNode(AGREAGAR_CONTROLADOR);
			
			controladores.add(addCtrls);
			controladores.add(addCtrl);
			
			//vistas
			DefaultMutableTreeNode jstltOut = new DefaultMutableTreeNode(JSTL_OUT);
			DefaultMutableTreeNode gestionarVista = new DefaultMutableTreeNode(GESTIONAR_VISTA);
			DefaultMutableTreeNode addEdit = new DefaultMutableTreeNode(AGREGAR_VISTA_EDIT);
			DefaultMutableTreeNode addList = new DefaultMutableTreeNode(AGREGAR_VISTA_LIST);
			
			vistas.add(jstltOut);
			vistas.add(gestionarVista);
			vistas.add(addEdit);
			vistas.add(addList);
			
			//configuracion
			DefaultMutableTreeNode converters = new DefaultMutableTreeNode(CREAR_CONVERTERS);
			configuration.add(converters);
			
		
		navigator.addTreeProjects(project.getProjectPath(), top, project.getName());
		ProjectFactory.loadProject(project);
	}
	
	public void reloadProject(Project p) {
		Project project = ProjectFactory.getProject(p.getProjectPath() + ".project");
		navigator.removeTreeProjects(p.getProjectPath(), p.getName());
		ProjectFactory.unloadProject(p);
		addProject(project);
	}
}
