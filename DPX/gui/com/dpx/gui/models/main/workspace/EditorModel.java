package com.dpx.gui.models.main.workspace;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.dpx.core.Project;
import com.dpx.core.common.Consumer;
import com.dpx.core.common.Pair;
import com.dpx.core.configurations.Converters;
import com.dpx.core.domain.Entity;
import com.dpx.core.repositories.Repository;
import com.dpx.core.services.Service;
import com.dpx.core.services.Test;
import com.dpx.gui.components.LabelButton;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.gui.view.main.workspace.Editor;
import com.dpx.gui.view.main.workspace.tabs.ControllerTab;
import com.dpx.gui.view.main.workspace.tabs.EntityTab;
import com.dpx.gui.view.main.workspace.tabs.ReconstructTab;
import com.dpx.gui.view.main.workspace.tabs.listing.ListingTab;
import com.dpx.gui.view.main.workspace.tabs.prune.PruneTab;
import com.dpx.gui.view.main.workspace.tabs.views.AddViewTab;
import com.dpx.gui.view.main.workspace.tabs.views.EditViewTab;
import com.dpx.gui.view.main.workspace.tabs.views.ListViewTab;
import com.dpx.resource.Resources;

public class EditorModel {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private Editor editor;

	
	//------------------------------------------- Constructors -------------------------------------------
	
	public EditorModel(Editor editor) {
		super();
		this.editor = editor;
	}
	
	
	//-------------------------------------------- Operations --------------------------------------------
	
	public void openEntity(Project project, Entity entity) {
		openTab(entity.getName(), new EntityTab(project, entity));
	}

	public void openReaminRepositories(final Project project, Collection<Pair<String, Boolean>> repos) {
		openTab("Repositorios restantes", new ListingTab(project, "Repositorios sin hacer", "Crear repositorios", repos, new Consumer<Collection<String>>() {
			public void run(Collection<String> param) {
				for(String s : param)
					Repository.saveFile(project, s.replace("Repository", ""));
			}
		}));
	}
	
	public void openReaminServices(final Project project, Collection<Pair<String, Boolean>> serv) {
		openTab("Servicios restantes", new ListingTab(project, "Servicios sin hacer", "Crear servicios", serv, new Consumer<Collection<String>>() {
			public void run(Collection<String> param) {
				for(String s : param)
					Service.saveFile(project, s.replace("Service", ""));
			}
		}));
	}
	
	public void openSelecterConverters(final Project project, Collection<Pair<String, Boolean>> conv) {
		openTab("Crear convertidores", new ListingTab(project, "Convertidores disponibles", "Crear convertidores", conv, new Consumer<Collection<String>>() {
			public void run(Collection<String> param) {
				Converters.saveAll(project, param);
			}
		}));
	}
	
	public void openRemainTest(final Project project, Collection<Pair<String, Boolean>> test) {
		openTab("Crear test", new ListingTab(project, "Test disponibles", "Crear tests", test, new Consumer<Collection<String>>() {
			public void run(Collection<String> param) {
				for(String s : param)
					Test.saveFile(project, s.replace("ServiceTest", ""));
				
				DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(project);
			}
		}));
	}
	
	public void openCreatePrune(Project project) {
		openTab("Crear clases pruned", new PruneTab(project));
	}
	
	public void openCommonMethod(Project project) {
		openTab("Crear clases pruned", new ReconstructTab(project));
	}
	
	public void openCreateController(Project project) {
		openTab("Crear controlador", new ControllerTab(project));
	}
	
	public void openManageView(Project project) {
		openTab("Gestiona vista", new AddViewTab(project));
	}
	
	public void openCreateViewEdit(Project project) {
		openTab("Crear edit.jsp", new EditViewTab(project));
	}
	
	public void openCreateViewList(Project project) {
		openTab("Crear list.jsp", new ListViewTab(project));
	}
	
	
	private void openTab(String title, JPanel tab) {
		//pesta�a
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBackground(Color.WHITE);
		
		JLabel lTitle = new JLabel(title);
		panel.add(lTitle);
		
		Component padding = Box.createHorizontalStrut(10);
		panel.add(padding);
		
		LabelButton close = new LabelButton(7, Resources.get().getIcon("simple_cross"));
		close.setPercentMoveClicked(0.2);
		panel.add(close);
		
		//tab
		JScrollPane scrollPane = new JScrollPane(tab);
		scrollPane.getVerticalScrollBar().setUnitIncrement(20);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(20);
		editor.addTab(title, scrollPane);	//TODO with icon
		editor.setTabComponentAt(editor.getTabCount()-1, panel);
		editor.setSelectedIndex(editor.getTabCount()-1);
		
		//cerrar pesta�a
		CloseTabController act = new CloseTabController(null, editor.getTabCount()-1);
		close.addActionListener(act);
		if(last != null)
			last.next = act;
		last = act;
	}
	
	
	//------------------------------------------ Cerrar pesta�a ------------------------------------------
	
	private static CloseTabController last = null;
	
	private class CloseTabController implements ActionListener {
		
		private CloseTabController next;
		private int id;
		
		public CloseTabController(CloseTabController next, int id) {
			super();
			this.next = next;
			this.id = id;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if(next != null)
				next.lastClose();
			
			EditorModel.this.editor.removeTabAt(id);
		}
		
		public void lastClose() {
			id--;
			if(next != null)
				next.lastClose();
		}
	}
}
