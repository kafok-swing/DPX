package com.dpx.gui.controller.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import com.dpx.core.Project;
import com.dpx.core.ProjectFactory;
import com.dpx.core.common.Pair;
import com.dpx.gui.controller.AbstractController;
import com.dpx.gui.utils.UtilsCommonDialogs;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.gui.view.main.DpxMenuBar;

public class DpxMenuBarController extends AbstractController implements ActionListener {
	
	//-------------------------------------------- Atributos ---------------------------------------------
	
	private DpxMenuBar component;

	
	//------------------------------------------- Constructor --------------------------------------------
	
	public DpxMenuBarController(DpxMenuBar component) {
		super(component);
		this.component = component;
	}

	
	//---------------------------------------------- Evento ----------------------------------------------
	
	public void actionPerformed(ActionEvent act) {
		switch(act.getActionCommand()) {
			case "archivo.abrir":
				doArchivoAbrir();
				break;
				
			case "archivo.recargar":
				doArchivoRecargar();
				break;
		}
	}


	//--------------------------------------------- Acciones ---------------------------------------------
	
	private void doArchivoAbrir() {
		String path = UtilsCommonDialogs.chooseFile("Elija el proyecto", ".", 
				new Pair<String, String>("Proyecto de eclipse (.project)", "project"), component.getParent());
//		String path = "C:\\Temp\\VM\\Acme-Pad-Thai\\.project";
		
		Project p = ProjectFactory.getProject(path);
		
		DpxFrame dpxFrame = (DpxFrame)component.getParent().getParent().getParent();
		dpxFrame.getWorkspace().getNavigator().getNavigatorModel().addProject(p);
	}
	
	private void doArchivoRecargar() {
		Collection<Project> res = new LinkedList<Project>(ProjectFactory.getAllProject());
		ProjectFactory.getClearAllProject();
		for(Project p : res)
			DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(p);
	}

}
