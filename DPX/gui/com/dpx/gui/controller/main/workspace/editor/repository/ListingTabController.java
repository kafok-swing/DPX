package com.dpx.gui.controller.main.workspace.editor.repository;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.dpx.core.common.Consumer;
import com.dpx.gui.view.main.workspace.tabs.listing.ListingEntry;
import com.dpx.gui.view.main.workspace.tabs.listing.ListingTab;

public class ListingTabController implements ActionListener {
	
	private ListingTab view;
	private Consumer<Collection<String>> action;

	public ListingTabController(ListingTab view, Consumer<Collection<String>> action) {
		super();
		this.view = view;
		this.action = action;
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "createAll":
				List<String> param = new LinkedList<String>();
				for(ListingEntry e : view.getEntries())
					if(e.isSelected())
						param.add(e.getName());
						
				action.run(param);
				break;
				
			case "selectAll":
				for(ListingEntry e : view.getEntries())
					e.setSelected(true);
				break;
				
			case "ncreate":
				for(ListingEntry e : view.getEntries())
					if(!e.isCreate())
						e.setSelected(true);
				break;
				
			case "quitAll":
				for(ListingEntry e : view.getEntries())
					e.setSelected(false);
				break;
		}
	}
	
}
