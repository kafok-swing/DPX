package com.dpx.gui.controller.main.workspace.editor.service;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JCheckBox;

import com.dpx.core.domain.clazz.DomainClass;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.services.Service;
import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.workspace.tabs.ReconstructTab;

public class ReconstructTabController implements ActionListener {

	private ReconstructTab tab;

	public ReconstructTabController(ReconstructTab tab) {
		super();
		this.tab = tab;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "entidad":
				String item = tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex());
				DomainClass clazz = tab.getProject().getClasses().get(item);
				tab.getFields().setFields(clazz.getAllField());
				
				tab.revalidate();
				tab.repaint();
				break;
				
			case "reconstruct":
				String item2 = tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex());
				Map<String, DomainField> fields = new LinkedHashMap<String, DomainField>();
				DomainClass clazz1 = tab.getProject().getClasses().get(item2);
				for(DomainField f : clazz1.getAllField())
					fields.put(f.getName(), f);
				
				for(JCheckBox cb : tab.getFields().getFields())
					if(!cb.isSelected())
						fields.remove(cb.getText());
				
				UtilsGui.copyToClippboard(Service.getReconstruct(tab.getProject(), clazz1.getName(), clazz1.getName(), fields.values()));
				break;
				
			case "create":
				String item3 = tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex());
				DomainClass clazz2 = tab.getProject().getClasses().get(item3);
				
				UtilsGui.copyToClippboard(Service.getCreate(tab.getProject(), clazz2.getName()));
				break;
				
			case "sall":
				for(JCheckBox cb : tab.getFields().getFields())
					cb.setSelected(true);
				break;
				
			case "qall":
				for(JCheckBox cb : tab.getFields().getFields())
					cb.setSelected(false);
				break;
		}
	}
}
