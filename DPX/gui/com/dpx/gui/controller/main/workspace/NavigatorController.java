package com.dpx.gui.controller.main.workspace;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;

import com.dpx.core.Project;
import com.dpx.core.ProjectFactory;
import com.dpx.core.common.Pair;
import com.dpx.core.view.ViewUtils;
import com.dpx.gui.models.main.workspace.NavigatorModel;
import com.dpx.gui.view.main.workspace.Navigator;
import com.dpx.gui.view.main.workspace.Workspace;
import com.dpx.resource.ResourceManager;

public class NavigatorController implements MouseListener {

	//-------------------------------------------- Attributes --------------------------------------------
	
	private Navigator navigator;

	
	//------------------------------------------- Constructor --------------------------------------------
	
	public NavigatorController(Navigator navigator) {
		super();
		this.navigator = navigator;
	}

	
	//--------------------------------------------- Eventos ----------------------------------------------
	
	@Override
	public void mouseClicked(MouseEvent e) {
        if(e.getClickCount() == 2) {
        	TreePath selPath = navigator.getTree().getPathForLocation(e.getX(), e.getY());
    		Project pNow = ProjectFactory.getLoadedProject(selPath.getPathComponent(1).toString());
    		
    		switch(selPath.getPathComponent(2).toString()) {
    			case NavigatorModel.DOMINIO:
    				switch(selPath.getPathComponent(3).toString()) {
    					case NavigatorModel.AGREGAR_PRUNE:
    						((Workspace)navigator.getParent().getParent()).getEditor()
    							.getEditorModel().openCreatePrune(pNow);
    					break;
    					
	    				case NavigatorModel.ENTIDADES:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
	    						.getEditorModel().openEntity(pNow, pNow.getEntityByName(selPath.getPathComponent(4).toString()));
	    					break;
    				}
    				break;
    			
    			case NavigatorModel.REPOSITORIOS:
    				switch(selPath.getPathComponent(3).toString()) {
	    				case NavigatorModel.L_REPOSITORIOS:
	    					break;
	    				
	    				case NavigatorModel.AGREGAR_REPOSITORIOS:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
	    						.getEditorModel().openReaminRepositories(pNow, pNow.getRepositoriesRemain());
	    					break;
					}
					break;
					
    			case NavigatorModel.SERVICIOS:
    				switch(selPath.getPathComponent(3).toString()) {
	    				
	    				case NavigatorModel.AGREGAR_SERVICIOS:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
	    						.getEditorModel().openReaminServices(pNow, pNow.getServicesRemain());
	    					break;
	    					
	    				case NavigatorModel.COMMON_METHOD:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
    							.getEditorModel().openCommonMethod(pNow);
	    					break;
	    					
	    				case NavigatorModel.TEST_SERVICES:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
    							.getEditorModel().openRemainTest(pNow, pNow.getTestsRemain());
	    					break;
					}
					break;
					
    			case NavigatorModel.CONTROLADORES:
    				switch(selPath.getPathComponent(3).toString()) {
	    				
	    				case NavigatorModel.AGREAGAR_CONTROLADOR:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
	    						.getEditorModel().openCreateController(pNow);
	    					break;
	    				}
					break;
					
    			case NavigatorModel.VISTAS:
    				switch(selPath.getPathComponent(3).toString()) {
	    				case NavigatorModel.JSTL_OUT:
	    					int res = JOptionPane.showOptionDialog(navigator.getParent().getParent().getParent().getParent().getParent().getParent(),
	    							"<html>�Quiere que cambiemos todos los <b>&#36;{...}</b> por <b>&lt;jstl:out value=&quot;...&quot; &#47;&gt;</b> en su proyecto?</html>", 
	    							"Desea realizar la conversi�n", 
	    							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[]{"Si", "No"}, "Si");
	    					
	    					if(res == JOptionPane.OK_OPTION) {	//TODO config (detectar borrando metodo getProjectPath)
	    						ViewUtils.transformJstlOut(ResourceManager.scanFiles(pNow.getProjectPath() + "src\\main\\webapp\\views", "jsp"));//TODO seleccionar en la vista
	    					}
	    					
	    					break;
	    					
	    				case NavigatorModel.GESTIONAR_VISTA:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
								.getEditorModel().openManageView(pNow);
	    					break;
	    					
	    				case NavigatorModel.AGREGAR_VISTA_EDIT:
	    					((Workspace)navigator.getParent().getParent()).getEditor()
								.getEditorModel().openCreateViewEdit(pNow);
	    					break;
	    					
						case NavigatorModel.AGREGAR_VISTA_LIST:
							((Workspace)navigator.getParent().getParent()).getEditor()
								.getEditorModel().openCreateViewList(pNow);
							break;
					}
					break;
					
    			case NavigatorModel.CONFIGURACION:
    				switch(selPath.getPathComponent(3).toString()) {
	    				case NavigatorModel.CREAR_CONVERTERS:
	    					Collection<Pair<String, Boolean>> conv = new LinkedList<Pair<String, Boolean>>();
	    					for(String s : pNow.getEntitiesNames())
	    						conv.add(new Pair<String, Boolean>(s, false));
	    					
	    					((Workspace)navigator.getParent().getParent()).getEditor()
    							.getEditorModel().openSelecterConverters(pNow, conv);
	    					break;
					}
					break;
    		}
        }
	}

	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
	
	
}
