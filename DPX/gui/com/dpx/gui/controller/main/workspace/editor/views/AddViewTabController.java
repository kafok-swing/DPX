package com.dpx.gui.controller.main.workspace.editor.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import com.dpx.core.common.Tuple3;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.View;
import com.dpx.gui.view.main.workspace.tabs.views.AddViewTab;
import com.dpx.gui.view.main.workspace.tabs.views.ViewEntry;

public class AddViewTabController implements ActionListener {

	private AddViewTab tab;

	public AddViewTabController(AddViewTab tab) {
		super();
		this.tab = tab;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "entity":
				tab.changeEntity(tab.getEntities().getItemAt(tab.getEntities().getSelectedIndex()));
				break;
				
			case "add":
				tab.addEntry();
				break;
				
			case "delete":
				tab.removeEntry((ViewEntry)((Component)arg0.getSource()).getParent().getParent().getParent().getParent());
				break;
				
			case "deleteFile":
				String name2 = (String) tab.getEntities().getSelectedItem();
				name2 = TextUtils.tranformString(name2, false, true, false, "");
				
				List<String> res = new LinkedList<String>();
				for(Tuple3<String, String, String> t : tab.getParamsES())
					res.add(t.getSecond());
				
				View.deleteFile(tab.getProject(), name2, res);
				
				break;
				
			case "save":
				String name = (String) tab.getEntities().getSelectedItem();
				name = TextUtils.tranformString(name, false, true, false, "");
				
				View.saveViewsFile(tab.getProject(), name, tab.getParamsES(), tab.getParamsEN());
				break;
		}
	}
}
