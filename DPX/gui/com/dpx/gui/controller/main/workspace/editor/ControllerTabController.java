package com.dpx.gui.controller.main.workspace.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBox;

import com.dpx.core.controllers.Controller;
import com.dpx.core.utils.TextUtils;
import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.workspace.tabs.ControllerTab;

public class ControllerTabController implements ActionListener {

	private ControllerTab tab;

	public ControllerTabController(ControllerTab tab) {
		super();
		this.tab = tab;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String cmd[] = arg0.getActionCommand().split("[.]");
		switch(cmd[0]) {
			case "entidades":
				changePath();
				calculeSecurity();
				break;

			case "auth":
				switch(cmd[1]) {
					case "all":
						onlyOne("Todos");
						break;
						
					case "nl":
						onlyOne("No logueados");
						break;
						
					case "l":
						onlyOne("Logueado");
						break;
						
					default:
						onlySpecifics();
						break;
				}
				changePath();
				calculeSecurity();
				break;
				
			case "create":
				Controller.saveFile(tab.getProject(), tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex()), 
						tab.getTextField().getText(), tab.getChckbxconMtodoReconstruct().isSelected(), tab.getRdbtnClaseVacia().isSelected());
				break;
				
			case "cclase":
				Controller.copyFile(tab.getProject(), tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex()), 
						tab.getTextField().getText(), tab.getChckbxconMtodoReconstruct().isSelected(), tab.getRdbtnClaseVacia().isSelected());
				break;
				
			case "csecurity":
				UtilsGui.copyToClippboard(tab.getTextFieldSecurity().getText());
				break;
		}
	}
	
	private void calculeSecurity() {
		List<String> auth = getAuth();
		String access = "";
		
		if(auth.size() == 0)
			access = "permitAll";
		else if(auth.size() == 1) {
			switch(auth.get(0)) {
				case "Todos":
					access = "permitAll";
					break;
					
				case "No logueados":
					access = "isAnonymous()";
					break;
					
				case "Logueado":
					access = "isAuthenticated()";
					break;
					
				default:
					access = "hasRole('" + auth.get(0) + "')";
					break;
			}
		} else {
			String res = "";
			for(String s : auth)
				res += ("'" + s + "', ");
			res = res.substring(0, res.length() - 2);
			
			access = "hasAnyRole(" + res + ")";
		}
		
		tab.getTextFieldSecurity().setText("<security:intercept-url pattern=\"" + tab.getTextField().getText() + "/**\" access=\"" + access + "\" />");
	}
	
	private void changePath() {
		String n = TextUtils.tranformString(tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex()), false, true, false, "");
		tab.getTextField().setText("/" + n + getPathAuth());
	}
	
	private String getPathAuth() {
		List<String> auth = getAuth();
		if(auth.size() == 0)
			return "";
		else if(auth.size() == 1) {
			switch(auth.get(0)) {
				case "Todos":
					return "";
				case "No logueados":
					return "";
				case "Logueado":
					return "";
				default:
					return "/" + auth.get(0).toLowerCase();
			}
		} else
			return "";
	}
	
	private List<String> getAuth() {
		List<String> res = new LinkedList<String>();
		for(JCheckBox cb : tab.getAuthorities())
			if(cb.isSelected())
				res.add(cb.getText());
		
		return res;
	}
	
	private void onlyOne(String name) {
		for(JCheckBox cb : tab.getAuthorities())
			if(!cb.getText().equals(name))
				cb.setSelected(false);
	}
	
	private void onlySpecifics() {
		for(JCheckBox cb : tab.getAuthorities())
			if(cb.getText().equals("Todos") || cb.getText().equals("No logueados") || cb.getText().equals("Logueado"))
				cb.setSelected(false);
	}
}
