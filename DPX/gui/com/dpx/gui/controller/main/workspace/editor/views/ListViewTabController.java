package com.dpx.gui.controller.main.workspace.editor.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.dpx.core.Project;
import com.dpx.core.common.Pair;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.View;
import com.dpx.gui.models.main.workspace.column.ColumnField;
import com.dpx.gui.models.main.workspace.column.ColumnModel;
import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.gui.view.main.workspace.tabs.views.ListEntry;
import com.dpx.gui.view.main.workspace.tabs.views.ListViewTab;

public class ListViewTabController  implements ActionListener {
	
	private ListViewTab tab;

	public ListViewTabController(ListViewTab tab) {
		super();
		this.tab = tab;
	}

	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "entity":
				tab.changeModel((String) tab.getComboBox().getSelectedItem());
				break;
				
			case "crear":
				create(tab.getProject(), false);
				break;
				
			case "msg":
				create(tab.getProject(), true);
				break;
				
			case "copy":
				copy(tab.getProject());
				break;
				
			case "all":
				for(ListEntry entry : tab.getEntries())
					entry.setPut(true);
				break;
				
			case "quit":
				for(ListEntry entry : tab.getEntries())
					entry.setPut(false);
				break;
				
			case "put":
				ListEntry entry = (ListEntry) ((Component) arg0.getSource()).getParent().getParent().getParent();
				entry.setPut(entry.isPut());
				break;
				
			case "up":
				ListEntry up = (ListEntry) ((Component) arg0.getSource()).getParent().getParent();
				int indexUp = tab.getEntries().indexOf(up);
				if(indexUp > 0) {
					tab.getEntryBox().removeAll();
					ListEntry _up = tab.getEntries().get(indexUp - 1);
					tab.getEntries().set(indexUp - 1, up);
					tab.getEntries().set(indexUp, _up);
					
					for(JPanel upPanel : tab.getEntries()) {
						tab.getEntryBox().add(upPanel);
						tab.getEntryBox().add(Box.createVerticalStrut(10));
					}
					
					tab.getEntryBox().revalidate();
					tab.getEntryBox().repaint();
				}
				break;
				
			case "down":
				ListEntry down = (ListEntry) ((Component) arg0.getSource()).getParent().getParent();
				int indexDown = tab.getEntries().indexOf(down);
				if(indexDown <= tab.getEntries().size()) {
					tab.getEntryBox().removeAll();
					ListEntry _down = tab.getEntries().get(indexDown + 1);
					tab.getEntries().set(indexDown + 1, down);
					tab.getEntries().set(indexDown, _down);
					
					for(JPanel upPanel : tab.getEntries()) {
						tab.getEntryBox().add(upPanel);
						tab.getEntryBox().add(Box.createVerticalStrut(10));
					}
					
					tab.getEntryBox().revalidate();
					tab.getEntryBox().repaint();
				}
				break;
				
			case "add":
				break;
				
			case "quitDesc":
				tab.getEntryBox().removeAll();
				List<ListEntry> list = new LinkedList<ListEntry>(tab.getEntries());
				tab.getEntries().clear();
				for(ListEntry entry1 : list) {
					if(entry1.isPut()) {
						tab.getEntryBox().add(entry1);
						tab.getEntryBox().add(Box.createVerticalStrut(10));
						
						tab.getEntries().add(entry1);
					}
				}
				tab.getEntryBox().revalidate();
				tab.getEntryBox().repaint();
				break;
		}
	}
	
	private void create(Project p, boolean msg) {
		//atributos que pasarle
		String name = TextUtils.tranformString((String) tab.getComboBox().getSelectedItem(), false, true, false, "");
		Map<String, Pair<String, String>> messages = new LinkedHashMap<String, Pair<String, String>>();
		for(ListEntry entry : tab.getEntries()) {
			if(entry.isPut()) {
				messages.put(name + "." + entry.getName(), new Pair<String, String>(entry.getTitleES(), entry.getTitleEN()));
			}
		}
		
		//comprobamos si existe
		String path = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/list.jsp";
		if(!msg) {
			int num = JOptionPane.OK_OPTION;
			File file = new File(path).getAbsoluteFile();
			if(file.exists()) {
				num = JOptionPane.showConfirmDialog(DpxFrame.get(), "�Cuidado! El archivo \"" + file.getAbsolutePath() + "\" ya existe. �Desea sobreescribirlo?", 
						"El archivo ya existe", JOptionPane.WARNING_MESSAGE);
			
				if(num != JOptionPane.OK_OPTION)
					return;
			}
		} else {
			int num = JOptionPane.OK_OPTION;
			num = JOptionPane.showConfirmDialog(DpxFrame.get(), "�Desea realizar esta acci�n?", "�Est� seguro?", JOptionPane.WARNING_MESSAGE);
		
			if(num != JOptionPane.OK_OPTION)
				return;
		}
		
		String pathEN = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/messages.properties";
		String pathES = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/messages_es.properties";
		toMessage(messages, pathES, true);
		toMessage(messages, pathEN, false);
		
		//guardar vista
		if(!msg) {
			List<ColumnModel> columns = new LinkedList<ColumnModel>();
			String entity = "";
			for(int i=0; i < tab.getEntryBox().getComponentCount(); i++) {
				Component comp = tab.getEntryBox().getComponent(i);
				switch(comp.getClass().getSimpleName()) {
					case "ListEntry":
						ListEntry listEntry = (ListEntry) comp;
						entity = listEntry.getEntity();
						if(listEntry.isPut())
							columns.add(new ColumnField(listEntry.getField(), listEntry.getEntity(), listEntry.isSortable(), "", "", "", ColumnModel.NO_WRAPPER));
						break;
						
					case "----":
						break;
				}
			}
			View.saveFile(path, View.getVoidView(false) + "\n\n" + View.getList(entity, columns));
		}
	}
	
	private void toMessage(Map<String, Pair<String, String>> messages, String pathES, boolean es) {
		Collection<String> res = new LinkedList<String>();
		for(String str : TextUtils.readLineTextFile(pathES)) {
			String split[] = str.split("[=]");
			if(split.length >= 2) {
				Pair<String, String> obj = messages.get(split[0].trim());
				if(obj == null)
					res.add(str);
			} else
				res.add(str);
		}
		
		for(String str : messages.keySet())
			res.add(str + " = " + (es ? messages.get(str).getFirst() : messages.get(str).getSecond()));
		
		String recons = "";
		for(String str : res)
			recons += (str + "\n");
		
		recons.substring(0, recons.length()-1);
		View.saveFile(pathES, recons);
	}
	
	private void copy(Project p) {
		List<ColumnModel> columns = new LinkedList<ColumnModel>();
		String entity = "";
		for(int i=0; i < tab.getEntryBox().getComponentCount(); i++) {
			Component comp = tab.getEntryBox().getComponent(i);
			switch(comp.getClass().getSimpleName()) {
				case "ListEntry":
					ListEntry listEntry = (ListEntry) comp;
					entity = listEntry.getEntity();
					if(listEntry.isPut())
						columns.add(new ColumnField(listEntry.getField(), listEntry.getEntity(), listEntry.isSortable(), "", "", "", ColumnModel.NO_WRAPPER));
					break;
			}
		}
		UtilsGui.copyToClippboard(View.getList(entity, columns));
	}
}
