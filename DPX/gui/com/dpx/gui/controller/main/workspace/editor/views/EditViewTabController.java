package com.dpx.gui.controller.main.workspace.editor.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.JOptionPane;

import com.dpx.core.Project;
import com.dpx.core.common.Pair;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.View;
import com.dpx.gui.utils.UtilsGui;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.gui.view.main.workspace.tabs.views.EditEntry;
import com.dpx.gui.view.main.workspace.tabs.views.EditViewTab;

public class EditViewTabController implements ActionListener {
	
	private EditViewTab tab;

	public EditViewTabController(EditViewTab tab) {
		super();
		this.tab = tab;
	}

	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "entity":
				tab.changeModel(tab.getProject(), (String) tab.getComboBox().getSelectedItem());
				break;
				
			case "crear":
				create(tab.getProject(), false);
				break;
			
			case "msg":
				create(tab.getProject(), true);
				break;
			
			case "copy":
				copy(tab.getProject());
				break;
				
			case "all":
				for(EditEntry entry : tab.getEntries())
					entry.setPut(true);
				break;
				
			case "seguro":
				Map<String, Boolean> map = new HashMap<String, Boolean>();
				for(DomainField prop : tab.getProject().getAnyClass((String) tab.getComboBox().getSelectedItem()).getAllField())
					map.put(prop.getName(), !(prop.isCollection() || prop.getType().isOtherClass() || prop.getName().equals("version")));
				
				for(EditEntry entry : tab.getEntries())
					entry.setPut(map.get(entry.getName()));
				break;
				
			case "quit":
				for(EditEntry entry : tab.getEntries())
					entry.setPut(false);
				break;
				
			case "put":
				EditEntry entry = (EditEntry) ((Component) arg0.getSource()).getParent().getParent().getParent();
				entry.setPut(entry.isPut());
				break;
				
			case "hidden":
				EditEntry entry2 = (EditEntry) ((Component) arg0.getSource()).getParent().getParent().getParent();
				entry2.setHidden(entry2.isHidden());
				break;
		}
	}



	private void create(Project p, boolean msg) {
		//atributos que pasarle
		String name = TextUtils.tranformString((String) tab.getComboBox().getSelectedItem(), false, true, false, "");
		Collection<Pair<DomainField, String>> fields = new LinkedList<Pair<DomainField, String>>();
		Collection<String> hidden = new LinkedList<String>();
		Map<String, Pair<String, String>> messages = new LinkedHashMap<String, Pair<String, String>>();
		for(EditEntry entry : tab.getEntries()) {
			if(entry.isPut()) {
				if(entry.isHidden())
					hidden.add(entry.getName());
				else {
					fields.add(new Pair<DomainField, String>(p.getAnyClass(TextUtils.tranformString(name, true, true, false, "")).getField(entry.getName()), entry.getType()));
					messages.put(name + "." + entry.getName(), new Pair<String, String>(entry.getNameES(), entry.getNameEN()));
				}
			}
		}
		
		//comprobamos si existe
		String path = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/edit.jsp";
		if(!msg) {
			int num = JOptionPane.OK_OPTION;
			File file = new File(path).getAbsoluteFile();
			if(file.exists()) {
				num = JOptionPane.showConfirmDialog(DpxFrame.get(), "�Cuidado! El archivo \"" + file.getAbsolutePath() + "\" ya existe. �Desea sobreescribirlo?", 
						"El archivo ya existe", JOptionPane.WARNING_MESSAGE);
			
				if(num != JOptionPane.OK_OPTION)
					return;
			}
		} else {
			int num = JOptionPane.OK_OPTION;
			num = JOptionPane.showConfirmDialog(DpxFrame.get(), "�Desea realizar esta acci�n?", "�Est� seguro?", JOptionPane.WARNING_MESSAGE);
		
			if(num != JOptionPane.OK_OPTION)
				return;
		}
		
		//mensajes
		messages.put(name + ".save", new Pair<String, String>(tab.getGuardarES(), tab.getGuardarEN()));
		messages.put(name + ".delete", new Pair<String, String>(tab.getEliminarES(), tab.getEliminarEN()));
		messages.put(name + ".confirm.delete", new Pair<String, String>(tab.getCofonfirmES(), tab.getCofonfirmEN()));
		messages.put(name + ".cancel", new Pair<String, String>(tab.getCancelES(), tab.getCancelEN()));
		messages.put(name + ".commit.error", new Pair<String, String>(tab.getErrorES(), tab.getErrorEN()));
		
		String pathEN = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/messages.properties";
		String pathES = tab.getProject().getProjectPath() + "src/main/webapp/views/" + name + "/messages_es.properties";
		toMessage(messages, pathES, true);
		toMessage(messages, pathEN, false);
		
		//guardar vista
		if(!msg)
			View.saveFile(path, View.getVoidView(tab.isCustomTag()) + "\n\n" +View.getEditForm(name, tab.isCustomTag(), hidden, fields));
	}

	private void toMessage(Map<String, Pair<String, String>> messages, String pathES, boolean es) {
		Collection<String> res = new LinkedList<String>();
		for(String str : TextUtils.readLineTextFile(pathES)) {
			String split[] = str.split("[=]");
			if(split.length >= 2) {
				Pair<String, String> obj = messages.get(split[0].trim());
				if(obj == null)
					res.add(str);
			} else
				res.add(str);
		}
		
		for(String str : messages.keySet())
			res.add(str + " = " + (es ? messages.get(str).getFirst() : messages.get(str).getSecond()));
		
		String recons = "";
		for(String str : res)
			recons += (str + "\n");
		
		recons.substring(0, recons.length()-1);
		View.saveFile(pathES, recons);
	}
	
	private void copy(Project p) {
		String name = TextUtils.tranformString((String) tab.getComboBox().getSelectedItem(), false, true, false, "");
		Collection<Pair<DomainField, String>> fields = new LinkedList<Pair<DomainField, String>>();
		Collection<String> hidden = new LinkedList<String>();
		for(EditEntry entry : tab.getEntries()) {
			if(entry.isPut()) {
				if(entry.isHidden())
					hidden.add(entry.getName());
				else
					fields.add(new Pair<DomainField, String>(p.getAnyClass(TextUtils.tranformString(name, true, true, false, "")).getField(entry.getName()), entry.getType()));
			}
		}
		
		UtilsGui.copyToClippboard(View.getEditForm(name, tab.isCustomTag(), hidden, fields));
	}
}
