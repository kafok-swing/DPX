package com.dpx.gui.controller.main.workspace.editor.prune;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JCheckBox;

import com.dpx.core.domain.Prune;
import com.dpx.core.domain.clazz.DomainClass;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.gui.view.main.DpxFrame;
import com.dpx.gui.view.main.workspace.tabs.prune.PruneTab;

public class PruneTabController implements ActionListener {

	private PruneTab tab;

	public PruneTabController(PruneTab tab) {
		super();
		this.tab = tab;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "entidad":
				String item = tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex());
				DomainClass clazz = tab.getProject().getClasses().get(item);
				tab.getFields().setFields(clazz.getAllField());
				
				tab.getTextField().setText(item + "Form");
				
				tab.revalidate();
				tab.repaint();
				break;
				
			case "create":
				String item2 = tab.getComboBox().getItemAt(tab.getComboBox().getSelectedIndex());
				Map<String, DomainField> fields = new LinkedHashMap<String, DomainField>();
				DomainClass clazz1 = tab.getProject().getClasses().get(item2);
				for(DomainField f : clazz1.getAllField())
					fields.put(f.getName(), f);
				
				for(JCheckBox cb : tab.getFields().getFields())
					if(!cb.isSelected())
						fields.remove(cb.getText());
				
				Prune.saveFile(tab.getProject(), fields.values(), tab.getTextField().getText());
				
				DpxFrame.get().getWorkspace().getNavigator().getNavigatorModel().reloadProject(tab.getProject());
				break;
				
			case "edit":
				break;
				
			case "sall":
				for(JCheckBox cb : tab.getFields().getFields())
					cb.setSelected(true);
				break;
				
			case "qall":
				for(JCheckBox cb : tab.getFields().getFields())
					cb.setSelected(false);
				break;
		}
	}
}
