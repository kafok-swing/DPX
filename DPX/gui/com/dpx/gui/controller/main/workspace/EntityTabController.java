package com.dpx.gui.controller.main.workspace;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.dpx.gui.view.main.workspace.tabs.EntityTab;

public class EntityTabController {

	private EntityTab view;

	public EntityTabController(EntityTab view) {
		super();
		this.view = view;
	}
	
	
	public static class ButtonDelete implements ActionListener {
		
		private Container container;
		private Component[] comp;
		
		public ButtonDelete(Container container, Component... comp) {
			super();
			this.container = container;
			this.comp = comp;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			for(Component cc : comp)
				cc.getParent().remove(cc);
			
			Container c = container.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
			container.getParent().remove(container);
			c.repaint();
		}
	}
}
