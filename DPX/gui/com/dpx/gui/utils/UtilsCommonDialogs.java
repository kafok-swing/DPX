package com.dpx.gui.utils;

import java.awt.Component;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.dpx.core.common.CustomFileFilter;
import com.dpx.core.common.Pair;

public class UtilsCommonDialogs {
	
	public static String chooseDirectory(String title, String pathInit, Component frameCaller) {
		JFileChooser chooser;
		File fInit = new File(pathInit);
		String res = fInit.getAbsolutePath();
		
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(fInit);
	    chooser.setDialogTitle(title);
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);
	    
		if (chooser.showOpenDialog(frameCaller) == JFileChooser.APPROVE_OPTION)
			res = chooser.getSelectedFile().getAbsolutePath();
		else
			return null;
		
		return res;
	}
	
	
	
	
	public static String chooseFile(String title, String pathInit, Component frameCaller) {
		List<Pair<String, String>> extensions = new LinkedList<Pair<String, String>>();
		return chooseFile(title, pathInit, extensions, frameCaller);
	}
	
	public static String chooseFile(String title, String pathInit, Pair<String, String> extension, Component frameCaller) {
		List<Pair<String, String>> extensions = new LinkedList<Pair<String, String>>();
		extensions.add(extension);
		return chooseFile(title, pathInit, extensions, frameCaller);
	}
	
	public static String chooseFile(String title, String pathInit, List<Pair<String, String>> extensions, Component frameCaller) {
		JFileChooser chooser;
		File fInit = new File(pathInit);
		String res = fInit.getAbsolutePath();
		
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(fInit);
	    chooser.setDialogTitle(title);
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    chooser.setAcceptAllFileFilterUsed(extensions.isEmpty());
	    
	    for(Pair<String, String> p : extensions) {
	    	FileFilter ff = new CustomFileFilter(p.getFirst(), p.getSecond());
	    	chooser.addChoosableFileFilter(ff);
	    }
	    
		if (chooser.showOpenDialog(frameCaller) == JFileChooser.APPROVE_OPTION)
			res = chooser.getSelectedFile().getAbsolutePath();
		else
			return null;
		
		return res;
	}
	
	
	public static List<String> chooseFiles(String title, String pathInit, Component frameCaller) {
		List<Pair<String, String>> extensions = new LinkedList<Pair<String, String>>();
		return chooseFiles(title, pathInit, extensions, frameCaller);
	}
	
	public static List<String> chooseFiles(String title, String pathInit, Pair<String, String> extension, Component frameCaller) {
		List<Pair<String, String>> extensions = new LinkedList<Pair<String, String>>();
		extensions.add(extension);
		return chooseFiles(title, pathInit, extensions, frameCaller);
	}
	
	public static List<String> chooseFiles(String title, String pathInit, List<Pair<String, String>> extensions, Component frameCaller) {
		JFileChooser chooser;
		File fInit = new File(pathInit);
		List<String> res = new LinkedList<String>();
		
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(fInit);
	    chooser.setDialogTitle(title);
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    chooser.setAcceptAllFileFilterUsed(extensions.isEmpty());
	    chooser.setMultiSelectionEnabled(true);
	    
	    for(Pair<String, String> p : extensions) {
	    	FileFilter ff = new CustomFileFilter(p.getFirst(), p.getSecond());
	    	chooser.addChoosableFileFilter(ff);
	    }
	    
		if (chooser.showOpenDialog(frameCaller) == JFileChooser.APPROVE_OPTION) {
			for(File f : chooser.getSelectedFiles())
				res.add(f.getAbsolutePath());
		} else
			return null;
		
		return res;
	}

}
