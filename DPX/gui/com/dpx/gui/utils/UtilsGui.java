package com.dpx.gui.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.UIManager;

public class UtilsGui {

	public static boolean setSystemLookAndFeel() {
		try {
			 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static void copyToClippboard(String res) {
		StringSelection s = new StringSelection(res);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(s, null);
	}
	
	public static String copyFromClippboard() {
		try {
			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable ct = clpbrd.getContents(null);
			return (String) ct.getTransferData(DataFlavor.stringFlavor);
		} catch(Throwable t) {
			return null;
		}
	}
}
