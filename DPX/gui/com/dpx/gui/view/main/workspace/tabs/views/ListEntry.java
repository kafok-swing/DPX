package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.view.Message;
import com.dpx.gui.components.LabelButton;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class ListEntry extends JPanel {
	
	private JTextField titleES;
	private JTextField titleEN;
	private JCheckBox chckbxPonerEnLa;
	private JCheckBox chckbxSorted;
	private JLabel lblNombreEnEspaol;
	private JLabel label;
	private DomainField field;
	private JPanel panel_3;
	private JPanel panel_4;
	private LabelButton btnUp;
	private LabelButton btnDown;
	private String entity;
	
	public ListEntry(ActionListener controller, String entity, boolean active, DomainField field, Message msg) {
		this.field = field;
		this.entity = entity;
		
		setBorder(new TitledBorder(null,  "<html><b>" + field.getName() + "</b></html>", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		panel_4 = new JPanel();
		add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		btnUp = new LabelButton(24, Resources.get().getIcon("up"));
		btnUp.addActionListener(controller);
		btnUp.setActionCommand("up");
		panel_4.add(btnUp, BorderLayout.NORTH);
		
		btnDown = new LabelButton(24, Resources.get().getIcon("down"));
		btnDown.addActionListener(controller);
		btnDown.setActionCommand("down");
		panel_4.add(btnDown, BorderLayout.SOUTH);
		
		panel_3 = new JPanel();
		add(panel_3);
		
		JPanel panel = new JPanel();
		panel_3.add(panel);
		panel.setLayout(new GridLayout(2, 3, 0, 0));
		
		lblNombreEnEspaol = new JLabel("Nombre en espa\u00F1ol");
		panel.add(lblNombreEnEspaol);
		lblNombreEnEspaol.setEnabled(active);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		titleES = new JTextField();
		panel_1.add(titleES);
		titleES.setText(msg.getES(entity + "." + field.getName(), field.getName()));
		titleES.setEnabled(active);
		titleES.setColumns(15);
		
		chckbxSorted = new JCheckBox("Ordenable");
		panel.add(chckbxSorted);
		chckbxSorted.setEnabled(active);
		
		label = new JLabel("Nombre en ingl\u00E9s");
		panel.add(label);
		label.setEnabled(active);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		
		titleEN = new JTextField();
		panel_2.add(titleEN);
		titleEN.setText(msg.getEN(entity + "." + field.getName(), field.getName()));
		titleEN.setEnabled(active);
		titleEN.setColumns(15);
		
		chckbxPonerEnLa = new JCheckBox("Poner en la vista");
		chckbxPonerEnLa.setSelected(active);
		panel.add(chckbxPonerEnLa);
		chckbxPonerEnLa.addActionListener(controller);
		chckbxPonerEnLa.setActionCommand("put");
		
		setEnabled(active);
	}
	
	
	public void setPut(boolean active) {
		lblNombreEnEspaol.setEnabled(active);
		label.setEnabled(active);
		titleES.setEnabled(active);
		chckbxSorted.setEnabled(active);
		titleEN.setEnabled(active);
		
		setEnabled(active);
		chckbxPonerEnLa.setSelected(active);
	}
	
	public String getName() {
		return field.getName();
	}
	
	public boolean isPut() {
		return chckbxPonerEnLa.isSelected();
	}
	
	public boolean isSortable() {
		return chckbxSorted.isSelected();
	}
	
	public String getTitleES() {
		return titleES.getText();
	}
	
	public String getTitleEN() {
		return titleEN.getText();
	}

	public DomainField getField() {
		return field;
	}

	public String getEntity() {
		return entity;
	}

}
