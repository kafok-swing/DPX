package com.dpx.gui.view.main.workspace;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

@SuppressWarnings("serial")
public class Workspace extends JPanel {

	//-------------------------------------------- Attributes --------------------------------------------

	private JSplitPane wsSplit;
	private Navigator navigator;
	private Editor editor;

	
	//------------------------------------------- Constructor --------------------------------------------

	public Workspace() {
		super();
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setBackground(new Color(208, 218, 255));
		
		wsSplit = new JSplitPane();
		wsSplit.setResizeWeight(0.2);
		wsSplit.setOneTouchExpandable(true);
		add(wsSplit);
		
		navigator = new Navigator();
		wsSplit.setLeftComponent(navigator);
		
		editor = new Editor();
		wsSplit.setRightComponent(editor);
	}
	

	//--------------------------------------------- Getters ----------------------------------------------

	public JSplitPane getWsSplit() {
		return wsSplit;
	}

	public Navigator getNavigator() {
		return navigator;
	}

	public Editor getEditor() {
		return editor;
	}
}
