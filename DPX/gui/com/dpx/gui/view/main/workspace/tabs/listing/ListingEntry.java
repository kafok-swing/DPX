package com.dpx.gui.view.main.workspace.tabs.listing;

import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ListingEntry extends JPanel{
	
	private JCheckBox checkBox;
	
	public ListingEntry(String name, boolean exists) {
		setBackground(null);
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		Box horizontalBox = Box.createHorizontalBox();
		add(horizontalBox);
		
		checkBox = new JCheckBox(exists ? "<html><b>" + name + "</b></html>" : name);
		checkBox.setSelected(!exists);
		horizontalBox.add(checkBox);
	}
	
	public boolean isSelected() {
		return checkBox.isSelected();
	}
	
	public String getName() {
		String res = checkBox.getText();
		res = res.replace("<html><b>", "");
		res = res.replace("</b></html>", "");
		return res;
	}
	
	public boolean isCreate() {
		return checkBox.getText().startsWith("<");
	}
	
	public void setSelected(boolean sel) {
		checkBox.setSelected(sel);
	}
}
