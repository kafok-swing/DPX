package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.Message;

@SuppressWarnings("serial")
public class EditEntry extends JPanel {
	
	private JTextField nameES;
	private JTextField nameEN;
	private JCheckBox chckbxPoner;
	private JCheckBox chckbxHidden;
	private JLabel lblNombreEnEspaol;
	private JLabel label;
	private String name;
	private JComboBox<String> comboBox;
	
	public EditEntry(ActionListener controller, DomainField attr, String entity, boolean hidden, boolean active, Message msg) {
		name = attr.getName();
		setBorder(new TitledBorder(null, "<html><b>" + name + "</b></html>", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		entity = TextUtils.tranformString(entity, false, true, false, "");
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JPanel panel_2 = new JPanel();
		panel_4.add(panel_2);
		panel_2.setLayout(new GridLayout(2, 3, 0, 0));
		
		lblNombreEnEspaol = new JLabel("Nombre en espa\u00F1ol");
		panel_2.add(lblNombreEnEspaol);
		lblNombreEnEspaol.setEnabled(active && !hidden);
		
		JPanel panel = new JPanel();
		panel_2.add(panel);
		
		nameES = new JTextField();
		nameES.setEnabled(active && !hidden);
		nameES.setText(msg.getES(entity + "." + attr.getName(), attr.getName()));
		panel.add(nameES);
		nameES.setColumns(15);
		
		chckbxHidden = new JCheckBox("Hidden");
		panel_2.add(chckbxHidden);
		chckbxHidden.setActionCommand("hidden");
		chckbxHidden.addActionListener(controller);
		chckbxHidden.setEnabled(active);
		chckbxHidden.setSelected(hidden);
		
		label = new JLabel("Nombre en ingl\u00E9s");
		panel_2.add(label);
		label.setEnabled(active && !hidden);
		
		JPanel panel_1 = new JPanel();
		panel_2.add(panel_1);
		
		nameEN = new JTextField();
		nameEN.setEnabled(active && !hidden);
		nameEN.setText(msg.getEN(entity + "." + attr.getName(), attr.getName()));
		panel_1.add(nameEN);
		nameEN.setColumns(15);
		
		chckbxPoner = new JCheckBox("Poner en la vista");
		panel_2.add(chckbxPoner);
		chckbxPoner.setActionCommand("put");
		chckbxPoner.addActionListener(controller);
		chckbxPoner.setSelected(active);
		
		this.setEnabled(active);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		add(panel_3);
		
		comboBox = new JComboBox<String>();
		comboBox.setEnabled(active && !hidden);
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"input", "password", "textarea"}));
		panel_3.add(comboBox);
	}

	public void setHidden(boolean hidden) {
		boolean active = chckbxPoner.isSelected();
		
		lblNombreEnEspaol.setEnabled(active && !hidden);
		nameES.setEnabled(active && !hidden);
		chckbxHidden.setEnabled(active);
		chckbxHidden.setSelected(hidden);
		label.setEnabled(active && !hidden);
		nameEN.setEnabled(active && !hidden);
		comboBox.setEnabled(active && !hidden);
		this.setEnabled(active);
	}
	
	public void setPut(boolean active) {
		boolean hidden = chckbxHidden.isSelected();
		
		lblNombreEnEspaol.setEnabled(active && !hidden);
		nameES.setEnabled(active && !hidden);
		chckbxHidden.setEnabled(active);
		label.setEnabled(active && !hidden);
		nameEN.setEnabled(active && !hidden);
		chckbxPoner.setSelected(active);
		comboBox.setEnabled(active && !hidden);
		this.setEnabled(active);
	}
	
	
	public String getNameES() {
		return nameES.getText();
	}

	public String getNameEN() {
		return nameEN.getText();
	}

	public boolean isPut() {
		return chckbxPoner.isSelected();
	}

	public boolean isHidden() {
		return chckbxHidden.isSelected();
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return (String) comboBox.getSelectedItem();
	}
}
