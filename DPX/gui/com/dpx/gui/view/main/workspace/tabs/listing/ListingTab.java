package com.dpx.gui.view.main.workspace.tabs.listing;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.dpx.core.Project;
import com.dpx.core.common.Consumer;
import com.dpx.core.common.Pair;
import com.dpx.gui.controller.main.workspace.editor.repository.ListingTabController;

@SuppressWarnings("serial")
public class ListingTab extends JPanel {
	
	private List<ListingEntry> entries;
	
	public ListingTab(final Project p, String title, String nameButton, Collection<Pair<String, Boolean>> repos, Consumer<Collection<String>> action) {
		setBackground(null);
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		ListingTabController controller = new ListingTabController(this, action);
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setBorder(new TitledBorder(null, title, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(verticalBox);
		
		entries = new LinkedList<ListingEntry>();
		for(Pair<String, Boolean> r : repos) {
			ListingEntry entry = new ListingEntry(r.getFirst(), r.getSecond());
			addEntry(verticalBox, entry);
		}
		
		//botones
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalBox.add(verticalStrut);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);
		
		JButton btnCrearRepositorios = new JButton(nameButton);
		btnCrearRepositorios.setActionCommand("createAll");
		btnCrearRepositorios.addActionListener(controller);
		horizontalBox.add(btnCrearRepositorios);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalBox.add(horizontalStrut);
		
		JButton btnSeleccionarTodo = new JButton("Seleccionar todo");
		btnSeleccionarTodo.setActionCommand("selectAll");
		btnSeleccionarTodo.addActionListener(controller);
		horizontalBox.add(btnSeleccionarTodo);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut_1);
		
		JButton btnQuitarTodo = new JButton("Quitar todo");
		btnQuitarTodo.setActionCommand("quitAll");
		btnQuitarTodo.addActionListener(controller);
		
		JButton btnSeleccionarNoCreados = new JButton("Seleccionar no creados");
		btnSeleccionarNoCreados.setActionCommand("ncreate");
		btnSeleccionarNoCreados.addActionListener(controller);
		horizontalBox.add(btnSeleccionarNoCreados);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut_2);
		horizontalBox.add(btnQuitarTodo);
	}
	
	
	private void addEntry(Box box, ListingEntry entry) {
		entries.add(entry);
		box.add(entry);
	}

	public List<ListingEntry> getEntries() {
		return entries;
	}
}
