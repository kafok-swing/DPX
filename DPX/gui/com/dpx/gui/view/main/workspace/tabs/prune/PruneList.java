package com.dpx.gui.view.main.workspace.tabs.prune;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.dpx.core.domain.clazz.DomainField;

@SuppressWarnings("serial")
public class PruneList extends JPanel {

	private Collection<JCheckBox> fields;
	private Box box;
	
	public PruneList(Collection<DomainField> fields) {
		this.fields = new LinkedList<JCheckBox>();
		
		setBorder(new TitledBorder(null, "Campos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		FlowLayout flowLayout_1 = (FlowLayout) getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		
		box = Box.createVerticalBox();
		add(box);
		box.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		for(DomainField f : fields) {
			JCheckBox chckbx = new JCheckBox(f.getName());
			chckbx.setSelected(!f.isCollection() && !f.getType().isOtherClass());
			box.add(chckbx);
			this.fields.add(chckbx);
		}
	}

	
	
	public Collection<JCheckBox> getFields() {
		return fields;
	}

	public void setFields(Collection<DomainField> fields) {
		box.removeAll();
		
		this.fields = new LinkedList<JCheckBox>();
		for(DomainField f : fields) {
			JCheckBox chckbx = new JCheckBox(f.getName());
			chckbx.setSelected(!f.isCollection() && !f.getType().isOtherClass());
			box.add(chckbx);
			this.fields.add(chckbx);
		}
	}
	
}
