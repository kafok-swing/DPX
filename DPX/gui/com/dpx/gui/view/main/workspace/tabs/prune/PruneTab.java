package com.dpx.gui.view.main.workspace.tabs.prune;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.dpx.core.Project;
import com.dpx.gui.controller.main.workspace.editor.prune.PruneTabController;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PruneTab extends JPanel {
	
	private JComboBox<String> comboBox;
	private JButton btnCrear;
	private JButton btnEditar;
	private JButton btnSeleccionarTodo;
	private JButton btnQuitarTodo;
	private Project project;
	private PruneList fields;
	private JTextField textField;

	
	public PruneTab(Project project) {
		this.project = project;
		PruneTabController controller = new PruneTabController(this);
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		setBackground(null);
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Crear clase pruneada", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		add(verticalBox);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_2.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_2);
		
		String[] entidades = project.getArrayDomainClass();
		
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalBox.add(verticalStrut);
		
		fields = new PruneList(project.getClasses().get(entidades[0]).getAllField());
		verticalBox.add(fields);
		
		Box verticalBox_1 = Box.createVerticalBox();
		panel_2.add(verticalBox_1);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox);
		
		JLabel lblEntidad = new JLabel("Entidad");
		horizontalBox.add(lblEntidad);
		
		textField = new JTextField();
		Component horizontalStrut = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut);
		comboBox = new JComboBox<String>();
		comboBox.setEditable(true);
		comboBox.setActionCommand("entidad");
		comboBox.setModel(new DefaultComboBoxModel<String>(entidades));
		comboBox.addActionListener(controller);
		horizontalBox.add(comboBox);
		comboBox.setSelectedIndex(0);
		
		Component verticalStrut_2 = Box.createVerticalStrut(10);
		verticalBox_1.add(verticalStrut_2);
		
		Box horizontalBox_3 = Box.createHorizontalBox();
		verticalBox_1.add(horizontalBox_3);
		
		JLabel lblNombre = new JLabel("Nombre");
		horizontalBox_3.add(lblNombre);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(5);
		horizontalBox_3.add(horizontalStrut_3);
		
		textField.setColumns(10);
		horizontalBox_3.add(textField);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		verticalBox.add(verticalStrut_1);
		
		JPanel panel = new JPanel();
		verticalBox.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		panel.add(horizontalBox_1, BorderLayout.WEST);
		
		btnCrear = new JButton("Crear");
		btnCrear.setActionCommand("create");
		btnCrear.addActionListener(controller);
		horizontalBox_1.add(btnCrear);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox_1.add(horizontalStrut_1);
		
		btnEditar = new JButton("Editar");
		btnEditar.setActionCommand("edit");
		btnEditar.addActionListener(controller);
		btnEditar.setEnabled(false);
		horizontalBox_1.add(btnEditar);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		panel.add(horizontalBox_2, BorderLayout.EAST);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(10);
		horizontalBox_2.add(horizontalStrut_2);
		
		btnSeleccionarTodo = new JButton("Seleccionar todo");
		btnSeleccionarTodo.setActionCommand("sall");
		btnSeleccionarTodo.addActionListener(controller);
		horizontalBox_2.add(btnSeleccionarTodo);
		
		btnQuitarTodo = new JButton("Quitar todo");
		btnQuitarTodo.setActionCommand("qall");
		btnQuitarTodo.addActionListener(controller);
		horizontalBox_2.add(btnQuitarTodo);
	}

	
	
	public JTextField getTextField() {
		return textField;
	}

	public PruneList getFields() {
		return fields;
	}
	
	public Project getProject() {
		return project;
	}
	
	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public JButton getBtnCrear() {
		return btnCrear;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnSeleccionarTodo() {
		return btnSeleccionarTodo;
	}

	public JButton getBtnQuitarTodo() {
		return btnQuitarTodo;
	}
	
	
	
}
