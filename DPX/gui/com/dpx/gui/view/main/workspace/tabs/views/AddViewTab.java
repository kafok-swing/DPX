package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.dpx.core.Project;
import com.dpx.core.common.Tuple3;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.View;
import com.dpx.gui.components.LabelButton;
import com.dpx.gui.controller.main.workspace.editor.views.AddViewTabController;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class AddViewTab extends JPanel {
	
	private Collection<ViewEntry> views;
	private Box verticalBox_1;
	private AddViewTabController controller;
	private Project project;
	private JComboBox<String> entities;
	
	public AddViewTab(Project project) {
		controller = new AddViewTabController(this);
		this.views = new LinkedList<ViewEntry>();
		this.project = project;
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		Box verticalBox = Box.createVerticalBox();
		add(verticalBox);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel);
		
		JLabel lblEntidad = new JLabel("Entidad");
		panel.add(lblEntidad);
		
		entities = new JComboBox<String>();
		entities.setModel(new DefaultComboBoxModel<String>(project.getArrayAllDomainClass()));
		entities.setEditable(true);
		entities.addActionListener(controller);
		entities.setActionCommand("entity");
		panel.add(entities);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut);
		
		verticalBox_1 = Box.createVerticalBox();
		verticalBox.add(verticalBox_1);
		
		JPanel panel_1 = new JPanel();
		verticalBox_1.add(panel_1);
		
		LabelButton btnAdd = new LabelButton(24, Resources.get().getIcon("add"));
		btnAdd.setToolTipText("A�adir vista");
		btnAdd.addActionListener(controller);
		btnAdd.setActionCommand("add");
		panel_1.add(btnAdd);
		
		changeEntity(entities.getItemAt(0));
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_3.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_3);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(controller);
		btnGuardar.setActionCommand("save");
		panel_3.add(btnGuardar);
		
		JButton btnEliminarVistasNo = new JButton("Eliminar vistas no mapeadas");
		btnEliminarVistasNo.addActionListener(controller);
		btnEliminarVistasNo.setActionCommand("deleteFile");
		panel_3.add(btnEliminarVistasNo);
	}


	public void addEntry() {
		ViewEntry entry = new ViewEntry("", "", "", controller);
		verticalBox_1.add(entry);
		views.add(entry);
		
		this.revalidate();
		this.repaint();
	}
	
	public void removeEntry(ViewEntry entry) {
		verticalBox_1.remove(entry);
		views.remove(entry);
		verticalBox_1.revalidate();
		verticalBox_1.repaint();
	}
	
	public void changeEntity(String name) {
		for(ViewEntry entry : views)
			verticalBox_1.remove(entry);
		views.clear();
		
		for(Tuple3<String, String, String> v : View.getViewByEntityName(project, name)) {
			ViewEntry entry = new ViewEntry(v.getFirst(), v.getSecond(), v.getThird(), controller);
			verticalBox_1.add(entry);
			views.add(entry);
		}
		
		verticalBox_1.revalidate();
		verticalBox_1.repaint();
	}
	
	public Collection<Tuple3<String, String, String>> getParamsES() {
		Collection<Tuple3<String, String, String>> res = new LinkedList<Tuple3<String, String, String>>();
		for(ViewEntry entry : views)
			res.add(new Tuple3<String, String, String>(TextUtils.tranformString((String)entities.getSelectedItem(), false, true, false, ""),
					entry.getViewName().getText(), entry.getTextFieldES().getText()));
		
		return res;
	}
	
	public Collection<Tuple3<String, String, String>> getParamsEN() {
		Collection<Tuple3<String, String, String>> res = new LinkedList<Tuple3<String, String, String>>();
		for(ViewEntry entry : views)
			res.add(new Tuple3<String, String, String>(TextUtils.tranformString((String)entities.getSelectedItem(), false, true, false, ""),
					entry.getViewName().getText(), entry.getTextFieldEN().getText()));
		
		return res;
	}
	
	
	public Collection<ViewEntry> getViews() {
		return views;
	}
	
	public Project getProject() {
		return project;
	}
	
	public JComboBox<String> getEntities() {
		return entities;
	}
}
