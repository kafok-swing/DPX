package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.dpx.gui.components.LabelButton;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class ViewEntry extends JPanel {
	
	private JTextField viewName;
	private JTextField textFieldES;
	private JTextField textFieldEN;


	public ViewEntry(String name, String es, String en, ActionListener controller) {
		setBorder(null);
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_4);
		
		Box verticalBox = Box.createVerticalBox();
		panel_4.add(verticalBox);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel);
		
		JLabel lblNombre = new JLabel("Nombre");
		panel.add(lblNombre);
		
		viewName = new JTextField();
		viewName.setText(name);
		panel.add(viewName);
		viewName.setColumns(30);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_1.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_1);
		
		JLabel lblEs = new JLabel("T�tulo en espa�ol");
		panel_1.add(lblEs);
		
		textFieldES = new JTextField();
		textFieldES.setText(es);
		textFieldES.setColumns(30);
		panel_1.add(textFieldES);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_2.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_2);
		
		JLabel lblTtuloEnIngls = new JLabel("T\u00EDtulo en ingl\u00E9s");
		panel_2.add(lblTtuloEnIngls);

		textFieldEN = new JTextField();
		textFieldEN.setText(en);
		textFieldEN.setColumns(30);
		panel_2.add(textFieldEN);
		
		JPanel panel_3 = new JPanel();
		verticalBox.add(panel_3);
		
		LabelButton delete = new LabelButton(24, Resources.get().getIcon("delete"));
		delete.addActionListener(controller);
		delete.setActionCommand("delete");
		panel_3.add(delete);
	}

	
	public JTextField getViewName() {
		return viewName;
	}

	public JTextField getTextFieldES() {
		return textFieldES;
	}

	public JTextField getTextFieldEN() {
		return textFieldEN;
	}
}
