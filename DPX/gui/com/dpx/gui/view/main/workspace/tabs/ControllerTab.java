package com.dpx.gui.view.main.workspace.tabs;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.dpx.core.Project;
import com.dpx.gui.controller.main.workspace.editor.ControllerTabController;

@SuppressWarnings("serial")
public class ControllerTab extends JPanel {
	
	private Project project;
	private JTextField textField;
	private JComboBox<String> comboBox;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnEditarYListado;
	private JRadioButton rdbtnClaseVacia;
	private List<JCheckBox> authorities;
	private JCheckBox chckbxconMtodoReconstruct;
	private JTextField textFieldSecurity;
	private JButton btnCopiarClase;
	private JButton btnCopiar;

	public ControllerTab(Project project) {
		super();
		authorities = new LinkedList<JCheckBox>();
		ControllerTabController controller = new ControllerTabController(this);
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		this.project = project;
		
		Box verticalBox = Box.createVerticalBox();
		add(verticalBox);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);
		
		JLabel lblEntidad = new JLabel("Entidad");
		horizontalBox.add(lblEntidad);
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(project.getArrayDomainClass()));
		comboBox.setEditable(true);
		comboBox.setActionCommand("entidades");
		comboBox.addActionListener(controller);
		horizontalBox.add(comboBox);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_1.getLayout();
		flowLayout_2.setHgap(0);
		flowLayout_2.setVgap(0);
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_1);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		panel_1.add(horizontalBox_1);
		horizontalBox_1.setAlignmentY(Component.CENTER_ALIGNMENT);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setVgap(0);
		flowLayout_1.setHgap(0);
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		horizontalBox_1.add(panel);
		
		JLabel lblAutoridades = new JLabel("Autoridades");
		panel.add(lblAutoridades);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		horizontalBox_1.add(horizontalStrut_2);
		
		Box verticalBox_1 = Box.createVerticalBox();
		horizontalBox_1.add(verticalBox_1);
		
		//autoridades
		JCheckBox chckbxTodos = new JCheckBox("Todos");
		chckbxTodos.setSelected(true);
		chckbxTodos.setActionCommand("auth.all");
		chckbxTodos.addActionListener(controller);
		verticalBox_1.add(chckbxTodos);
		authorities.add(chckbxTodos);
		
		JCheckBox chckbxNoLogueados = new JCheckBox("No logueados");
		verticalBox_1.add(chckbxNoLogueados);
		chckbxNoLogueados.setActionCommand("auth.nl");
		chckbxNoLogueados.addActionListener(controller);
		authorities.add(chckbxNoLogueados);
		
		JCheckBox chckbxLogueado = new JCheckBox("Logueado");
		verticalBox_1.add(chckbxLogueado);
		chckbxLogueado.setActionCommand("auth.l");
		chckbxLogueado.addActionListener(controller);
		authorities.add(chckbxLogueado);
		
		for(String auth : project.getAuthorities()) {
			JCheckBox chckbxAutoridades = new JCheckBox(auth);
			chckbxAutoridades.setActionCommand("auth." + auth);
			chckbxAutoridades.addActionListener(controller);
			verticalBox_1.add(chckbxAutoridades);
			authorities.add(chckbxAutoridades);
		}
		
		Component verticalStrut_1 = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut_1);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_2);
		
		JLabel lblRuta = new JLabel("Ruta");
		horizontalBox_2.add(lblRuta);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox_2.add(horizontalStrut_1);
		
		textField = new JTextField();
		textField.setActionCommand("path");
		textField.addActionListener(controller);
		horizontalBox_2.add(textField);
		textField.setColumns(25);
		
		Component verticalStrut_2 = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut_2);
		
		JPanel panel_2 = new JPanel();
		panel_2.setAlignmentY(Component.TOP_ALIGNMENT);
		FlowLayout flowLayout_3 = (FlowLayout) panel_2.getLayout();
		flowLayout_3.setVgap(0);
		flowLayout_3.setHgap(0);
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_2);
		
		Box horizontalBox_3 = Box.createHorizontalBox();
		panel_2.add(horizontalBox_3);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_4 = (FlowLayout) panel_3.getLayout();
		flowLayout_4.setAlignment(FlowLayout.LEFT);
		flowLayout_4.setVgap(0);
		flowLayout_4.setHgap(0);
		horizontalBox_3.add(panel_3);
		
		JLabel lblMtodos = new JLabel("M\u00E9todos");
		panel_3.add(lblMtodos);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(5);
		horizontalBox_3.add(horizontalStrut_3);
		
		Box verticalBox_2 = Box.createVerticalBox();
		horizontalBox_3.add(verticalBox_2);
		
		rdbtnEditarYListado = new JRadioButton("Editar y listado");
		rdbtnEditarYListado.setActionCommand("edit-list");
		rdbtnEditarYListado.addActionListener(controller);
		buttonGroup.add(rdbtnEditarYListado);
		rdbtnEditarYListado.setSelected(true);
		verticalBox_2.add(rdbtnEditarYListado);
		
		rdbtnClaseVacia = new JRadioButton("Clase vacia");
		rdbtnClaseVacia.setActionCommand("none");
		rdbtnClaseVacia.addActionListener(controller);
		buttonGroup.add(rdbtnClaseVacia);
		verticalBox_2.add(rdbtnClaseVacia);
		
		JPanel panel_5 = new JPanel();
		verticalBox.add(panel_5);
		
		Box horizontalBox_4 = Box.createHorizontalBox();
		panel_5.add(horizontalBox_4);
		
		chckbxconMtodoReconstruct = new JCheckBox("\u00BFCon m\u00E9todo reconstruct? (2\u00BA cuatrimestre)");
		horizontalBox_4.add(chckbxconMtodoReconstruct);
		
		JPanel panel_6 = new JPanel();
		verticalBox.add(panel_6);
		
		Box horizontalBox_5 = Box.createHorizontalBox();
		panel_6.add(horizontalBox_5);
		
		JLabel lblSecurity = new JLabel("Security");
		horizontalBox_5.add(lblSecurity);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(5);
		horizontalBox_5.add(horizontalStrut_4);
		
		textFieldSecurity = new JTextField();
		horizontalBox_5.add(textFieldSecurity);
		textFieldSecurity.setColumns(15);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(5);
		horizontalBox_5.add(horizontalStrut_5);
		
		btnCopiar = new JButton("Copiar");
		btnCopiar.setActionCommand("csecurity");
		btnCopiar.addActionListener(controller);
		horizontalBox_5.add(btnCopiar);
		
		Component verticalStrut_3 = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut_3);
		
		JPanel panel_4 = new JPanel();
		FlowLayout flowLayout_5 = (FlowLayout) panel_4.getLayout();
		flowLayout_5.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_4);
		
		JButton btnCrear = new JButton("Crear");
		btnCrear.setActionCommand("create");
		btnCrear.addActionListener(controller);
		panel_4.add(btnCrear);
		
		btnCopiarClase = new JButton("Copiar clase");
		btnCopiarClase.setToolTipText("Copia el contenido que genera el bot\u00F3n crear");
		btnCopiarClase.addActionListener(controller);
		btnCopiarClase.setActionCommand("cclase");
		panel_4.add(btnCopiarClase);
	}

	
	
	public JCheckBox getChckbxconMtodoReconstruct() {
		return chckbxconMtodoReconstruct;
	}

	public Project getProject() {
		return project;
	}

	public JTextField getTextField() {
		return textField;
	}
	
	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public JRadioButton getRdbtnEditarYListado() {
		return rdbtnEditarYListado;
	}

	public JRadioButton getRdbtnClaseVacia() {
		return rdbtnClaseVacia;
	}

	public List<JCheckBox> getAuthorities() {
		return authorities;
	}
	
	public JTextField getTextFieldSecurity() {
		return textFieldSecurity;
	}

	public JButton getBtnCopiarClase() {
		return btnCopiarClase;
	}

	public JButton getBtnCopiar() {
		return btnCopiar;
	}
	
}
