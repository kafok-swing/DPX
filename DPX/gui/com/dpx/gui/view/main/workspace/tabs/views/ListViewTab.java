package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.dpx.core.Project;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.view.Message;
import com.dpx.gui.controller.main.workspace.editor.views.ListViewTabController;

@SuppressWarnings("serial")
public class ListViewTab extends JPanel {
	
	private JComboBox<String> comboBox;
	private List<ListEntry> entries;
	private ListViewTabController controller;
	private Project project;
	private Box entryBox;
	
	public ListViewTab(Project project) {
		controller = new ListViewTabController(this);
		entries = new LinkedList<ListEntry>();
		this.project = project;
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		Box verticalBox = Box.createVerticalBox();
		add(verticalBox);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel);
		
		JLabel lblEntidad = new JLabel("Entidad");
		panel.add(lblEntidad);
		
		comboBox = new JComboBox<String>();
		comboBox.addActionListener(controller);
		comboBox.setActionCommand("entity");
		comboBox.setModel(new DefaultComboBoxModel<String>(project.getArrayAllDomainClass()));
		comboBox.setEditable(true);
		panel.add(comboBox);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(10);
		panel.add(horizontalStrut_1);
		
		JButton btnQuitarDesactivadas = new JButton("Quitar desactivadas");
		btnQuitarDesactivadas.addActionListener(controller);
		btnQuitarDesactivadas.setActionCommand("quitDesc");
		panel.add(btnQuitarDesactivadas);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut);
		
		entryBox = Box.createVerticalBox();
		verticalBox.add(entryBox);
		
		changeModel((String) comboBox.getSelectedItem());
		
		JPanel panel_3 = new JPanel();
		verticalBox.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.WEST);
		
		JButton btnGuardar = new JButton("Crear");
		btnGuardar.addActionListener(controller);
		btnGuardar.setActionCommand("crear");
		panel_1.add(btnGuardar);
		
		JButton btnCopiar = new JButton("Copiar");
		btnCopiar.addActionListener(controller);
		
		JButton btnCrearMensajes = new JButton("Crear mensajes");
		btnCrearMensajes.addActionListener(controller);
		btnCrearMensajes.setActionCommand("msg");
		panel_1.add(btnCrearMensajes);
		btnCopiar.setActionCommand("copy");
		panel_1.add(btnCopiar);
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4, BorderLayout.EAST);
		
		JButton btnPonerTodo = new JButton("Poner todo");
		btnPonerTodo.addActionListener(controller);
		btnPonerTodo.setActionCommand("all");
		panel_4.add(btnPonerTodo);
		
		JButton btnQuitarTodo = new JButton("Quitar todo");
		btnQuitarTodo.addActionListener(controller);
		btnQuitarTodo.setActionCommand("quit");
		panel_4.add(btnQuitarTodo);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_3.add(horizontalStrut, BorderLayout.CENTER);
	}

	
	public void changeModel(String entity) {
		entryBox.removeAll();
		entries.clear();
		
		Message msg = Message.load(project.getProjectPath() + "src\\main\\webapp\\views\\" + entity + "\\messages");
		
		for(DomainField prop : project.getClasses().get(entity).getAllField()) {
			Box panelEntry = Box.createVerticalBox();
			String name = prop.getName();
			ListEntry entry = new ListEntry(controller, entity, !(name.equals("id") || name.equals("version") || prop.isCollection()), prop, msg);
			
			entries.add(entry);
			
			panelEntry.add(entry);
			panelEntry.add(Box.createVerticalStrut(10));
			entryBox.add(panelEntry);
		}
		
		entryBox.revalidate();
		entryBox.repaint();
	}
	
	
	public List<ListEntry> getEntries() {
		return entries;
	}

	public Project getProject() {
		return project;
	}

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public Box getEntryBox() {
		return entryBox;
	}

	public void setEntries(List<ListEntry> entries) {
		this.entries = entries;
	}
	
}
