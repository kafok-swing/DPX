package com.dpx.gui.view.main.workspace;

import javax.swing.JTabbedPane;

import com.dpx.gui.models.main.workspace.EditorModel;

@SuppressWarnings("serial")
public class Editor extends JTabbedPane {
	
	private EditorModel model;
	
	public Editor() {
		super(JTabbedPane.TOP);
		setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		setBackground(null);
		
		model = new EditorModel(this);
	}

	public EditorModel getEditorModel() {
		return model;
	}
}
