package com.dpx.gui.view.main.workspace;

import java.awt.FlowLayout;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.dpx.gui.controller.main.workspace.NavigatorController;
import com.dpx.gui.models.main.workspace.NavigatorModel;

@SuppressWarnings("serial")
public class Navigator extends JTabbedPane {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private NavigatorModel model;
	private NavigatorController controller;
	
	private Set<String> treeProjects;
	private JPanel panel;
	private JTree tree;
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public Navigator() {
		super(JTabbedPane.TOP);
		setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		setBackground(null);

		treeProjects = new HashSet<String>();
		
		model = new NavigatorModel(this);
		controller = new NavigatorController(this);

		panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		JScrollPane scrollPane = new JScrollPane(panel);
		
		DefaultMutableTreeNode nodeRoot = new DefaultMutableTreeNode();
		tree = new JTree(nodeRoot);
		tree.setBackground(null);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		tree.setRootVisible(false);
		tree.setShowsRootHandles(true);
		tree.setEditable(false);
		tree.addMouseListener(controller);
		
		panel.add(tree);
		addTab("Navegador de proyectos", scrollPane);
		
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public NavigatorModel getNavigatorModel() {
		return model;
	}

	public JTree getTree() {
		return tree;
	}

	public JPanel getPanel() {
		return panel;
	}

	public Set<String> getTreeProjects() {
		return treeProjects;
	}
	
	public void addTreeProjects(String path, DefaultMutableTreeNode tree, String name) {
		if(treeProjects.contains(path)) {
			removeTreeProjects(path, name);
		}
		
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		model.insertNodeInto(tree, root, 0);
		
		treeProjects.add(path);
		
		this.tree.setRootVisible(true);
		this.tree.expandRow(0);
		this.tree.setRootVisible(false);
	}
	
	public void removeTreeProjects(String path, String name) {
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

		for(int i=0;  i<root.getChildCount(); i++) {
			if(root.getChildAt(i).toString().equals(name)) {
				root.remove(i);
				break;
			}
		}
		
		model.reload();
		treeProjects.remove(path);
	}
}
