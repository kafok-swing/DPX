package com.dpx.gui.view.main.workspace.tabs.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.dpx.core.Project;
import com.dpx.core.domain.clazz.DomainField;
import com.dpx.core.utils.TextUtils;
import com.dpx.core.view.Message;
import com.dpx.gui.controller.main.workspace.editor.views.EditViewTabController;

@SuppressWarnings("serial")
public class EditViewTab extends JPanel {
	
	private JComboBox<String> comboBox;
	private Box entryBox;
	private Project project;
	private JCheckBox chckbxCustomTags;
	private EditViewTabController controller;
	private Collection<EditEntry> entries;
	private JTextField txtGuardar;
	private JTextField txtSave;
	private JTextField txtEliminar;
	private JTextField txtDelete;
	private JTextField txtdeseaEliminar;
	private JTextField txtareYouSure;
	private JTextField txtCancelar;
	private JTextField txtCancel;
	private JTextField textErrorEN;
	private JTextField textErrorES;
	
	public EditViewTab(Project project) {
		this.project = project;
		this.controller = new EditViewTabController(this);
		this.entries = new LinkedList<EditEntry>();
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		Box verticalBox = Box.createVerticalBox();
		add(verticalBox);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel);
		
		JLabel lblEntidad = new JLabel("Entidad");
		panel.add(lblEntidad);
		
		comboBox = new JComboBox<String>();
		comboBox.addActionListener(controller);
		comboBox.setActionCommand("entity");
		comboBox.setModel(new DefaultComboBoxModel<String>(project.getArrayAllDomainAndFormClass()));
		comboBox.setEditable(true);
		panel.add(comboBox);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut);
		
		entryBox = Box.createVerticalBox();
		verticalBox.add(entryBox);
		
		JPanel panel_3 = new JPanel();
		verticalBox.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.WEST);
		
		JButton btnGuardar = new JButton("Crear");
		btnGuardar.addActionListener(controller);
		btnGuardar.setActionCommand("crear");
		panel_1.add(btnGuardar);
		
		JButton btnCopiar = new JButton("Copiar");
		btnCopiar.addActionListener(controller);
		
		JButton btnCrearMensajes = new JButton("Crear mensajes");
		btnCrearMensajes.setActionCommand("msg");
		btnCrearMensajes.addActionListener(controller);
		panel_1.add(btnCrearMensajes);
		btnCopiar.setActionCommand("copy");
		panel_1.add(btnCopiar);
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4, BorderLayout.EAST);
		
		JButton btnPonerTodo = new JButton("Poner todo");
		btnPonerTodo.addActionListener(controller);
		btnPonerTodo.setActionCommand("all");
		panel_4.add(btnPonerTodo);
		
		JButton btnPonerSeguros = new JButton("Poner seguros");
		btnPonerSeguros.addActionListener(controller);
		btnPonerSeguros.setActionCommand("seguro");
		btnPonerSeguros.setToolTipText("Poner solo campos que sean seguros (2\u00BA cuatrimestre)");
		panel_4.add(btnPonerSeguros);
		
		JButton btnQuitarTodo = new JButton("Quitar todo");
		btnQuitarTodo.addActionListener(controller);
		btnQuitarTodo.setActionCommand("quit");
		panel_4.add(btnQuitarTodo);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_3.add(horizontalStrut, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2, BorderLayout.NORTH);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		JPanel panel_5 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_5.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		panel_2.add(panel_5);
		
		chckbxCustomTags = new JCheckBox("Custom tags (2\u00BA Cuatrimestre)");
		panel_5.add(chckbxCustomTags);
		chckbxCustomTags.setSelected(true);
		chckbxCustomTags.setActionCommand("");
		
		JPanel panel_15 = new JPanel();
		panel_2.add(panel_15);
		
		JPanel panel_6 = new JPanel();
		panel_15.add(panel_6);
		panel_6.setLayout(new GridLayout(10, 2, 0, 0));
		
		JLabel lblGuardarespaol = new JLabel("Guardar (espa\u00F1ol)");
		panel_6.add(lblGuardarespaol);
		
		JPanel panel_14 = new JPanel();
		panel_6.add(panel_14);
		
		txtGuardar = new JTextField();
		panel_14.add(txtGuardar);
		txtGuardar.setColumns(25);
		
		JLabel lblGuardaringls = new JLabel("Guardar (ingl\u00E9s)");
		panel_6.add(lblGuardaringls);
		
		JPanel panel_13 = new JPanel();
		panel_6.add(panel_13);
		
		txtSave = new JTextField();
		panel_13.add(txtSave);
		txtSave.setColumns(25);
		
		JLabel lblBorrarespaol = new JLabel("Borrar (espa\u00F1ol)");
		panel_6.add(lblBorrarespaol);
		
		JPanel panel_12 = new JPanel();
		panel_6.add(panel_12);
		
		txtEliminar = new JTextField();
		panel_12.add(txtEliminar);
		txtEliminar.setColumns(25);
		
		JLabel lblBorraringls = new JLabel("Borrar (ingl\u00E9s)");
		panel_6.add(lblBorraringls);
		
		JPanel panel_11 = new JPanel();
		panel_6.add(panel_11);
		
		txtDelete = new JTextField();
		panel_11.add(txtDelete);
		txtDelete.setColumns(25);
		
		JLabel lblConfirmacinBorrarespaol = new JLabel("Confirmaci\u00F3n borrar (espa\u00F1ol)");
		panel_6.add(lblConfirmacinBorrarespaol);
		
		JPanel panel_10 = new JPanel();
		panel_6.add(panel_10);
		
		txtdeseaEliminar = new JTextField();
		panel_10.add(txtdeseaEliminar);
		txtdeseaEliminar.setColumns(25);
		
		JLabel lblConfirmacinBorraringls = new JLabel("Confirmaci\u00F3n borrar (ingl\u00E9s)");
		panel_6.add(lblConfirmacinBorraringls);
		
		JPanel panel_9 = new JPanel();
		panel_6.add(panel_9);
		
		txtareYouSure = new JTextField();
		panel_9.add(txtareYouSure);
		txtareYouSure.setColumns(25);
		
		JLabel lblCancelarespaol = new JLabel("Cancelar (espa\u00F1ol)");
		panel_6.add(lblCancelarespaol);
		
		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8);
		
		txtCancelar = new JTextField();
		panel_8.add(txtCancelar);
		txtCancelar.setColumns(25);
		
		JLabel lblCancelaringls = new JLabel("Cancelar (ingl\u00E9s)");
		panel_6.add(lblCancelaringls);
		
		JPanel panel_7 = new JPanel();
		panel_6.add(panel_7);
		
		txtCancel = new JTextField();
		panel_7.add(txtCancel);
		txtCancel.setColumns(25);
		
		JLabel lblCommitErrorespaol = new JLabel("Commit error (espa\u00F1ol)");
		panel_6.add(lblCommitErrorespaol);
		
		JPanel panel_16 = new JPanel();
		panel_6.add(panel_16);
		
		textErrorES = new JTextField();
		panel_16.add(textErrorES);
		textErrorES.setColumns(25);
		
		JLabel lblCommitErroringls = new JLabel("Commit error (ingl\u00E9s)");
		panel_6.add(lblCommitErroringls);
		
		JPanel panel_17 = new JPanel();
		panel_6.add(panel_17);
		
		textErrorEN = new JTextField();
		panel_17.add(textErrorEN);
		textErrorEN.setColumns(25);
		
		changeModel(project, (String) comboBox.getSelectedItem());
	}

	public void changeModel(Project p, String entity) {
		entryBox.removeAll();
		entries.clear();
		
		Message msg = Message.load(p.getProjectPath() + "src\\main\\webapp\\views\\" + entity + "\\messages");
		
		for(DomainField prop : project.getAnyClass(entity).getAllField()) {
			Box panelEntry = Box.createVerticalBox();
			String name = prop.getName();
			EditEntry entry = new EditEntry(controller, prop, entity, name.equals("id") || name.equals("version") || prop.isCollection() || prop.getType().isOtherClass(), true, msg);
			
			entries.add(entry);
			
			panelEntry.add(entry);
			panelEntry.add(Box.createVerticalStrut(10));
			entryBox.add(panelEntry);
		}
		
		entryBox.revalidate();
		entryBox.repaint();
		
		//comunes
		entity = TextUtils.tranformString(entity, false, true, false, "");
		txtGuardar.setText(msg.getES(entity + ".save"));
		txtSave.setText(msg.getEN(entity + ".save"));
		txtEliminar.setText(msg.getES(entity + ".delete"));
		txtDelete.setText(msg.getEN(entity + ".delete"));
		txtdeseaEliminar.setText(msg.getES(entity + ".confirm.delete"));
		txtareYouSure.setText(msg.getEN(entity + ".confirm.delete"));
		txtCancelar.setText(msg.getES(entity + ".cancel"));
		txtCancel.setText(msg.getEN(entity + ".cancel"));
		textErrorES.setText(msg.getES(entity + ".commit.error"));
		textErrorEN.setText(msg.getEN(entity + ".commit.error"));
		
		fillCommonMessage();
	}
	
	public void fillCommonMessage() {
		if(txtGuardar.getText().equals(""))
			txtGuardar.setText("Guardar");
		if(txtSave.getText().equals(""))
			txtSave.setText("Save");
		if(txtEliminar.getText().equals(""))
			txtEliminar.setText("Borrar");
		if(txtDelete.getText().equals(""))
			txtDelete.setText("Delete");
		if(txtdeseaEliminar.getText().equals(""))
			txtdeseaEliminar.setText("�Est� seguro?");
		if(txtareYouSure.getText().equals(""))
			txtareYouSure.setText("�Are you sure?");
		if(txtCancelar.getText().equals(""))
			txtCancelar.setText("Cancelar");
		if(txtCancel.getText().equals(""))
			txtCancel.setText("Cancel");
		if(textErrorES.getText().equals(""))
			textErrorES.setText("No se pudo realizar la operaci�n");
		if(textErrorEN.getText().equals(""))
			textErrorEN.setText("Cannot commit this operation");
	}
	
	
	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public Box getEntryBox() {
		return entryBox;
	}

	public Project getProject() {
		return project;
	}

	public Collection<EditEntry> getEntries() {
		return entries;
	}

	public String getGuardarES() {
		return txtGuardar.getText();
	}

	public String getGuardarEN() {
		return txtSave.getText();
	}

	public String getEliminarES() {
		return txtEliminar.getText();
	}

	public String getEliminarEN() {
		return txtDelete.getText();
	}

	public String getCofonfirmES() {
		return txtdeseaEliminar.getText();
	}

	public String getCofonfirmEN() {
		return txtareYouSure.getText();
	}

	public String getCancelES() {
		return txtCancelar.getText();
	}

	public String getCancelEN() {
		return txtCancel.getText();
	}

	public boolean isCustomTag() {
		return chckbxCustomTags.isSelected();
	}
	
	public String getErrorEN() {
		return textErrorEN.getText();
	}

	public String getErrorES() {
		return textErrorES.getText();
	}
	
}
