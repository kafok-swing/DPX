package com.dpx.gui.view.main.workspace.tabs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.dpx.core.Project;
import com.dpx.core.common.Pair;
import com.dpx.core.domain.Attribute;
import com.dpx.core.domain.AttributeType;
import com.dpx.core.domain.Entity;
import com.dpx.core.domain.Relationship;
import com.dpx.core.domain.constrains.Constrain;
import com.dpx.core.domain.constrains.ConstrainDigit;
import com.dpx.core.domain.constrains.ConstrainLength;
import com.dpx.core.domain.constrains.ConstrainMax;
import com.dpx.core.domain.constrains.ConstrainMin;
import com.dpx.core.domain.constrains.ConstrainPattern;
import com.dpx.core.domain.constrains.ConstrainRange;
import com.dpx.core.domain.constrains.ConstrainSize;
import com.dpx.gui.components.LabelButton;
import com.dpx.gui.controller.main.workspace.EntityTabController;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class EntityTab extends JPanel {
	private JTextField fieldNombre;
	private JComboBox<String> comboBoxPadre;
	private JTextField textField;
	
	public EntityTab(final Project project, final Entity entity) {
		setBorder(new LineBorder(new Color(0, 14, 68), 1, true));
		setBackground(new Color(239, 242, 255));
		FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT, 5, 5);
		setLayout(flowLayout);
		
		Box verticalBox = Box.createVerticalBox();
		add(verticalBox);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(null);
		verticalBox.add(panel_1);
		
		JLabel nombre = new JLabel("Nombre ");
		panel_1.add(nombre);
		
		fieldNombre = new JTextField();
		panel_1.add(fieldNombre);
		fieldNombre.setColumns(20);
		fieldNombre.setText(entity.getName());
		
		Component horizontalStrut = Box.createHorizontalStrut(15);
		panel_1.add(horizontalStrut);
		
		JLabel labelPadre = new JLabel("Padre ");
		panel_1.add(labelPadre);
		
		Component verticalStrut = Box.createVerticalStrut(10);
		verticalBox.add(verticalStrut);
		
		final Box boxAtributos = Box.createVerticalBox();
		boxAtributos.setBorder(new TitledBorder(null, "Atributos", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		verticalBox.add(boxAtributos);
		
		LabelButton plusAttr = new LabelButton(24, Resources.get().getIcon("add"));
		plusAttr.setToolTipText("A�adir atributo");
		boxAtributos.add(plusAttr);
		plusAttr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addAttr(entity, boxAtributos, 1, new Attribute("", AttributeType.STRING, false, false, false, false, new LinkedList<Constrain>(), false));
			}
		});
		
		//padre
		Pair<Integer, String[]> posEnts = Entity.getEntitiesAndPos(project, entity);
		if(posEnts.getFirst() != -1) {
			comboBoxPadre = new JComboBox<String>();
			panel_1.add(comboBoxPadre);
			comboBoxPadre.setEditable(true);
			comboBoxPadre.setModel(new DefaultComboBoxModel<String>(posEnts.getSecond()));
			comboBoxPadre.setSelectedIndex(posEnts.getFirst());
		}
		
		//atributos
		int count = 0;
		for(Attribute attr : entity.getAttr()) {
			addAttr(entity, boxAtributos, count, attr);
			count++;
		}
		
		//relaciones
		final Box boxRel = Box.createVerticalBox();
		boxRel.setBorder(new TitledBorder(null, "Relaciones", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		verticalBox.add(Box.createVerticalStrut(20));
		verticalBox.add(boxRel);
		
		LabelButton plusRel = new LabelButton(24, Resources.get().getIcon("add"));
		plusRel.setToolTipText("A�adir relac�n");
		boxRel.add(plusRel);
		plusRel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addRel(project, entity, boxRel, 1, new Relationship("", "", 0, false, false, false));
			}
		});
		
		count = 0;
		for(Attribute attr : entity.getAttr()) {
			if(attr.getType() != AttributeType.DATATYPE && attr.getType() != AttributeType.RELATIONSHIP)
				continue;
			addRel(project, entity, boxRel, count, attr);
			count++;
		}
	}


	private void addAttr(Entity entity, Box boxAtributos, int count, Attribute attr) {
		Component verticalStrut2 = Box.createVerticalStrut(30);
		boxAtributos.add(verticalStrut2);
		
		Box atributos = Box.createHorizontalBox();
		boxAtributos.add(atributos);
		
		JPanel panel = new JPanel();
		panel.setBackground(null);
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		atributos.add(panel);
		
		LabelButton quitar = new LabelButton(24, Resources.get().getIcon("delete"));
		quitar.setToolTipText("Quitar atributo");
		quitar.addActionListener(new EntityTabController.ButtonDelete(atributos, verticalStrut2));
		panel.add(quitar);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		panel.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel<String>(Attribute.getTypes()));
		comboBox.setEditable(true);
		comboBox.setSelectedIndex(Attribute.getIndexByType(entity.getAttr().get(count).getType()));
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(20);
		textField.setText(attr.getName());
		
		panel.add(Box.createHorizontalStrut(5));
		
		JCheckBox chckbxOptativo = new JCheckBox("\u00BFEs optativo?");
		chckbxOptativo.setBackground(null);
		chckbxOptativo.setSelected(attr.isOptative());
		panel.add(chckbxOptativo);
		
		final JComboBox<String> comboBox2 = new JComboBox<String>();
		panel.add(Box.createVerticalStrut(30));
		panel.add(comboBox2);
		comboBox2.setModel(new DefaultComboBoxModel<String>(Constrain.getListConstraints()));
		comboBox2.setEditable(true);
		
		LabelButton plus = new LabelButton(24, Resources.get().getIcon("add"));
		plus.setToolTipText("A�adir restrcci�n");
		panel.add(Box.createVerticalStrut(5));
		panel.add(plus);
		
		//constrain
		final Box constrainsBox = createAllConstrain(attr.getConstrains());
		atributos.add(constrainsBox);
		
		plus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container c = constrainsBox.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
				createConstrain(constrainsBox, Constrain.getByNumber(comboBox2.getSelectedIndex()));
				c.repaint();
			}
		});
	}
	
	private void addRel(Project p, Entity entity, Box boxAtributos, int count, Attribute attr) {
		Component verticalStrut2 = Box.createVerticalStrut(30);
		boxAtributos.add(verticalStrut2);
		
		Box atributos = Box.createHorizontalBox();
		boxAtributos.add(atributos);
		
		JPanel panel = new JPanel();
		panel.setBackground(null);
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		atributos.add(panel);
		
		LabelButton quitar = new LabelButton(24, Resources.get().getIcon("delete"));
		quitar.setToolTipText("Quitar atributo");
		quitar.addActionListener(new EntityTabController.ButtonDelete(atributos, verticalStrut2));
		panel.add(quitar);
		
		Box typeBox = Box.createVerticalBox();
		panel.add(typeBox);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		typeBox.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel<String>(Relationship.getTypes()));
		comboBox.setEditable(true);
		comboBox.setSelectedIndex(((Relationship)attr).getRelType());
		
		Box relBox = Box.createHorizontalBox();
		typeBox.add(relBox);
		
		JComboBox<String> comboBox3 = new JComboBox<String>();
		relBox.add(comboBox3);
		Pair<Integer, String[]> pair = Entity.getEntitiesAndPos(p, ((Relationship)attr).getEntity());
		comboBox3.setModel(new DefaultComboBoxModel<String>(pair.getSecond()));
		comboBox3.setEditable(true);
		comboBox3.setSelectedIndex(pair.getFirst());
		
		LabelButton ir = new LabelButton(24, Resources.get().getIcon("forward"));
		ir.setToolTipText("Ir a la clase relacionada");
		relBox.add(ir);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(20);
		textField.setText(attr.getName());
		
		panel.add(Box.createHorizontalStrut(5));
		
		JCheckBox chckbxOptativo = new JCheckBox("\u00BFEs optativo?");
		chckbxOptativo.setBackground(null);
		chckbxOptativo.setSelected(attr.isOptative());
		panel.add(chckbxOptativo);
		
		final JComboBox<String> comboBox2 = new JComboBox<String>();
		panel.add(Box.createVerticalStrut(30));
		panel.add(comboBox2);
		comboBox2.setModel(new DefaultComboBoxModel<String>(Constrain.getListConstraints()));
		comboBox2.setEditable(true);
		
		LabelButton plus = new LabelButton(24, Resources.get().getIcon("add"));
		plus.setToolTipText("A�adir restrcci�n");
		panel.add(Box.createVerticalStrut(5));
		panel.add(plus);
		
		//constrain
		final Box constrainsBox = createAllConstrain(attr.getConstrains());
		atributos.add(constrainsBox);
		
		plus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Container c = constrainsBox.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
				createConstrain(constrainsBox, Constrain.getByNumber(comboBox2.getSelectedIndex()));
				c.repaint();
			}
		});
	}
	
	
	private Box createAllConstrain(Collection<Constrain> cons) {
		Box constrainsBox = Box.createVerticalBox();
		constrainsBox.setBorder(new TitledBorder(null, "Restricciones", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		for(Constrain c : cons) {
			createConstrain(constrainsBox, c);
		}
		
		return constrainsBox;
	}


	private void createConstrain(Box constrainsBox, Constrain c) {
		switch(c.getType()) {
			case DIGIT:
				ConstrainDigit _c = (ConstrainDigit) c;
				createTwoConstraint("Digits", "Entero:", ""+_c.getInteger(), "Fraccion:", ""+_c.getFraction(), 5, constrainsBox);
				break;
				
			case LENGTH:
				ConstrainLength _c0 = (ConstrainLength) c;
				createTwoConstraint("Length", "Min:", ""+_c0.getMin(), "Max:", ""+_c0.getMax(), 5, constrainsBox);
				break;
				
			case MAX:
				ConstrainMax _c1 = (ConstrainMax) c;
				createOneConstraint("Max", "Max:", ""+_c1.getMax(), 5, constrainsBox);
				break;
				
			case MIN:
				ConstrainMin _c2 = (ConstrainMin) c;
				createOneConstraint("Min", "Min:", ""+_c2.getMin(), 5, constrainsBox);
				break;
				
			case NOT_BLANK:
				createNormalConstrain("NotBlanck", constrainsBox);
				break;
				
			case NOT_EMPTY:
				createNormalConstrain("NotEmpty", constrainsBox);
				break;
				
			case NOT_NULL:
				createNormalConstrain("NotNull", constrainsBox);
				break;
				
			case PAST:
				createNormalConstrain("Past", constrainsBox);
				break;
				
			case PATTERN:
				ConstrainPattern _c3 = (ConstrainPattern) c;
				createOneConstraint("Pattern", "Patr�n:", _c3.getPattern(), 50, constrainsBox);
				break;
				
			case RANGE:
				ConstrainRange _c4 = (ConstrainRange) c;
				createTwoConstraint("Range", "Min:", ""+_c4.getMin(), "Max:", ""+_c4.getMax(), 5, constrainsBox);
				break;
				
			case SIZE:
				ConstrainSize _c5 = (ConstrainSize) c;
				createTwoConstraint("Size", "Min:", ""+_c5.getMin(), "Max:", ""+_c5.getMax(), 5, constrainsBox);
				break;
		}
		
		constrainsBox.add(Box.createVerticalStrut(3));
	}
	
	
	private void createNormalConstrain(String name, Container cont) {
		JPanel panel = new JPanel();
		panel.setBackground(null);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox = Box.createHorizontalBox();
		panel.add(horizontalBox, BorderLayout.WEST);
		
		JLabel nombre = new JLabel(name);
		nombre.setFont(nombre.getFont().deriveFont(Font.BOLD));
		horizontalBox.add(nombre);
		
		buttonDelete(panel, cont);
		
		cont.add(panel);
	}
	
	private void createOneConstraint(String name, String n, String param, int size, Container cont) {
		JPanel panel = new JPanel();
		panel.setBackground(null);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox = Box.createHorizontalBox();
		panel.add(horizontalBox, BorderLayout.WEST);
		
		JLabel nombre = new JLabel(name);
		nombre.setFont(nombre.getFont().deriveFont(Font.BOLD));
		horizontalBox.add(nombre);
		
		Component horizontalStrut = Box.createHorizontalStrut(15);
		horizontalBox.add(horizontalStrut);
		
		JLabel arg0 = new JLabel(n);
		horizontalBox.add(arg0);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut_1);
		
		textField = new JTextField();
		textField.setText(param);
		horizontalBox.add(textField);
		textField.setColumns(size);
		
		buttonDelete(panel, cont);
		
		cont.add(panel);
	}
	
	private void createTwoConstraint(String name, String n1, String param, String n2, String param2, int size, Container cont) {
		JPanel panel = new JPanel();
		panel.setBackground(null);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox = Box.createHorizontalBox();
		panel.add(horizontalBox, BorderLayout.WEST);
		
		JLabel nombre = new JLabel(name);
		nombre.setFont(nombre.getFont().deriveFont(Font.BOLD));
		horizontalBox.add(nombre);
		
		Component horizontalStrut = Box.createHorizontalStrut(15);
		horizontalBox.add(horizontalStrut);
		
		JLabel arg0 = new JLabel(n1);
		horizontalBox.add(arg0);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut_1);
		
		textField = new JTextField();
		textField.setText(param);
		horizontalBox.add(textField);
		textField.setColumns(size);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(15);
		horizontalBox.add(horizontalStrut_2);
		
		JLabel arg1 = new JLabel(n2);
		horizontalBox.add(arg1);
		
		JTextField textField2 = new JTextField();
		textField2.setText(param2);
		horizontalBox.add(textField2);
		textField2.setColumns(size);
		
		buttonDelete(panel, cont);
		
		cont.add(panel);
	}

	
	private void buttonDelete(JPanel panel, Component entry) {
		LabelButton quitar = new LabelButton(24, Resources.get().getIcon("delete"));
		quitar.setToolTipText("Quitar restricci�n");
		Box box = Box.createHorizontalBox();
		box.add(Box.createHorizontalStrut(10));
		box.add(quitar);
		
		panel.add(box, BorderLayout.EAST);
		
		quitar.addActionListener(new EntityTabController.ButtonDelete(panel));
	}

}
