package com.dpx.gui.view.main;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.dpx.gui.controller.main.DpxMenuBarController;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class DpxMenuBar extends JMenuBar {
	
	private JMenu archivo;
	private JMenu editar;
	private JMenu insertar;
	private JMenu configuracin;
	private JMenu ayuda;
	private JMenuItem nuevo;
	private JMenuItem abrir;
	private JMenuItem guardar;
	private JMenuItem guardarTodo;
	
	private DpxMenuBarController controller;
	private JMenuItem salir;
	private JMenuItem recargar;
	
	
	//constructores

	public DpxMenuBar() {
		this.controller = new DpxMenuBarController(this);
		init();
	}
	
	public DpxMenuBar(DpxMenuBarController controller) {
		this.controller = controller;
		init();
	}


	private void init() {
		archivo = new JMenu("Archivo");
		add(archivo);
		
		nuevo = new JMenuItem("Nuevo", Resources.get().getIcon("new"));
		nuevo.setActionCommand("archivo.nuevo");
		nuevo.addActionListener(controller);
		archivo.add(nuevo);
		
		abrir = new JMenuItem("Abrir", Resources.get().getIcon("folder"));
		abrir.setActionCommand("archivo.abrir");
		abrir.addActionListener(controller);
		archivo.add(abrir);
		
		recargar = new JMenuItem("Recargar");
		recargar.setActionCommand("archivo.recargar");
		recargar.addActionListener(controller);
		archivo.add(recargar);
		
		archivo.addSeparator();
		
		guardar = new JMenuItem("Guardar", Resources.get().getIcon("save"));
		guardar.setActionCommand("archivo.guardar");
		guardar.addActionListener(controller);
		archivo.add(guardar);
		
		guardarTodo = new JMenuItem("Guardar todo", Resources.get().getIcon("save_all"));
		guardarTodo.setActionCommand("archivo.guardar_todo");
		guardarTodo.addActionListener(controller);
		archivo.add(guardarTodo);
		
		archivo.addSeparator();
		
		salir = new JMenuItem("Salir");
		salir.setActionCommand("archivo.salir");
		salir.addActionListener(controller);
		archivo.add(salir);
		
		editar = new JMenu("Editar");
		add(editar);
		
		insertar = new JMenu("Insertar");
		add(insertar);
		
		configuracin = new JMenu("Configuración");
		add(configuracin);
		
		ayuda = new JMenu("Ayuda");
		add(ayuda);
	}
	
	
	//getters
	
	public JMenu getArchivo() {
		return archivo;
	}

	public JMenu getEditar() {
		return editar;
	}

	public JMenu getInsertar() {
		return insertar;
	}

	public JMenu getConfiguracin() {
		return configuracin;
	}

	public JMenu getAyuda() {
		return ayuda;
	}

	public JMenuItem getNuevo() {
		return nuevo;
	}

	public JMenuItem getAbrir() {
		return abrir;
	}

	public JMenuItem getGuardar() {
		return guardar;
	}

	public JMenuItem getGuardarTodo() {
		return guardarTodo;
	}

	public JMenuItem getSalir() {
		return salir;
	}
	
	public JMenuItem getRecargar() {
		return recargar;
	}
	
}
