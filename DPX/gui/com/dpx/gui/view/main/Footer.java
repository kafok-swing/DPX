package com.dpx.gui.view.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.dpx.gui.common.DpxProgressable;

@SuppressWarnings("serial")
public class Footer extends JPanel {

	//-------------------------------------------- Attributes --------------------------------------------

	private JPanel progress;
	private JLabel lblAccin;
	private JLabel label;
	private JProgressBar progressBar;
	

	//------------------------------------------- Constructor --------------------------------------------

	public Footer() {
		super();

		setPreferredSize(new Dimension(0, 27));
		setLayout(new BorderLayout(0, 0));
		setBackground(new Color(166, 174, 204));
		
		progress = new JPanel();
		progress.setBackground(null);
		add(progress, BorderLayout.EAST);
		progress.setLayout(new BoxLayout(progress, BoxLayout.X_AXIS));
		
		lblAccin = new JLabel("");
		progress.add(lblAccin);
		
		label = new JLabel(" ");
		progress.add(label);
		
		progressBar = new JProgressBar();
		progress.add(progressBar);
		
		DpxProgressable.create(lblAccin, progressBar);
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public JPanel getProgress() {
		return progress;
	}

	public JLabel getLblAccin() {
		return lblAccin;
	}

	public JLabel getLabel() {
		return label;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}
	
}
