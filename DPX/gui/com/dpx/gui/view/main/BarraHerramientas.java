package com.dpx.gui.view.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.dpx.gui.components.BorderLabelButton;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class BarraHerramientas extends JPanel {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private JTextField txtFiltro;
	private JLabel lblBuscar;
	private BorderLabelButton btnP;
	private BorderLabelButton btnQ;
	private BorderLabelButton btnR;
	private JPanel bhBuscar;
	

	//------------------------------------------- Constructor --------------------------------------------
	
	public BarraHerramientas() {
		super();
		
		setPreferredSize(new Dimension(0, 32));
		setLayout(new BorderLayout(0, 0));
		setBackground(new Color(166, 174, 204));
		
		bhBuscar = new JPanel();
		bhBuscar.setBackground(null);
		add(bhBuscar, BorderLayout.EAST);
		
		btnR = new BorderLabelButton(22, Resources.get().getIcon("regex"));
		btnR.setToolTipText("Editor de expresiones regulares");
		bhBuscar.add(btnR);
		
		btnQ = new BorderLabelButton(22, Resources.get().getIcon("database"));
		btnQ.setToolTipText("Asistente para dise�ar consultas a la base de datos");
		bhBuscar.add(btnQ);
		
		btnP = new BorderLabelButton(22, Resources.get().getIcon("user_group"));
		btnP.setToolTipText("Poblar base de datos");
		bhBuscar.add(btnP);
		
		lblBuscar = new JLabel("Buscar");
		bhBuscar.add(lblBuscar);
		
		txtFiltro = new JTextField();
		bhBuscar.add(txtFiltro);
		txtFiltro.setColumns(40);
	}

	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public JLabel getLblBuscar() {
		return lblBuscar;
	}

	public BorderLabelButton getBtnP() {
		return btnP;
	}

	public BorderLabelButton getBtnQ() {
		return btnQ;
	}

	public BorderLabelButton getBtnR() {
		return btnR;
	}

	public JPanel getBhBuscar() {
		return bhBuscar;
	}
}
