package com.dpx.gui.view.main;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;

import com.dpx.gui.view.main.workspace.Workspace;
import com.dpx.resource.Resources;

@SuppressWarnings("serial")
public class DpxFrame extends JFrame {
	
	//-------------------------------------------- Attributes --------------------------------------------
	
	private DpxMenuBar menuBar;
	private BarraHerramientas barraHerramientas;
	private Workspace workspace;
	private Footer footer;
	
	
	//-------------------------------------------- Singleton ---------------------------------------------
	
	private static DpxFrame instance;
	
	public static DpxFrame get() {
		return instance;
	}
	
	
	//------------------------------------------- Constructor --------------------------------------------
	
	public DpxFrame() {
		instance = this;
		
		setTitle("DPX - Prototipo 2");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Resources.get().getIcon("favicon").getImage());
		getContentPane().setLayout(new BorderLayout(0, 0));
		setBackground(new Color(144, 150, 176));
		
		menuBar = new DpxMenuBar();
		setJMenuBar(menuBar);
		
		barraHerramientas = new BarraHerramientas();
		getContentPane().add(barraHerramientas, BorderLayout.NORTH);
		
		workspace = new Workspace();
		getContentPane().add(workspace, BorderLayout.CENTER);
		
		footer = new Footer();
		getContentPane().add(footer, BorderLayout.SOUTH);
		
		
		//poner visible
		this.setSize(720, 480);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setVisible(true);
	}
	
	
	//--------------------------------------------- Getters ----------------------------------------------
	
	public DpxMenuBar getDpxMenuBar() {
		return menuBar;
	}

	public BarraHerramientas getBarraHerramientas() {
		return barraHerramientas;
	}

	public Workspace getWorkspace() {
		return workspace;
	}

	public Footer getFooter() {
		return footer;
	}

}
