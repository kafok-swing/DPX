package com.dpx.gui.components;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

@SuppressWarnings("serial")
public class CTextPane extends JTextPane {

	private int tabSize;
	private int lengthMax;
	
	public CTextPane(int lengthMax) {
		super();
		this.lengthMax = lengthMax;
		tabSize = 4;
		setEditable(false);
	}

	
	public int getTabSize() {
		return tabSize;
	}

	public void setTabSize(int tabSize) {
		this.tabSize = tabSize;
	}
	
	public int getLengthMax() {
		return lengthMax;
	}
	
	
	public int getContentWidth(String content) {
        JTextPane dummyTextPane = new JTextPane();
        dummyTextPane.setFont(this.getFont());
        dummyTextPane.setSize(Short.MAX_VALUE, Short.MAX_VALUE);
        dummyTextPane.setText(content);
        
        return dummyTextPane.getPreferredSize().width;
    }
	
	public String getLastLine() {
        String text = getText();
        int i;
        for(i = text.length() - 1; i >= 0; i--) {
        	if(text.charAt(i) == '\n')
        		break;
        }
        
        return text.substring(i+1, text.length());
    }
	
	
	public void append(Color color, String str) {
		setEditable(true);
		
		String blank = "";
		for(int i=0; i<tabSize; i++)
			blank += " ";
		str = str.replaceAll("[\\t]", blank);
		
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);

		setCaretPosition(getDocument().getLength());
		setCharacterAttributes(aset, false);
		replaceSelection(str);
        
        if(getDocument().getLength() > lengthMax) {
        	int diff = getDocument().getLength() - lengthMax;
    		select(0, diff);
    		replaceSelection("");
    		setCaretPosition(getDocument().getLength());
        }
        
        setEditable(false);
	}
	
	public void setText(String str) {
		setEditable(true);
		
		super.setText("");
		append(Color.BLACK, str);
		
		setEditable(false);
	}
	
	public void clear() {
		setEditable(true);
		super.setText("");
		setEditable(false);
	}
	
	public void clear2() {
	}
}
