package com.dpx.gui.components;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class MaskLabelButton extends JComponent {
//	
//	
//	//---------------------------------------- Atributos de vista ----------------------------------------
//	
//	private ImageIcon icon;
//	private BufferedImage imageHover;
//	private BufferedImage imagePressed;
//	private LabelButtonController controller;
//	
//	
//	//---------------------------------------------- Modelo ----------------------------------------------
//	
//	private int size;
//	
//	private List<ActionListener> actions;
//	
//
//	//------------------------------------------- Constructor --------------------------------------------
//	
//	public MaskLabelButton(int size, ImageIcon normal) {
//		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
//		setPreferredSize(new Dimension(size, size));
//		setMinimumSize(new Dimension(size, size));
//		setMaximumSize(new Dimension(size, size));
//		setBorder(null);
//		setLayout(null);
//		
//		//configuracion inicial
//		this.size = size;
//		this.actions = new LinkedList<ActionListener>();
//		setIcon(normal);
//		setFactors(0.6, 0.7);
//		
//		//controller
//		controller = new LabelButtonController();
//		
//		addMouseListener(controller);
//		addMouseMotionListener(controller);
//	}
//	
//	
//	//------------------------------------- Operaciones en el modelo -------------------------------------
//	
//	public ImageIcon getIcon() {
//		return icon;
//	}
//
//	public void setIcon(ImageIcon icon) {
//		this.icon = icon;
//	}
//	
//	public void setFactors(double hover, double pressed) {
//		if(hover < 0 || hover > 1. || pressed < 0 || pressed > 1.)
//			throw new IllegalArgumentException("Factors must belong to rank [0.0, 1.0]");
//		
//		imageHover = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
//		imagePressed = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
//		
//		int mid = size/2;
//		int mx = icon.getIconWidth()/2;
//		int my = icon.getIconHeight()/2;
//		
//		icon.paintIcon(null, imageHover.createGraphics(), mid-mx, mid-my);
//		icon.paintIcon(null, imagePressed.createGraphics(), mid-mx, mid-my);
//		
//		for (int y = 0; y < size; y++) {
//			for (int x = 0; x < size; x++) {
//				//hover
//				int rgb = imageHover.getRGB(x, y)&0xFFFFFF;
//				int alpha = imageHover.getRGB(x, y)&0xFF000000;
//				rgb = (int) (rgb * hover);
//				imageHover.setRGB(x, y, alpha|rgb);
//				
//				//pressed
//				rgb = imagePressed.getRGB(x, y)&0xFFFFFF;
//				alpha = imagePressed.getRGB(x, y)&0xFF000000;
////				rgb *= pressed;
//				imagePressed.setRGB(x, y, alpha|rgb);
//			}
//		}
//	}
//	
//	
//	//------------------------------------ Operaciones del componente ------------------------------------
//
//	public void addActionListener(ActionListener act) {
//		this.actions.add(act);
//	}
//	
//	public void removeActionListener(ActionListener act) {
//		this.actions.remove(act);
//	}
//	
//	
//	//------------------------------------ Controlador de apariencia -------------------------------------
//	
//	private class LabelButtonController implements MouseListener, MouseMotionListener {
//		
//		//Attributos -----------------------------
//		
//		private boolean isHover;
//		private boolean isPressed;
//		
//		
//		//Constructor ----------------------------
//		
//		public LabelButtonController() {
//			super();
//			this.isHover = false;
//		}
//
//		
//		//Eventos --------------------------------
//		
//		public void mouseDragged(MouseEvent arg0) {}
//		public void mouseMoved(MouseEvent arg0) {}
//		public void mouseClicked(MouseEvent arg0) {}
//
//		@Override
//		public void mouseEntered(MouseEvent arg0) {
//			this.isHover = true;
//			MaskLabelButton.this.repaint();
//		}
//
//		@Override
//		public void mouseExited(MouseEvent arg0) {
//			this.isHover = false;
//			MaskLabelButton.this.repaint();
//		}
//
//		@Override
//		public void mousePressed(MouseEvent arg0) {
//			isPressed = true;
//			MaskLabelButton.this.repaint();
//		}
//
//		@Override
//		public void mouseReleased(MouseEvent arg0) {
//			isPressed = false;
//			MaskLabelButton.this.repaint();
//			if(!isHover) {
//				for(ActionListener act : actions) {
//					ActionEvent e = new ActionEvent(arg0.getSource(), arg0.getID(), null, arg0.getModifiers());
//					act.actionPerformed(e);
//				}
//			}
//		}
//	}
//	
//	@Override
//	protected void paintComponent(Graphics g) {
//		Graphics2D g2 = (Graphics2D) g;
//		
//		if(controller.isHover)
//			g2.drawImage(imageHover, null, 0, 0);
//		if(controller.isPressed)
//			g2.drawImage(imagePressed, null, 0, 0);
//		
//		if(!controller.isHover && !controller.isPressed) {
//			int mid = size/2;
//			int mx = icon.getIconWidth()/2;
//			int my = icon.getIconHeight()/2;
//			
//			icon.paintIcon(null, g, mid-mx, mid-my);
//		}
//	}
	
}
