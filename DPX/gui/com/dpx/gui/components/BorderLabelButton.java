package com.dpx.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class BorderLabelButton extends JPanel {
	
	//-------------------------------------------- Constante ---------------------------------------------
	
	private static final Color TRANSPARENT_COLOR = new Color(0, 0, 0, 0);
	
	
	//---------------------------------------- Atributos de vista ----------------------------------------
	
	private JLabel label;
	private LineBorder borderNormal;
	private LineBorder borderHover;
	private LineBorder borderPressed;
	private LabelButtonController controller;
	
	
	//---------------------------------------------- Modelo ----------------------------------------------
	
	private int size;
	private Color hoverColor;
	private Color fillColor;
	private Color pressedColor;
	private Color pressedFillColor;
	private int percentMoveClicked;
	
	private List<ActionListener> actions;
	

	//------------------------------------------- Constructor --------------------------------------------
	
	public BorderLabelButton(int size, ImageIcon normal) {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setPreferredSize(new Dimension(size, size));
		setMinimumSize(new Dimension(size, size));
		setMaximumSize(new Dimension(size, size));
		setBackground(null);

		controller = new LabelButtonController();
		
		//configuracion inicial
		this.size = size;
		this.setColor(71, 146, 221, 2, 0.8);
		this.setPercentMoveClicked(.05);
		this.actions = new LinkedList<ActionListener>();
		
		//borde inicial
		setBorder(this.borderNormal);
		
		//label
		label = new JLabel(normal);
		label.setPreferredSize(new Dimension(size, size));
		label.addMouseListener(controller);
		label.addMouseMotionListener(controller);
		add(label);
	}
	
	
	//------------------------------------- Operaciones en el modelo -------------------------------------
	
	public void setColor(int r, int g, int b, int levelFill, double factorHover) {
		this.hoverColor = new Color(r, g, b);
		this.fillColor = new Color(r, g, b, 255/levelFill);
		this.pressedColor = new Color((int) (r*factorHover), (int) (g*factorHover), (int) (b*factorHover));
		this.pressedFillColor = new Color((int) (r*factorHover), (int) (g*factorHover), (int) (b*factorHover), 255/levelFill);

		this.borderNormal = new LineBorder(TRANSPARENT_COLOR, 1, false);
		this.borderHover = new LineBorder(this.hoverColor, 1, false);
		this.borderPressed = new LineBorder(this.pressedColor, 1, false);
	}
	
	public void setPercentMoveClicked(double percentMoveClicked) {
		this.percentMoveClicked = (int) (size*percentMoveClicked);
	}
	
	
	//------------------------------------ Operaciones del componente ------------------------------------
	
	public void setIcon(Icon icon) {
		label.setIcon(icon);
	}
	
	public void setToolTipText(String text) {
		label.setToolTipText(text);
	}
	
	public void addActionListener(ActionListener act) {
		this.actions.add(act);
	}
	
	public void removeActionListener(ActionListener act) {
		this.actions.remove(act);
	}
	
	
	//------------------------------------ Controlador de apariencia -------------------------------------
	
	private class LabelButtonController implements MouseListener, MouseMotionListener {
		
		//Attributos -----------------------------
		
		private boolean isPressed;
		private boolean isHover;
		
		
		//Constructor ----------------------------
		
		public LabelButtonController() {
			super();
			this.isPressed = false;
			this.isHover = false;
		}

		
		//Eventos --------------------------------
		
		public void mouseDragged(MouseEvent arg0) {}
		public void mouseMoved(MouseEvent arg0) {}
		public void mouseClicked(MouseEvent arg0) {}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			this.isHover = true;
			if(!isPressed) {
				setBackground(fillColor);
				setBorder(borderHover);
			}
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			this.isHover = false;
			label.setBounds(0, 0, size, size);
			if(!isPressed) {
				setBackground(null);
				setBorder(new LineBorder(TRANSPARENT_COLOR, 1, false));
			}
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			label.setBounds(percentMoveClicked, percentMoveClicked, size+percentMoveClicked, size+percentMoveClicked);
			label.repaint();
			this.isPressed = true;
			setBackground(pressedFillColor);
			setBorder(borderPressed);
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			label.setBounds(0, 0, size, size);
			this.isPressed = false;
			
			if(!isHover) {
				setBorder(borderNormal);
				setBackground(null);
				for(ActionListener act : actions) {
					ActionEvent e = new ActionEvent(BorderLabelButton.this, arg0.getID(), null, arg0.getModifiers());
					act.actionPerformed(e);
				}
			} else {
				setBackground(fillColor);
				setBorder(borderHover);
			}
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.clearRect(0, 0, size, size);
		super.paintComponent(g);
	}
}
