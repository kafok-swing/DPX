package com.dpx.gui.components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class LabelButton extends JPanel {
	
	//---------------------------------------- Atributos de vista ----------------------------------------
	
	private JLabel label;
	private LabelButtonController controller;
	
	
	//---------------------------------------------- Modelo ----------------------------------------------
	
	private int size;
	private int percentMoveClicked;
	private String actionCommand;
	
	private List<ActionListener> actions;
	

	//------------------------------------------- Constructor --------------------------------------------
	
	public LabelButton(int size, ImageIcon normal) {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setPreferredSize(new Dimension(size, size));
		setMinimumSize(new Dimension(size, size));
		setMaximumSize(new Dimension(size, size));
		setBorder(null);
		setLayout(null);
		setBackground(null);

		controller = new LabelButtonController();
		
		//configuracion inicial
		this.size = size;
		this.setPercentMoveClicked(.05);
		this.actions = new LinkedList<ActionListener>();
		
		//label
		label = new JLabel(normal);
		label.setPreferredSize(new Dimension(size, size));
		label.addMouseListener(controller);
		label.addMouseMotionListener(controller);
		label.setBounds(0, 0, size, size);
		add(label);
	}
	
	
	//------------------------------------- Operaciones en el modelo -------------------------------------
	
	public void setPercentMoveClicked(double percentMoveClicked) {
		this.percentMoveClicked = (int) (size*percentMoveClicked);
	}
	
	
	//------------------------------------ Operaciones del componente ------------------------------------
	
	public void setIcon(Icon icon) {
		label.setIcon(icon);
	}
	
	public void setToolTipText(String text) {
		label.setToolTipText(text);
	}
	
	public void addActionListener(ActionListener act) {
		this.actions.add(act);
	}
	
	public void removeActionListener(ActionListener act) {
		this.actions.remove(act);
	}
	
	public String getActionCommand() {
		return actionCommand;
	}

	public void setActionCommand(String actionCommand) {
		this.actionCommand = actionCommand;
	}
	
	
	//------------------------------------ Controlador de apariencia -------------------------------------
	
	private class LabelButtonController implements MouseListener, MouseMotionListener {
		
		//Attributos -----------------------------
		
		private boolean isHover;
		
		
		//Constructor ----------------------------
		
		public LabelButtonController() {
			super();
			this.isHover = false;
		}

		
		//Eventos --------------------------------
		
		public void mouseDragged(MouseEvent arg0) {}
		public void mouseMoved(MouseEvent arg0) {}
		public void mouseClicked(MouseEvent arg0) {}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			this.isHover = true;
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			this.isHover = false;
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			label.setBounds(percentMoveClicked, percentMoveClicked, size+percentMoveClicked, size+percentMoveClicked);
			label.repaint();
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			label.setBounds(0, 0, size, size);
			
			if(isHover) {
				for(ActionListener act : actions) {
					ActionEvent e = new ActionEvent(LabelButton.this, arg0.getID(), actionCommand, arg0.getModifiers());
					act.actionPerformed(e);
				}
			}
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.clearRect(0, 0, size, size);
		super.paintComponent(g);
	}
}
