package com.dpx.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ResourceManager {

	
	//----------------------------- GetResourceRootPath ------------------------------
	
	public static String getResourceRootPath() {
		File res = new File(".");
		try {
			res = new File(ResourceManager.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		} catch (URISyntaxException e) {}
		
		try {
			return res.getCanonicalPath();
		} catch (IOException e) {
			return null;
		}
	}
	
	
	//--------------------------------- ScanResource ---------------------------------
	
	public static List<Resource> scanResources(String path) {
		return scanResources(path, "", ';');
	}
	
	public static List<Resource> scanResources(String path, String extensionFilter) {
		return scanResources(path, extensionFilter, ';');
	}
	
	public static List<Resource> scanResources(String path, String extensionFilter, char separator) {
		//obtenemos el filtro
		List<String> filter = new LinkedList<String>();
		
		if(!extensionFilter.equals("")) {
			String[] splitFilter = extensionFilter.split("[" + separator + "]");
			for(String s : splitFilter) 
				filter.add(s);
		}
		
		//escaneamos recursos recursivamente
		String root = getResourceRootPath();
		List<Resource> res = new LinkedList<Resource>();
		if(root.toLowerCase().endsWith(".jar")) {	//somos un jar
			try {
				ZipInputStream zip = new ZipInputStream(new FileInputStream(root));
				for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
					if(!entry.isDirectory() && entry.getName().startsWith(path) && checkFilter(filter, entry.getName()))
						res.add(new ResourceZipEntry(root, entry));
				}
				zip.close();
			} catch(Throwable e) {}
		} else	//estamos en la estapa de desarrollo
			scanFiles(root + "/" + path, res, filter);
		
		return res;
	}
	
	private static boolean checkFilter(List<String> filter, String name) {
		if(filter.isEmpty())
			return true;
		
		for(String s : filter) {
			if(name.endsWith("." + s))
				return true;
		}
		
		return false;
	}
	
	private static void scanFiles(String path, List<Resource> res, List<String> filter) {
		File file = new File(path);
		for(File f : file.listFiles()) {
			if(f.isDirectory())
				scanFiles(path + "/" + f.getName(), res, filter);
			else if(checkFilter(filter, f.getName()))
				res.add(new ResourceFile(f.getAbsolutePath()));
		}
	}
	
	
	//---------------------------------- ScanFiles -----------------------------------
	
	public static List<File> scanFiles(String path) {
		return scanFiles(path, "", ';');
	}
	
	public static List<File> scanFiles(String path, String extensionFilter) {
		return scanFiles(path, extensionFilter, ';');
	}
	
	public static List<File> scanFiles(String path, String extensionFilter, char separator) {
		//obtenemos el filtro
		List<String> filter = new LinkedList<String>();
		
		if(!extensionFilter.equals("")) {
			String[] splitFilter = extensionFilter.split("[" + separator + "]");
			for(String s : splitFilter) 
				filter.add(s);
		}
		
		//escaneamos archivos recursivamente
		List<File> res = new LinkedList<File>();
		scanFiles2(path, res, filter);
		
		return res;
	}
	
	private static void scanFiles2(String path, List<File> res, List<String> filter) {
		File file = new File(path);
		if(!file.exists())
			return;
		for(File f : file.listFiles()) {
			if(f.isDirectory())
				scanFiles2(path + "/" + f.getName(), res, filter);
			else if(checkFilter(filter, f.getName()))
				res.add(new File(f.getAbsolutePath()));
		}
	}
	
	
	public static List<File> scanDirectories(String path) {
		List<File> res = new LinkedList<File>();
		
		File dirs = new File(path);
		File[] files = dirs.listFiles();
		
		if(files != null) {
			for(File dir : files)
				if(dir.isDirectory())
					res.add(dir);
		}
		
		return res;
	}
	
	
	//---------------------------------- LoadClass -----------------------------------
	
	public static ClassLoader createLoader(String targetClassesPath) {
		try {
			File fileLoader = new File(targetClassesPath);
		    URL url = fileLoader.toURI().toURL();
		    URL[] urls = new URL[]{url};
		    return new URLClassLoader(urls);
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ClassLoader createLoader(Collection<String> targetsClassesPath) {
		try {
			URL[] urls = new URL[targetsClassesPath.size()];
			int i = 0;
			for(String s : targetsClassesPath) {
				File fileLoader = new File(s);
			    URL url = fileLoader.toURI().toURL();
			    urls[i] = url;
			    i++;
			}
			
		    return new URLClassLoader(urls);
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
}
