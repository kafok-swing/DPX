package com.dpx.resource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

class ResourceFile implements Resource {
	
	//---------------------------------- Attributes ----------------------------------
	
	private File file;
	
	
	//--------------------------------- Constructor ----------------------------------
	
	public ResourceFile(String path) {
		this.file = new File(path);
		if(!this.file.isFile())
			throw new IllegalArgumentException("Resources must be valid files!");
	}
	

	//------------------------------------ Getter ------------------------------------
	
	public String getName() {
		return this.file.getName();
	}
	
	public String getAbsolutePath() {
		return this.file.getAbsolutePath();
	}

	
	//----------------------------------- Creates ------------------------------------
	
	public byte[] getAsByte() {
		return toBytes();
	}
	
	public InputStream getAsInputStream() {
		return toInput();
	}
	
	public OutputStream getAsOutputStream() {
		return toOutput();
	}
	
	//----------------------------------- Privates -----------------------------------
	
	private InputStream toInput() {
		try {
	    	return new FileInputStream(file);
		} catch(Throwable e) {
			return null;
		}
	}
	
	private OutputStream toOutput() {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	    	InputStream in = new FileInputStream(file);
	    	byte[] buffer = new byte[4096];
	    	for(int n; (n = in.read(buffer)) != -1; )
	    	    out.write(buffer, 0, n);
	    	
	    	in.close();
	    	
	    	return out;
		} catch(Throwable e) {
			return null;
		}
	}
	
	private byte[] toBytes() {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	    	InputStream in = new FileInputStream(file);
	    	byte[] buffer = new byte[4096];
	    	for(int n; (n = in.read(buffer)) != -1; )
	    	    out.write(buffer, 0, n);
	    	
	    	in.close();
	    	out.close();
	    	
	    	return out.toByteArray();
		} catch(Throwable e) {
			return null;
		}
	}
	
}
