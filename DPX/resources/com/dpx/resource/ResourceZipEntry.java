package com.dpx.resource;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

class ResourceZipEntry implements Resource{
	
	//---------------------------------- Attributes ----------------------------------
	
	private ZipEntry entry;
	private String root;
	
	
	//--------------------------------- Constructor ----------------------------------
	
	public ResourceZipEntry(String root, ZipEntry entry) {
		this.entry = entry;
		this.root = root;
		if(this.entry.isDirectory())
			throw new IllegalArgumentException("Resources must be valid files!");
	}
	

	//------------------------------------ Getter ------------------------------------
	
	public String getName() {
		return this.entry.getName();
	}
	
	public String getAbsolutePath() {
		return this.root + "/" + this.entry.getName();
	}

	
	//----------------------------------- Creates ------------------------------------
	
	public byte[] getAsByte() {
	    return toBytes();
	}
	
	public InputStream getAsInputStream() {
		return toInput();
	}
	
	public OutputStream getAsOutputStream() {
		return toOutput();
	}
	
	
	//----------------------------------- Privates -----------------------------------
	
	private InputStream toInput() {
		try {
	    	ZipFile zf = new ZipFile(root);
	    	InputStream res = zf.getInputStream(entry);
	    	zf.close();
	    	return res;
		} catch(Throwable e) {
			return null;
		}
	}
	
	private OutputStream toOutput() {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ZipFile zf = new ZipFile(root);
	    	InputStream in = zf.getInputStream(entry);
	    	byte[] buffer = new byte[4096];
	    	for(int n; (n = in.read(buffer)) != -1; )
	    	    out.write(buffer, 0, n);
	    	
	    	in.close();
	    	zf.close();
	    	
	    	return out;
		} catch(Throwable e) {
			return null;
		}
	}
	
	private byte[] toBytes() {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	    	ZipFile zf = new ZipFile(root);
	    	InputStream in = zf.getInputStream(entry);
	    	byte[] buffer = new byte[4096];
	    	for(int n; (n = in.read(buffer)) != -1; )
	    	    out.write(buffer, 0, n);
	    	
	    	in.close();
	    	zf.close();
	    	out.close();
	    	
	    	return out.toByteArray();
		} catch(Throwable e) {
			return null;
		}
	}
}
