package com.dpx.resource;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public class Resources {

	//singleton ----------------------------------------
	
	private static Resources singleton = null;
	
	public static Resources get() {
		if(singleton == null)
			singleton = new Resources();
		
		return singleton;
	}
	
	
	//atributos ---------------------------------------
	
	private Map<String, ImageIcon> icons;
	private Map<String, Object> configs;
	
	
	//constructor -------------------------------------
	
	private Resources() {
		icons = new HashMap<String, ImageIcon>();
		configs = new HashMap<String, Object>();
	}
	
	
	//getters -----------------------------------------
	
	public Object getConfig(String name) {
		return configs.get(name);
	}

	public ImageIcon getIcon(String name) {
		return icons.get(name);
	}
	
	
	//loads -------------------------------------------
	
	void loadIcon(String name, ImageIcon icon) {
		icons.put(name, icon);
	}
	
	void loadConfig(String name, Object config) {
		configs.put(name, config);
	}
	
	
	//saves -------------------------------------------
	
	public void saveConfig(String name, Object config) {
		ResourceLoader.saveConfig(name, config);
		configs.put(name, config);
	}
}
