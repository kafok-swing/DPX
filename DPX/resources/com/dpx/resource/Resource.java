package com.dpx.resource;

import java.io.InputStream;
import java.io.OutputStream;

public interface Resource {
	
	//------------------------------------ Getter -----------------------------------
	
	public String getName();
	public String getAbsolutePath();
	
	
	//----------------------------------- Creates -----------------------------------
	
	public byte[] getAsByte();
	public InputStream getAsInputStream();
	public OutputStream getAsOutputStream();
	
}
