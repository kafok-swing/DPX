package com.dpx.resource;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import javax.swing.ImageIcon;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

public class ResourceLoader {
	
	//---------------------------------- Constantes ----------------------------------
	
	public static final String ICON_PATH = "assets/img/icons";
	public static final String CONFIG_PATH = "config";
	
	public static final String CONFIG_EXTENSION = ".config";
	
	
	//------------------------------------ Loads -------------------------------------
	
	public static void loadIcons() {
		List<Resource> icons = ResourceManager.scanResources(ICON_PATH);
		for(Resource icon : icons) {
			ImageIcon ic = new ImageIcon(icon.getAsByte());
			
			String name = icon.getName().replace(ICON_PATH, "");
			name = name.replace("/", ".").replace("\\", ".").toLowerCase();
			if(name.charAt(0) == '.')
				name = name.substring(1, name.length());
			name = name.substring(0, name.length() - 4);	//4 es lo que mide .gif
			name = name.replaceAll("[^a-zA-Z0-9.]", "_");
			
			Resources.get().loadIcon(name, ic);
		}
	}
	
	
	public static void loadConfigs(String _package) {
		String root = ResourceManager.getResourceRootPath();
		ClassLoader loader = ResourceManager.createLoader(root);
		
		List<Resource> configsClass = ResourceManager.scanResources(_package.replace(".", "/").replace("\\", "."), "class");
		
		for(Resource res : configsClass) {
			//nombre de la clase
			String name = res.getName().replace(root, "");
			name = name.substring(0, name.length() - 6);	//6 es lo que mide .class
			name = name.replace("/", ".").replace("\\", ".");
			if(name.charAt(0) == '.')
				name = name.substring(1, name.length());
			if(!name.contains(_package))
				name = _package + "." + name;
			
			//cargamos las clases
			Class<? extends Object> clazz;
			try {
				clazz = loader.loadClass(name);
				name = name.replace(_package + ".", "");
				
				//si no existe el archivo
				File f = new File(CONFIG_PATH + "/" + name + CONFIG_EXTENSION);
				if(!f.exists()) {
					//si no existe path
					File d = new File(CONFIG_PATH);
					if(!d.exists()) 
						d.mkdirs();
					
					if(clazz.isInterface())	//si no se puede instanciar pasamos a la siguiente clase
						continue;
					
					Object obj = clazz.newInstance();
					YamlWriter writer = new YamlWriter(new FileWriter(CONFIG_PATH + File.separator + name + CONFIG_EXTENSION));
					writer.getConfig().writeConfig.setWriteDefaultValues(true);
					writer.getConfig().writeConfig.setWriteRootTags(false);
					writer.write(obj);
					writer.close();
				}

				YamlReader reader = new YamlReader(new FileReader(CONFIG_PATH + File.separator + name + CONFIG_EXTENSION));
				Resources.get().loadConfig(name, reader.read(clazz));
				reader.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	static boolean saveConfig(String name, Object config) {
		try {
			File d = new File(CONFIG_PATH);
			if(!d.exists()) 
				d.mkdirs();
			
			YamlWriter writer = new YamlWriter(new FileWriter(CONFIG_PATH + File.separator + name + CONFIG_EXTENSION));
			writer.getConfig().writeConfig.setWriteDefaultValues(true);
			writer.getConfig().writeConfig.setWriteRootTags(false);
			writer.write(config);
			writer.close();
			
			return true;
		} catch(Throwable e) {
			return false;
		}
	}
	
}
