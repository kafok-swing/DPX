package com.dpx.resource.configurations;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class Blueprint extends JPanel {
	
	public Blueprint() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		setBackground(null);
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Crear clase pruneada", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		add(verticalBox);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_2.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_2);
		
		Box horizontalBox = Box.createHorizontalBox();
		panel_2.add(horizontalBox);
		
		JLabel lblEntidad = new JLabel("Entidad");
		horizontalBox.add(lblEntidad);
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		horizontalBox.add(horizontalStrut);
		
		JComboBox comboBox = new JComboBox();
		horizontalBox.add(comboBox);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalBox.add(verticalStrut);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Campos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		FlowLayout flowLayout_1 = (FlowLayout) panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		verticalBox.add(panel_1);
		
		Box verticalBox_1 = Box.createVerticalBox();
		panel_1.add(verticalBox_1);
		verticalBox_1.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		JCheckBox chckbxNameeeeee = new JCheckBox("nameeeeee");
		verticalBox_1.add(chckbxNameeeeee);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		verticalBox.add(verticalStrut_1);
		
		JPanel panel = new JPanel();
		verticalBox.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		panel.add(horizontalBox_1, BorderLayout.WEST);
		
		JButton btnCrear = new JButton("Crear");
		horizontalBox_1.add(btnCrear);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		horizontalBox_1.add(horizontalStrut_1);
		
		JButton btnEditar = new JButton("Editar");
		horizontalBox_1.add(btnEditar);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		panel.add(horizontalBox_2, BorderLayout.EAST);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(10);
		horizontalBox_2.add(horizontalStrut_2);
		
		JButton btnSeleccionarTodo = new JButton("Seleccionar todo");
		horizontalBox_2.add(btnSeleccionarTodo);
		
		JButton btnQuitarTodo = new JButton("Quitar todo");
		horizontalBox_2.add(btnQuitarTodo);
		
	}

}
